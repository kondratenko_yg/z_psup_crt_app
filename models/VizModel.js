sap.ui.define(['sap/ui/model/json/JSONModel'], function (JSONModel) {
  return JSONModel.extend('initiatives.models.VizModel', {
    initialize: function () {
      this.setData(this.oInitialData);

      var nCurYear = new Date().getFullYear() + 1;
      for (var i = 0; i < 5; i++) {
        var oYearData = {
          year: nCurYear + i,
          el: 0,
          risk: 0
        };
        this.setProperty('/years/' + i, oYearData)
      }

      return this
    },
    oInitialData:{
      years:{}
    }
  })
});