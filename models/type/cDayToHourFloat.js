sap.ui.define(['jquery.sap.global', 'sap/ui/core/format/NumberFormat', 'sap/ui/model/SimpleType', 'sap/ui/model/FormatException', 'sap/ui/model/ParseException', 'sap/ui/model/ValidateException'],
    function(jQuery, NumberFormat, SimpleType, FormatException, ParseException, ValidateException) {
        "use strict";
        var Float = SimpleType.extend("initiatives.models.type.cDayToHourFloat", {

            constructor : function () {
                SimpleType.apply(this, arguments);
                this.sName = "cDayToHourFloat";
                this.sAllowed = ['',' '];
                try {
                    //получаем ссылку на любой контрол через стабильный id
                    this.oControl = sap.ui.getCore().byId('companyName');
                }catch(ex){
                    //TODO Error
                    debugger;
                }
            }

        });

        /**
         * @see sap.ui.model.SimpleType.prototype.formatValue
         */
        Float.prototype.formatValue = function(vValue, sInternalType) {
            var fValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
            if (vValue == undefined || vValue == null) {
                return null;
            }
            if (this.oInputFormat) {
                fValue = this.oInputFormat.parse(vValue);
                if (fValue == null) {
                    throw new FormatException("Cannot format float: " + vValue + " has the wrong format");
                }
            }
            switch (this.getPrimitiveType(sInternalType)) {
                case "string":
                    return this.oOutputFormat.format(fValue);
                case "int":
                    return Math.floor(fValue);
                case "float":
                case "any":
                    return fValue;
                default:
                    throw new FormatException("Don't know how to format Float to " + sInternalType);
            }
        };
        Float.prototype.beforeParse = function(fResult){
            //Update Model before parse
            if(this.oFormatOptions.updateModelBeforeUpdate) {
                var ModeInfo, oModel, sPath, vResult;
                if(!this.oControl){
                    //TODO Error
                    return;
                }
                ModeInfo = this.oFormatOptions.updateModelBeforeUpdate.split(">");
                if(ModeInfo.length > 1){
                    oModel = this.oControl.getModel(ModeInfo[0]);
                    sPath = ModeInfo[1];
                }else{
                    oModel = this.oControl.getModel();
                    sPath = ModeInfo[0];
                }
                if(oModel) {
                    vResult = Math.floor(fResult * 24);
                    oModel.setProperty(sPath, vResult);
                }
            }
        };
        /**
         * @see sap.ui.model.SimpleType.prototype.parseValue
         */
        Float.prototype.parseValue = function(vValue, sInternalType) {
            vValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
            var fResult, oBundle;
            switch (this.getPrimitiveType(sInternalType)) {
                case "string":
                    fResult = this.oOutputFormat.parse(vValue);
                    if (isNaN(fResult)) {
                        oBundle = sap.ui.getCore().getLibraryResourceBundle();
                        throw new ParseException(oBundle.getText("Float.Invalid"));
                    }
                    break;
                case "int":
                case "float":
                    fResult = vValue;
                    break;
                default:
                    throw new ParseException("Don't know how to parse Float from " + sInternalType);
            }
            if (this.oInputFormat) {
                fResult = this.oInputFormat.format(fResult);
            }
            this.beforeParse(fResult);
            return fResult;
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.validateValue
         */
        Float.prototype.validateValue = function(vValue) {
            if (this.oConstraints) {
                var oBundle = sap.ui.getCore().getLibraryResourceBundle(),
                    aViolatedConstraints = [],
                    aMessages = [],
                    fValue = vValue;
                if (this.oInputFormat) {
                    fValue = this.oInputFormat.parse(vValue);
                }
                jQuery.each(this.oConstraints, function(sName, oContent) {
                    switch (sName) {
                        case "minimum":
                            if (fValue < oContent) {
                                aViolatedConstraints.push("minimum");
                                aMessages.push(oBundle.getText("Float.Minimum", [oContent]));
                            }
                            break;
                        case "maximum":
                            if (fValue > oContent) {
                                aViolatedConstraints.push("maximum");
                                aMessages.push(oBundle.getText("Float.Maximum", [oContent]));
                            }
                    }
                });
                if (aViolatedConstraints.length > 0) {
                    throw new ValidateException(aMessages.join(" "), aViolatedConstraints);
                }
            }
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.setFormatOptions
         */
        Float.prototype.setFormatOptions = function(oFormatOptions) {
            this.oFormatOptions = oFormatOptions;
            this._createFormats();
        };

        /**
         * Called by the framework when any localization setting changed
         * @private
         */
        Float.prototype._handleLocalizationChange = function() {
            this._createFormats();
        };

        /**
         * Create formatters used by this type
         * @private
         */
        Float.prototype._createFormats = function() {
            var oSourceOptions = this.oFormatOptions.source;
            this.oOutputFormat = NumberFormat.getFloatInstance(this.oFormatOptions);
            if (oSourceOptions) {
                if (jQuery.isEmptyObject(oSourceOptions)) {
                    oSourceOptions = {
                        groupingEnabled: false,
                        groupingSeparator: " ",
                        decimalSeparator: "."
                    };
                }
                this.oInputFormat = NumberFormat.getFloatInstance(oSourceOptions);
            }
        };

        return Float;

    });