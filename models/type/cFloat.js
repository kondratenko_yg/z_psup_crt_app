sap.ui.define(['jquery.sap.global', 'sap/ui/core/format/NumberFormat', 'sap/ui/model/type/Float', 'sap/ui/model/FormatException', 'sap/ui/model/ParseException', 'sap/ui/model/ValidateException'],
    function(jQuery, NumberFormat, Float, FormatException, ParseException, ValidateException) {
        "use strict";

        var cFloat = Float.extend("initiatives.models.type.cFloat", {
            constructor : function () {
                Float.apply(this, arguments);
                this.sName = "cFloat";
                this.sAllowed = ['',' '];
            }
        });

        /**
         * @see sap.ui.model.SimpleType.prototype.formatValue
         */
        cFloat.prototype.formatValue = function(vValue, sInternalType) {
            var fValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
            if (vValue == undefined || vValue == null) {
                return null;
            }
            if (this.oInputFormat) {
                fValue = this.oInputFormat.parse(vValue);
                if (fValue == null) {
                    throw new FormatException("Cannot format float: " + vValue + " has the wrong format");
                }
            }
            switch (this.getPrimitiveType(sInternalType)) {
                case "string":
                    return this.oOutputFormat.format(fValue);
                case "int":
                    return Math.floor(fValue);
                case "float":
                case "any":
                    return fValue;
                default:
                    throw new FormatException("Don't know how to format Float to " + sInternalType);
            }
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.parseValue
         */
        cFloat.prototype.parseValue = function(vValue, sInternalType) {
            var fResult, oBundle;
            switch (this.getPrimitiveType(sInternalType)) {
                case "string":
                    vValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
                    fResult = this.oOutputFormat.parse(vValue);
                    if (isNaN(fResult)) {
                        oBundle = sap.ui.getCore().getLibraryResourceBundle();
                        throw new ParseException(oBundle.getText("Float.Invalid"));
                    }
                    break;
                case "int":
                case "float":
                    fResult = vValue;
                    break;
                default:
                    throw new ParseException("Don't know how to parse Float from " + sInternalType);
            }
            if (this.oInputFormat) {
                fResult = this.oInputFormat.format(fResult);
            }
            return fResult;
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.validateValue
         */
        cFloat.prototype.validateValue = function(vValue) {
            if (this.oConstraints) {
                var oBundle = sap.ui.getCore().getLibraryResourceBundle(),
                  aViolatedConstraints = [],
                  aMessages = [],
                  fValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
                if (this.oInputFormat) {
                    fValue = this.oInputFormat.parse(vValue);
                }
                jQuery.each(this.oConstraints, function(sName, oContent) {
                    switch (sName) {
                        case "minimum":
                            if (fValue < oContent) {
                                aViolatedConstraints.push("minimum");
                                aMessages.push(oBundle.getText("Float.Minimum", [oContent]));
                            }
                            break;
                        case "maximum":
                            if (fValue > oContent) {
                                aViolatedConstraints.push("maximum");
                                aMessages.push(oBundle.getText("Float.Maximum", [oContent]));
                            }
                    }
                });
                if (aViolatedConstraints.length > 0) {
                    throw new ValidateException(aMessages.join(" "), aViolatedConstraints);
                }
            }
        };

        return cFloat;
    });