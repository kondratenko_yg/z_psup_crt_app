sap.ui.define(['jquery.sap.global', 'sap/ui/core/format/NumberFormat', 'sap/ui/model/SimpleType', 'sap/ui/model/FormatException', 'sap/ui/model/ParseException', 'sap/ui/model/ValidateException'],
    function(jQuery, NumberFormat, SimpleType, FormatException, ParseException, ValidateException) {
        "use strict";
        var Integer = SimpleType.extend("initiatives.models.type.cInteger", /** @lends sap.ui.model.type.cInteger.prototype */ {

            constructor : function () {
                SimpleType.apply(this, arguments);
                this.sName = "cInteger";
                this.sAllowed = ['',' '];
            }
        });

        /**
         * @see sap.ui.model.SimpleType.prototype.formatValue
         */
        Integer.prototype.formatValue = function(vValue, sInternalType) {
            var iValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
            if (vValue == undefined || vValue == null) {
                return null;
            }
            if (this.oInputFormat) {
                iValue = this.oInputFormat.parse(vValue);
                if (iValue == null) {
                    throw new FormatException("Cannot format float: " + vValue + " has the wrong format");
                }
            }
            switch (this.getPrimitiveType(sInternalType)) {
                case "string":
                    return this.oOutputFormat.format(iValue);
                case "int":
                case "float":
                case "any":
                    return iValue;
                default:
                    throw new FormatException("Don't know how to format Integer to " + sInternalType);
            }
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.parseValue
         */
        Integer.prototype.parseValue = function(vValue, sInternalType) {
            var iResult, oBundle;
            switch (this.getPrimitiveType(sInternalType)) {
                case "string":
                    vValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
                    iResult = this.oOutputFormat.parse(String(vValue));
                    if (isNaN(iResult)) {
                        oBundle = sap.ui.getCore().getLibraryResourceBundle();
                        throw new ParseException(oBundle.getText("Integer.Invalid"));
                    }
                    break;
                case "float":
                    iResult = Math.floor(vValue);
                    if (iResult != vValue) {
                        oBundle = sap.ui.getCore().getLibraryResourceBundle();
                        throw new ParseException(oBundle.getText("Integer.Invalid"));
                    }
                    break;
                case "int":
                    iResult = vValue;
                    break;
                default:
                    throw new ParseException("Don't know how to parse Integer from " + sInternalType);
            }
            if (this.oInputFormat) {
                iResult = this.oInputFormat.format(iResult);
            }
            return iResult;
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.validateValue
         */
        Integer.prototype.validateValue = function(vValue) {
            if (this.oConstraints) {
                var oBundle = sap.ui.getCore().getLibraryResourceBundle(),
                    aViolatedConstraints = [],
                    aMessages = [],
                    iValue = (!~this.sAllowed.indexOf(vValue)) ? vValue : '0';
                if (this.oInputFormat) {
                    iValue = this.oInputFormat.parse(vValue);
                }
                jQuery.each(this.oConstraints, function(sName, oContent) {
                    switch (sName) {
                        case "minimum":
                            if (iValue < oContent) {
                                aViolatedConstraints.push("minimum");
                                aMessages.push(oBundle.getText("Integer.Minimum", [oContent]));
                            }
                            break;
                        case "maximum":
                            if (iValue > oContent) {
                                aViolatedConstraints.push("maximum");
                                aMessages.push(oBundle.getText("Integer.Maximum", [oContent]));
                            }
                    }
                });
                if (aViolatedConstraints.length > 0) {
                    throw new ValidateException(aMessages.join(" "), aViolatedConstraints);
                }
            }
        };

        /**
         * @see sap.ui.model.SimpleType.prototype.setFormatOptions
         */
        Integer.prototype.setFormatOptions = function(oFormatOptions) {
            this.oFormatOptions = oFormatOptions;
            this._createFormats();
        };

        /**
         * Called by the framework when any localization setting changed
         * @private
         */
        Integer.prototype._handleLocalizationChange = function() {
            this._createFormats();
        };

        /**
         * Create formatters used by this type
         * @private
         */
        Integer.prototype._createFormats = function() {
            var oSourceOptions = this.oFormatOptions.source;
            this.oOutputFormat = NumberFormat.getIntegerInstance(this.oFormatOptions);
            if (oSourceOptions) {
                if (jQuery.isEmptyObject(oSourceOptions)) {
                    oSourceOptions = {
                        groupingEnabled: false,
                        groupingSeparator: " ",
                        decimalSeparator: "."
                    };
                }
                this.oInputFormat = NumberFormat.getIntegerInstance(oSourceOptions);
            }
        };

        return Integer;

    });