jQuery.sap.declare('initiatives.util.polyfills');
(function () {
  if (!Number.isNaN) {
    Number.isNaN = Number.isNaN || function (value) {
        return typeof value === 'number' && isNaN(value);
      }
  }
})();