jQuery.sap.declare('initiatives.util.formatters');

jQuery.sap.require('sap.ui.core.MessageType');

initiatives.util.formatters = {
  /**
     * Форматтер ячеек таблицы ожидаемого ущерба (EL) Группа А
     * 
     * @returns {*}
     */
    elTableCellsFormatter: function (downtimePenalty, critCoefficient, breakChance, downtimeTotal, realizationFormId) {
        var el = Math.round((downtimePenalty * critCoefficient) * breakChance / 100 * downtimeTotal);

        if (this.getModel('viz')) {
            var sPath = this.getBindingContext('engine').getPath();
            this.getModel('viz').setProperty(sPath + '/el', +el.toFixed(2));
            this.getModel('viz').refresh(true);
            this.getModel('engine').setProperty(sPath + '/groupA/el', +el.toFixed(2));
        }

        return initiatives.util.formatters.IntegerType.formatValue(+el.toFixed(2), "string");
    },

    /**
     * Форматтер ячеек таблицы ожидаемого ущерба (EL) Группа В
     * 
     * @returns {*}
     */
    elTableCellsFormatterGroupV: function (ZpurchPr, ZncPr, ZdwntmLm, ZfailPr, downtimePenalty, breakChance, downtimeTotal, reserveId) {
        var el = 0;
        switch (reserveId){
            case '1':
                el = (ZpurchPr/100) * downtimePenalty * (breakChance/100) * downtimeTotal;
                break;
            case '2':
                el = (ZncPr/100) * ZdwntmLm * (breakChance/100);
                break;
            case '3':
                el = downtimePenalty * downtimeTotal * (breakChance/100) * (ZfailPr/100);
                break;
        }

        if (this.getModel('viz')) {
            var sPath = this.getBindingContext('engine').getPath();
            this.getModel('viz').setProperty(sPath + '/el', el);
            this.getModel('viz').refresh(true);
            this.getModel('engine').setProperty(sPath + '/groupA/el', el);
        }

        return initiatives.util.formatters.IntegerType.formatValue(el, "string");
    },

    /**
     * Форматтер ячеек таблицы коэфициентов риска группа А
     */
    riskTableCellsFormatter: function (allBudget, downtimePenalty, critCoefficient, breakChance, downtimeTotal, realizationFormId) {
        var value = Math.round((downtimePenalty * critCoefficient) * breakChance / 100 * downtimeTotal) / allBudget;
    
        var bIsNaN = +value.toFixed(3) === "NaN";
    
        if (this.getModel('viz')) {
            if (!bIsNaN) {
                var sPath = this.getBindingContext('engine').getPath();
                this.getModel('viz').setProperty(sPath + '/risk', +value.toFixed(3));
                this.getModel('viz').refresh(true);
                this.getModel('engine').setProperty(sPath + '/groupA/riskCoef', +value.toFixed(3));
            }
        }
        return bIsNaN ? 0 : +value.toFixed(3);
    },

    /**
     * Форматтер ячеек таблицы коэфициентов риска группа В
     */

    riskTableCellsFormatterGroupV: function (allBudget, ZpurchPr, ZncPr, ZdwntmLm, ZfailPr, downtimePenalty, breakChance, downtimeTotal, reserveId) {
        // var value = (Math.round(downtimePenalty * critCoefficient * breakChance /
        // 100 * downtimeTotal) / allBudget).toFixed(2);

        var value = 0;
        switch (reserveId) {
            case '1':
                value = (ZpurchPr / 100) * downtimePenalty * (breakChance / 100) * downtimeTotal;
                break;
            case '2':
                value = (ZncPr / 100) * ZdwntmLm * (breakChance / 100);
                break;
            case '3':
                value = downtimePenalty * downtimeTotal * (breakChance / 100) * (ZfailPr / 100);
                break;
        }
    
        value = (value / allBudget).toFixed(1);
    
        var bIsNaN = value === "NaN";
    
        if (this.getModel('viz')) {
            if (!bIsNaN) {
                var sPath = this.getBindingContext('engine').getPath();
                this.getModel('viz').setProperty(sPath + '/risk', value);
                this.getModel('viz').refresh(true);
                this.getModel('engine').setProperty(sPath + '/groupA/riskCoef', value);
            }
        }
        return bIsNaN ? 0 : value;
    },

    /**
     * Форматтер общих сумм бюджета
     * 
     * @param financed
     * @param financeLeft
     * @returns {*}
     */
    ammountFormatter: function (financed, financeLeft) {
        if (typeof financed !== 'undefined' && typeof financeLeft !== 'undefined') {
            return +financed + financeLeft;
        } else {
            return "";
        }
    },

    /**
     * Форматтер цвета полосок вероятности отказа
     * 
     * @param nPercent
     * @returns {*}
     */
    critChanceBarStateFormatter: function (nPercent) {
        if (nPercent <= 30) {
            return 'Success'
        } else if (nPercent > 30 && nPercent < 60) {
            return 'Warning'
        } else {
            return 'Error'
        }
    },

    /**
     * Склонение по числам
     * 
     * @param number
     * @param titles
     * @returns {*}
     */
    declOfNum: function (number, titles) {
        cases = [2, 0, 1, 1, 1, 2];
        number = Math.floor(number);
        return  titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    },

    dayInText: function(){
        var formatters = initiatives.util.formatters;
        var values = Array.prototype.slice.call(arguments);
        var timeTotal = values.reduce(function (sum, val) {
            return sum += +val;
        }, 0);
    
        return formatters.declOfNum(timeTotal, [" " + 
            this.getText("Day1"), " " +
            this.getText("Day2"), " " +
            this.getText("Day3")]);
    },

    hourInText: function(){
        var formatters = initiatives.util.formatters;
        var values = Array.prototype.slice.call(arguments);
        var timeTotal = values.reduce(function (sum, val) {
            return sum += +val;
        }, 0);

        return formatters.declOfNum(timeTotal, [" " +
            this.getText("Hour1"), " " +
            this.getText("Hour2"), " " + 
            this.getText("Hour3")]);
    },

    floatType:  new sap.ui.model.type.Float({
        groupingEnabled: true,
        groupingSeparator: " ",
        decimalSeparator: ".",
        maxFractionDigits: 1
    }),

    /**
     * Fraction Digits 3
     */
    floatType_3:  new sap.ui.model.type.Float({
        groupingEnabled: true,
        groupingSeparator: " ",
        decimalSeparator: ".",
        maxFractionDigits: 3
    }),

    IntegerType:  new sap.ui.model.type.Integer({
        groupingEnabled: true,
        groupingSeparator: " "
    }),
    
    /**
     * Форматтер стоимости одного часа простоя
     * 
     * @param downtimePenalty
     * @param penaltyCoefficient
     */
    downtimePenaltyFormatter: function (downtimePenalty, penaltyCoefficient) {
        var formatters = initiatives.util.formatters;
        var value =  (downtimePenalty * penaltyCoefficient);

        return formatters.numberWithSpaces(+value.toFixed(3));
    },
  
    dateFormatter: function (dDate) {
        if (!dDate) return '';
        return dDate.toLocaleDateString();
    },

    downtimeFormatterText: function () {
        var formatters = initiatives.util.formatters;
        var values = Array.prototype.slice.call(arguments);

        var downtimeTotal = values.reduce(function (sum, val) {
          return sum += +val
        }, 0);

        downtimeTotal = formatters.floatType.formatValue(downtimeTotal ,"string");
        downtimeTotal += formatters.declOfNum(downtimeTotal, 
                [" " + this.getText("Hour1"), " " + this.getText("Hour2"), " " + this.getText("Hour3")]);
        return downtimeTotal;
    },

    downtimeFormatter: function(emergencyBuy, designing, liquidation, assembling, starting) {
        var downtimeTotal =  Math.max(+emergencyBuy, +liquidation) + +assembling + +starting + +designing;
        this.oEngineModel.setProperty('/downtimeTotal', downtimeTotal);
        return this.formatter._prepareDowntimeTotal.call(this, downtimeTotal);
    },

    downtimeFormatterV: function(emergencyBuy, liquidation, assembling, starting) {
        var downtimeTotal =  Math.max(+emergencyBuy, +liquidation) + +assembling + +starting;
        this.oEngineModel.setProperty('/downtimeTotal', downtimeTotal);
        return this.formatter._prepareDowntimeTotal.call(this, downtimeTotal);
    },

    _prepareDowntimeTotal: function(downtimeTotal, aTextHours) {
        downtimeTotal = this.formatter.floatType.formatValue(downtimeTotal ,"string");
        downtimeTotal += this.formatter.declOfNum(downtimeTotal, 
                [" " + this.getText("Hour1"), " " + this.getText("Hour2"), " " + this.getText("Hour3")]);
        return downtimeTotal;
    },

    downtimeFactorChange: function (oEvent) {
    	var oSelectedItem = oEvent.getParameter("selectedItem");
        if(oSelectedItem){
        	var oCntx = oSelectedItem.getBindingContext();	
            this.oEngineModel.setProperty('/downtimeFactor/text', oCntx.getProperty('ZdwntmFcTxt'));
            this.oEngineModel.setProperty('/emergencyBuyTime', oCntx.getProperty('ZpurchTm'));
        }
    },

    onVATLiveChange: function(oEvent){
        var nVATRate = 1.2,
            oModel = this.getModel('engine'),
            oSource = oEvent.getSource(),
            sPath = oSource.mBindingInfos.value.binding.sPath;
          
        if(sPath.indexOf("noNds")!==-1){
            //var nNewValue = oEvent.mParameters.value*nVATRate;
            var nNewValue = (oModel.getProperty(sPath) * nVATRate).toFixed(this._COUNT_BUDGET_DIGITS);
            var sNewPath = sPath.replace("noNds","nds");
            oModel.setProperty(sNewPath,nNewValue);
        }
    },

    numberWithSpaces: function(x) {// Вставляет пробелы после каждыйх 3 символов
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        return parts.join(".");
    },

    visibilityEquipment: function(sRealizationFormId, sZpsPrtyp) {
        return sRealizationFormId !== '8' && sZpsPrtyp !== '4' && sZpsPrtyp !== '3';
    },

    completeProjVisibleFormatter: function(bEdit, bView, oRoles) {
        if (bView) {
            return true;
        }
      
        if (bEdit) {
            if (oRoles.isAdmin || oRoles.isSupervisor) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },

    OzmLastPurchasePriceSuffixFormatter: function(sCurrency) {
        return this.getText('OzmLastPurchasePriceSuffix', [sCurrency]);
    },
    
    transitoryProjectEnableFormatter: function(bUnrealizedProject, bIsAdmin, bIsSupervisor, bIsCurrentApprover) {
        if (bUnrealizedProject) {
            return false;
        } else {
            if (bIsAdmin || bIsSupervisor || bIsCurrentApprover) {
                return true;
            } else {
                return false;
            }
        }
    },
    
    messageStripColor: function(iPts) {
    	var MessageType = sap.ui.core.MessageType;
    	return iPts > 0 && iPts <= 2 ? MessageType.Warning : (iPts >= 3 ? MessageType.Error : MessageType.Success);
    }
};