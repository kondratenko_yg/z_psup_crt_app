jQuery.sap.declare('initiatives.controllers.stepControllers.groupB.riskDetection');

jQuery.sap.require('sap.m.VBox');
jQuery.sap.require('sap.m.Input');
jQuery.sap.require('sap.m.CheckBox');
jQuery.sap.require('sap.ui.layout.form.FormElement');
jQuery.sap.require('sap.ui.core.ValueState');


initiatives.controllers.stepControllers.groupB.riskDetection = {
    typesCheckboxFactoryMain: function (sId, oContext) {
        var checkBoxsPath,
          oEngineContextPath,
          oEngineContext;

        var oData = oContext.getObject();
        var oCheckbox = {};
        if (oData.ZmnlFlg) {

            checkBoxsPath = '/groupB/OTPB/otherType';
            oEngineContextPath = "otherType"; // путь в контекте
            oEngineContext = this.oEngineModel.getContext('/groupB/OTPB/'); //контекст для связи
            oData.sPathText = '/groupB/OTPB/ZpsDntpi'; //Путь до теквтого определения

          oCheckbox = new sap.m.VBox({
            items:[
              new sap.m.CheckBox({
                text: oData.Txtlg,
                selected: '{engine>/groupB/OTPB/otherType}'
              }),
              new sap.m.Input({
                width: '50%',
                editable: '{engine>/groupB/OTPB/otherType}',
                value: '{engine>/groupB/OTPB/ZpsDntpi}'
              })
            ]
          });

        } else {

          checkBoxsPath = '/groupB/checkBoxes/' + oData.ZpsChbx;
          oEngineContextPath = oData.ZpsChbx;
          oEngineContext = this.oEngineModel.getContext('/groupB/checkBoxes');

          oCheckbox = new sap.m.CheckBox({
            text: oData.Txtlg,
            selected: '{engine>/groupB/checkBoxes/' + oData.ZpsChbx + '}'
          });
        }

        //Заполняем связь между значениями

        var isCheckBoxSelected = this.oEngineModel.getProperty(checkBoxsPath) ||
          (this.oEngineModel.setProperty(checkBoxsPath, false) === "");

        var checkBoxesGroup = this.oEngineModel.getProperty('/groupB/checkBoxesG') || {isValidate: false};
        var checkBoxGExtend = {};
        checkBoxGExtend[oData.ZpsChbg] = {};
        checkBoxGExtend[oData.ZpsChbg][oData.ZpsChbx] =  isCheckBoxSelected;
        jQuery.extend(true, checkBoxesGroup, checkBoxGExtend);

        this.oEngineModel.setProperty('/groupB/checkBoxesG', checkBoxesGroup);
        //Выставляем связи
        this.oEngineModel.bindProperty(oEngineContextPath, oEngineContext)
          .attachChange(function(oEvent){
            var oSource = oEvent.getSource();
            var oModel = oSource.getModel();
            var Value = oSource.getValue();
            //UPDATE
            oModel.setProperty('/groupB/checkBoxesG/'+ this.ZpsChbg + "/" +this.ZpsChbx, Value);

            //VALIDATION METHOD
            var oCheckBoxesG = oModel.getProperty('/groupB/checkBoxesG/');
            var result = [];
            jQuery.each( oCheckBoxesG, function( key, oObj ) {
              if(key !== "isValidate") {
                var keyResult = false;
                jQuery.each( oObj, function( key, oObj ) {
                  if(oObj === true){
                    keyResult = true;
                  }
                });
                result.push(keyResult);
              }
            });

            var isValidate = result.every(function(item){
              return item === true;
            });

            //Результат валидации
            oModel.setProperty('/groupB/checkBoxesG/isValidate', isValidate);

          }.bind(oData));

        return new sap.ui.layout.form.FormElement({
          fields: [oCheckbox]
        })
    },
    
    typesRadioButtonFactoryMain: function(sId, oContext) {
        var oData = oContext.getObject();
        var oRadioButton = {};
        if (oData.ZmnlFlg) {
            oRadioButton = new sap.m.VBox({
                items:[
                    new sap.m.RadioButton({
                        text: oData.Txtlg,
                        groupName: "TypeDamage",
                        selected: "{= ${engine>/groupB/OTPB/TypeDamage/id} === '" + oData.ZpsChbx + "' }",
                        select: this.onTypeDamageSelected.bind(this)
                    }),
                    new sap.m.Input({
                        width: '50%',
                        editable: '{engine>/groupB/OTPB/TypeDamage/otherType}',
                        value: '{engine>/groupB/OTPB/TypeDamage/ZpsDntpi}',
                        valueState: '{= ${engine>/groupB/OTPB/TypeDamage/ZpsDntpi} === "" ? "' + sap.ui.core.ValueState.Error + '" : "' + sap.ui.core.ValueState.None + '"}'
                    })
                ]
            });
        } else {
            oRadioButton = new sap.m.RadioButton({
                text: oData.Txtlg,
                groupName: "TypeDamage",
                selected: "{= ${engine>/groupB/OTPB/TypeDamage/id} === '" + oData.ZpsChbx + "' }",
                select: this.onTypeDamageSelected.bind(this)
            });
        }
        return new sap.ui.layout.form.FormElement({
            fields: [oRadioButton]
        });
    },
    
    onTypeDamageSelected: function(oEvent) {
        var oSource = oEvent.getSource();
        var oCurrentStep = this.byId(this.oWizard.getCurrentStep());
        if (oSource) {
            var oContext = oSource.getBindingContext();
            if (oContext) {
                this.oEngineModel.setProperty("/groupB/OTPB/TypeDamage/id", oContext.getProperty("ZpsChbx"));
                this.oEngineModel.setProperty("/groupB/OTPB/TypeDamage/otherType", oContext.getProperty("ZmnlFlg") === 'X');
                if (oContext.getProperty("ZmnlFlg") !== 'X') {
                    this.oEngineModel.setProperty("/groupB/OTPB/TypeDamage/ZpsDntpi", '');
                    this.setStepValidationPath("remove", oCurrentStep, 'engine>/groupB/OTPB/TypeDamage/ZpsDntpi');
                } else {
                    this.setStepValidationPath("add", oCurrentStep, 'engine>/groupB/OTPB/TypeDamage/ZpsDntpi');
                }
            }
        }
    },
    
    onWorkmodeChange: function (oEvent) {
        var oSelectedItem = oEvent.getParameter('selectedItem');
        var sSelectedText = oSelectedItem.getBindingContext().getProperty('Txtmd');
        this.oEngineModel.setProperty('/groupB/ZpsDnwty/text', sSelectedText);
    }
};