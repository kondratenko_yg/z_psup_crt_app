sap.ui.define([
    "sap/m/RadioButton",
    "sap/ui/core/CustomData",
    "sap/m/Text",
    "sap/m/HBox"
], function(RadioButton, CustomData, Text, HBox) {
    "use strict";

    return {
        leftoverRiskFrequencyFactroy: function(sId, oContext) {
            var oObj = oContext.getObject();
            var nFrequency = this.oEngineModel.getProperty("/groupB/leftoverRiskFrequency/id");
            
            var oRadioButton = new RadioButton({
                select: this.onLeftoverRiskFrequencySelect.bind(this),
                selected: oObj.ZpsRskrt == nFrequency,
                groupName: "riskLeftoverFrequency"
            });

            return oRadioButton;
        },

        onLeftoverRiskFrequencySelect: function (oEvent) {
            var bSelected = oEvent.getParameter("selected");
            if(bSelected){
                var oCb = oEvent.getSource();
                var oCntxt = oCb.getBindingContext();
                this.oEngineModel.setProperty('/groupB/leftoverRiskFrequency', {
                    id: oCntxt.getProperty('ZpsRskrt'),
                    points: oCntxt.getProperty('ZpsPts'),
                    text: oCntxt.getProperty('Txtmd')
                });
            }
        },

        leftoverRiskFactory: function (sId, oContext) {
            var oData = oContext.getObject();
            if(oData.text)
                return new Text({
                    text: "{engine>text}"
                    //level: 'H3'
                });

            else {
                var oRadioButton = {};
                if(oData.state !== "red" && oData.state !== "black")
                    oRadioButton = new RadioButton({
                        groupName: "leftoverRiskValue",
                        selected: "{engine>"+oContext.getPath()+"/selected}",
                        select: function (oEvent) {
                            var bSelected = oEvent.getParameter("selected");
                            if(bSelected){
                                var oCb = oEvent.getSource();
                                var nVal = oCb.data("value");
                                this.oEngineModel.setProperty("/groupB/leftoverRiskValue", nVal);

                                var sCatColor = oCb.data("dangerState");
                                var sRiskText, sCateoryText;
                                var sRiskId = "";
                                switch (sCatColor){
                                case "green":
                                    sRiskId = "4";
                                    sRiskText = this.getText("Low");
                                    sCateoryText = this.getText("Acceptable");
                                    break;
                                case "yellow":
                                    sRiskId = "3";
                                    sRiskText = this.getText("Medium");
                                    sCateoryText = this.getText("Marginal");
                                    break;
                                }
                                this.oEngineModel.setProperty("/groupB/leftoverHeaviness", oCb.data("heaviness"));
                                this.oEngineModel.setProperty("/groupB/leftoverPossibility", oCb.data("possibility"));
                                this.oEngineModel.setProperty("/groupB/leftoverRiskText", sRiskText);
                                this.oEngineModel.setProperty("/groupB/leftoverCategoryText", sCateoryText);
                                this.oEngineModel.setProperty("/groupB/leftoverDangerState", sCatColor);
                                this.oEngineModel.setProperty("/groupB/leftoverDangerCatState", sCatColor);
                                this.oEngineModel.setProperty("/groupB/Zpsrrskct", sRiskId);
                            }
                        }.bind(this)
                    }).addCustomData(
                        new CustomData({
                            key: "value",
                            value: "{engine>value}"
                        })
                    ).addCustomData(
                        new CustomData({
                            key: "dangerState",
                            value: "{engine>state}"
                        })
                    ).addCustomData(
                        new CustomData({
                            key: "heaviness",
                            value: "{engine>heaviness}"
                        })
                    ).addCustomData(
                        new CustomData({
                            key: "possibility",
                            value: "{engine>possibility}"
                        })
                    );

                //флаг, обазначающий что имеется значение (при редактировании)
                var sPath = oContext.getPath(),
                    oDescription = this.oEngineModel.getProperty(sPath),
                    bToSelect = (
                        (oData.value == this.oEngineModel.getProperty("/groupB/leftoverRiskValue")) &&
                        (oDescription.heaviness == this.oEngineModel.getProperty("/groupB/leftoverHeaviness")) &&
                        (oDescription.possibility == this.oEngineModel.getProperty("/groupB/leftoverPossibility"))
                    );
                //меня бесит это решение, но лучше не придумал (╯°□°）╯︵ 'a! `%p┻'``p'a! `%p┻'``p
                //При редактировании вызываем обработчик выбора риска чтобы он сам всё заполнил и покрасил
                if(bToSelect)
                    setTimeout(function () {
                        oRadioButton.setSelected(true);
                        oRadioButton.fireSelect({selected: true});
                    });

                return new HBox({
                    justifyContent: "Center",
                    height: "100%",
                    items: [oRadioButton]
                }).addCustomData(
                    new CustomData({
                        key: "dangerState",
                        value: "{engine>state}",
                        writeToDom: true
                    })
                ).addCustomData(
                    new CustomData({
                        key: "selected",
                        value: "{= typeof ${engine>selected} === \"undefined\" ? \"\" : ${engine>selected}.toString()}",
                        writeToDom: true
                    })
                );
            }
        },

        leftoverRiskAmountFormatter: function (riskValue, riskFrequency) {
            var riskAmount = riskValue * riskFrequency;
            this.oEngineModel.setProperty("/groupB/leftoverRiskAmount", riskAmount);
            return riskAmount;
        }
    };
});
