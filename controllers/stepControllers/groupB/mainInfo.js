jQuery.sap.declare('initiatives.controllers.stepControllers.groupB.mainInfo');

jQuery.sap.require('sap.ui.model.Filter');

initiatives.controllers.stepControllers.groupB.mainInfo = {
  onBTargetSubprogChange: function (oEvent) {
      this.onTargetSubprogChange(oEvent);

      this.oEngineModel.setProperty('/groupB/projectType/id', '');
      this.oEngineModel.setProperty('/groupB/projectType/text', '');
      this.oEngineModel.setProperty('/priority/txtsh', '');
      this.oEngineModel.setProperty('/priority/txtlg', '');
      this.oEngineModel.setProperty('/priority/id', '');
  },
  
  onProjectTypeHelpDialog: function(oEvent) {
	if (!this._oProjectTypeHelpDialog) {
		this._oProjectTypeHelpDialog = sap.ui.xmlfragment("initiatives.views.fragments.ProjectTypeDialog", this);
		this.getView().addDependent(this._oProjectTypeHelpDialog);
	}
		
	var oList = this.getDialogContentById(this._oProjectTypeHelpDialog, "projectTypesList");
	var oContext = oList.getBinding('items');
	var sSubprogId = this.oEngineModel.getProperty('/targetSubprog/id');
	oContext.filter([
	  	new sap.ui.model.Filter({
	       	filters: [
	       		new sap.ui.model.Filter({path:'Zsubprog', operator:'EQ', value1: sSubprogId}),
	           	new sap.ui.model.Filter({path:'Zparent', operator:'NE', value1: ""})
	       	],
	       	and: true
	   })
	]);
	    
	this._oProjectTypeHelpDialog.open();
  },
  
  onProjTypeHelpDialogCancel: function(oEvent) {
      this.getDialogContentById(this._oProjectTypeHelpDialog, "projectTypesList").removeSelections();
	  this._oProjectTypeHelpDialog.close();
  },
  
  onProjTypeHelpDialogConfirm: function(oEvent) {
	  var oSelectedItem = this.getDialogContentById(this._oProjectTypeHelpDialog, "projectTypesList").getSelectedItem();
	  
	  if (oSelectedItem) {
		  var oSelectedItemObject = oSelectedItem.getBindingContext().getObject();
		  this.oEngineModel.setProperty('/groupB/projectType/id', oSelectedItemObject.Zpsupid);
		  this.oEngineModel.setProperty('/groupB/projectType/text', oSelectedItemObject.Text);
	  }
	  
	  this._oProjectTypeHelpDialog.close();	
  },
  
  getGroupHeader: function(oGroup) {
	return new sap.m.GroupHeaderListItem({
		title: oGroup.key,
		upperCase: true
	});
  }
};
