jQuery.sap.declare('initiatives.controllers.stepControllers.groupB.ecoRiskDetect');

initiatives.controllers.stepControllers.groupB.ecoRiskDetect = {
  typesEcoCheckboxFactoryMain: function (sId, oContext) {
    var checkBoxsPath,
      oEngineContextPath,
      oEngineContext;

    var oData = oContext.getObject();
    var oCheckbox = {};
    if (oData.ZmnlFlg) {

      if(+oData.ZpsChbg === 4) {

        checkBoxsPath = '/groupB/eco/otherMode';
        oEngineContextPath = "otherMode"; // путь в контекте
        oEngineContext = this.oEngineModel.getContext('/groupB/eco/'); //контекст для связи
        oData.sPathText = '/groupB/eco/ZpsWrkmi'; //Путь до теквтого определения

        oCheckbox = new sap.m.VBox({
          items: [
            new sap.m.CheckBox({
              text: oData.Txtlg,
              selected: '{engine>/groupB/eco/otherMode}'
            }),
            new sap.m.Input({
              width: '50%',
              visible: '{engine>/groupB/eco/otherMode}',
              value: '{engine>/groupB/eco/ZpsWrkmi}'
            })
          ]
        });
      }else if(+oData.ZpsChbg === 5) {

        checkBoxsPath = '/groupB/eco/otherType';
        oEngineContextPath = "otherType"; // путь в контекте
        oEngineContext = this.oEngineModel.getContext('/groupB/eco/'); //контекст для связи
        oData.sPathText = '/groupB/eco/ZpsIncti'; //Путь до теквтого определения

        oCheckbox = new sap.m.VBox({
          items: [
            new sap.m.CheckBox({
              text: oData.Txtlg,
              selected: '{engine>/groupB/eco/otherType}'
            }),
            new sap.m.Input({
              width: '50%',
              visible: '{engine>/groupB/eco/otherType}',
              value: '{engine>/groupB/eco/ZpsIncti}'
            })
          ]
        });
      }



    } else {

      checkBoxsPath = '/groupB/checkBoxes/' + oData.ZpsChbx;
      oEngineContextPath = oData.ZpsChbx;
      oEngineContext = this.oEngineModel.getContext('/groupB/checkBoxes');

      oCheckbox = new sap.m.CheckBox({
        text: oData.Txtlg,
        selected: '{engine>/groupB/checkBoxes/' + oData.ZpsChbx + '}'
      });
    }

      //Заполняем связь между значениями

      var isCheckBoxSelected = this.oEngineModel.getProperty(checkBoxsPath) ||
          (this.oEngineModel.setProperty(checkBoxsPath, false) === "");

      var checkBoxesGroup = this.oEngineModel.getProperty('/groupB/checkBoxesG') || {isValidate: false};
      var checkBoxGExtend = {};
      checkBoxGExtend[oData.ZpsChbg] = {};
      checkBoxGExtend[oData.ZpsChbg][oData.ZpsChbx] =  isCheckBoxSelected;
      jQuery.extend(true, checkBoxesGroup, checkBoxGExtend);

      this.oEngineModel.setProperty('/groupB/checkBoxesG', checkBoxesGroup);
      //Выставляем связи
      this.oEngineModel.bindProperty(oEngineContextPath, oEngineContext)
        .attachChange(function(oEvent){
          var oSource = oEvent.getSource();
          var oModel = oSource.getModel();
          var Value = oSource.getValue();
          //UPDATE
          oModel.setProperty('/groupB/checkBoxesG/'+ this.ZpsChbg + "/" +this.ZpsChbx, Value);

          //VALIDATION METHOD
          var oCheckBoxesG = oModel.getProperty('/groupB/checkBoxesG/');
          var result = [];
          jQuery.each( oCheckBoxesG, function( key, oObj ) {
            if(key !== "isValidate") {
              var keyResult = false;
              jQuery.each( oObj, function( key, oObj ) {
                if(oObj === true){
                  keyResult = true;
                }
              });
              result.push(keyResult);
            }
          });

         var isValidate = result.every(function(item){
            return item === true;
          });

          //Результат валидации
          oModel.setProperty('/groupB/checkBoxesG/isValidate', isValidate);

        }.bind(oData));


    return new sap.ui.layout.form.FormElement({
      fields: [oCheckbox]
    })

  },
};