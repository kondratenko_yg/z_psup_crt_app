sap.ui.define([
    'sap/m/HBox',
    'sap/m/RadioButton',
    'sap/m/Text',
    'sap/ui/core/CustomData',
    'z_psup_shared/GROUPB'
], function(HBox, RadioButton, Text, CustomData, GROUPB) {
    'use strict';
    
    return {
        riskFrequencyFactroy: function (sId, oContext) {
            var oObj = oContext.getObject();
            var nFrequency = this.oEngineModel.getProperty('/groupB/riskFrequency/id');
            
            return new sap.m.RadioButton({
                select: this.onRiskFrequencySelect.bind(this),
                selected: oObj.ZpsRskrt == nFrequency,
                groupName: 'riskFrequency'
            });
        },
        
        onRiskFrequencySelect: function (oEvent) {
            var bSelected = oEvent.getParameter('selected');
            if(bSelected){
                var oCb = oEvent.getSource();
                var oCntxt = oCb.getBindingContext();
                this.oEngineModel.setProperty('/groupB/riskFrequency', {
                    id: oCntxt.getProperty('ZpsRskrt'),
                    points: oCntxt.getProperty('ZpsPts'),
                    text: oCntxt.getProperty('Txtmd')
                });
            }
        },
        
        OTPBRiskFactory: function (sId, oContext) {
            var oData = oContext.getObject();
            if(oData.text){
                return new sap.m.Text({
                    text: '{engine>text}'
                    //level: 'H3'
                });
            } else {
                //флаг, обазначающий что имеется значение (при редактировании)
                var sFunDirId = this.oEngineModel.getProperty('/funcDir/id'),
                    sPath = oContext.getPath(),
                    oDescription = this.oEngineModel.getProperty(sPath),
                    bToSelect = ((oData.value == this.oEngineModel.getProperty('/groupB/riskValue')) &&
                            (oDescription.heaviness == this.oEngineModel.getProperty('/groupB/heaviness')) &&
                            (oDescription.possibility == this.oEngineModel.getProperty('/groupB/possibility')));
                
                var oRadioButton = new sap.m.RadioButton({
                    groupName: 'riskValue',
                    selected: '{engine>'+oContext.getPath()+'/selected}',
                    select: function (oEvent) {
                        var bSelected = oEvent.getParameter('selected');
                        if (bSelected) {
                            var oCb = oEvent.getSource();
                            var oCntxt = oCb.getBindingContext('engine');
                            this.oEngineModel.setProperty('/groupB/riskValue', oCntxt.getProperty('value'));
                            
                            var sCatColor = oCntxt.getProperty('state');
                            var sColor = sCatColor;
                            var sRiskText, sCateoryText;
                            var sRiskId = '';
                            switch (sColor) {
                                case 'green':
                                    sRiskId = '4';
                                    sRiskText = this.getText("Low");
                                    sCateoryText = this.getText("Acceptable");
                                    this.setPriority(GROUPB.PRIORITY.LOW);
                                    break;
                                case 'yellow':
                                    sRiskId = '3';
                                    sRiskText = this.getText("Medium");
                                    sCateoryText = this.getText("Marginal");
                                    this.setPriority(GROUPB.PRIORITY.MEDIUM);
                                    break;
                                case 'red':
                                    sRiskId = '2';
                                    sRiskText = this.getText("HighLev");
                                    sCateoryText = this.getText("Unacceptable");
                                    this.setPriority(GROUPB.PRIORITY.HIGHT);
                                    break;
                                case 'black':
                                    sRiskId = '1';
                                    sRiskText = sFunDirId == '02' ? this.getText("Fatal"):this.getText("Critical");
                                    sCateoryText = this.getText("Unacceptable");
                                    sColor = 'red';
                                    this.setPriority(GROUPB.PRIORITY.FATAL);
                                    break;
                            }
                            this.oEngineModel.setProperty('/groupB/heaviness', oCntxt.getProperty('heaviness'));
                            this.oEngineModel.setProperty('/groupB/possibility', oCntxt.getProperty('possibility'));
                            this.oEngineModel.setProperty('/groupB/ZpsRskct', sRiskId);
                            this.oEngineModel.setProperty('/groupB/riskText', sRiskText);
                            this.oEngineModel.setProperty('/groupB/categoryText', sCateoryText);
                            this.oEngineModel.setProperty('/groupB/dangerState', sCatColor);
                            this.oEngineModel.setProperty('/groupB/dangerCatState', sColor);
                        }
                    }.bind(this)
                });
                
                var oCell = new sap.m.HBox({
                    justifyContent: 'Center',
                    height: '100%',
                    items: [
                        oRadioButton
                    ]
                }).addCustomData(
                    new sap.ui.core.CustomData({
                        key: 'dangerState',
                        value: '{engine>state}',
                        writeToDom: true
                    })
                ).addCustomData(
                    new sap.ui.core.CustomData({
                        key: 'selected',
                        value: '{= typeof ${engine>selected} === "undefined" ? "" : ${engine>selected}.toString()}',
                        writeToDom: true
                    })
                )
            }
            
            //меня бесит это решение, но лучше не придумал (╯°□°）╯︵ 'a! `%p┻'``p'a! `%p┻'``p
            //При редактировании вызываем обработчик выбора риска чтобы он сам всё заполнил и покрасил
            if(bToSelect) {
                setTimeout(function () {
                    oRadioButton.setSelected(true);
                    oRadioButton.fireSelect({
                        selected: true
                    })
                })
            }
            return oCell
        },
        
        /**
         * Форматтер величины риска
         * @param riskValue
         * @param riskFrequency
         * @returns {number}
         */
        riskAmountFormatter: function (riskValue, riskFrequency) {
            var riskAmount = riskValue * riskFrequency;
            this.oEngineModel.setProperty('/groupB/riskAmount', riskAmount);
            return riskAmount;
        },
        
        /**
          * Обработчик чекбокса остаточного риска ОТПБ
          * @param oEvent
          */
        onLeftoverRiskSelect: function (oEvent) {
            var oValuationStep = this.oView.byId('OTPBValuation');
            this.byId('appWizard').discardProgress(oValuationStep);
        },
        
        setPriority: function(sId) {
            var sTargetSubprog = this.oEngineModel.getProperty('/targetSubprog/id');
            if (sTargetSubprog === GROUPB.TARGET_SUBPROG.RISK_MANAGEMENT) {
                this.getPriorities().done(function(aResult) {
                    this.aPriorities = aResult;
                    this.aPriorities.forEach(function(oPriority) {
                        if (oPriority.id === sId) {
                            this.oEngineModel.setProperty('/priority/txtsh', oPriority.txtsh);
                            this.oEngineModel.setProperty('/priority/txtlg', oPriority.txtlg);
                            this.oEngineModel.setProperty('/priority/id', oPriority.id);
                        }
                    }, this);
                }.bind(this));
            }
        },
        
        getPriorities: function() {
            var oPriorityDfd = jQuery.Deferred()
            var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
            var sTargetSubprog = this.oEngineModel.getProperty('/targetSubprog/id');
            this.getView().getModel().read('/PrioritySet', {
                filters: [
                    new sap.ui.model.Filter('ZfuncDir', 'EQ', sFuncDir),
                    new sap.ui.model.Filter('Zsubprog', 'EQ', sTargetSubprog)
                ],
                urlParameters: {
                    $expand: 'ToPr'
                },
                success: function(oData, response) {
                    var aResults = oData.results.map(function(oItem) {
                        return {
                            id: oItem.IdPr,
                            txtsh: oItem.ToPr.Txtlg,
                            txtlg: oItem.Txtlg
                        };
                    });
                    oPriorityDfd.resolve(aResults);
                }
            });
            return oPriorityDfd.promise();
        }
    }
});

