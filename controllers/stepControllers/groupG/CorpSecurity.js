sap.ui.define([
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/ListItem"
], function (Filter, FilterOperator, ListItem) {
    "use strict";
	
	return {
		corpSec: {
			eventHandler: {
		    	onChangeZpsuppodp: function(oEvent) {
		    		var oSelectedItem = oEvent.getParameter("selectedItem"),
		    			oBindingContext = oSelectedItem.getBindingContext();
		    		
		    		if (oBindingContext) {
		    			this.oEngineModel.setProperty("/groupG/Zpsuppodp/text", oBindingContext.getProperty("Txtmd"));
		    			var oParameters = oEvent.getParameter("oParameters");
		    			if (!oParameters || !oParameters.isLoadedProject) {
			    			this.oEngineModel.setProperty("/groupG/Zpsuppr/id", "");
			    			this.oEngineModel.setProperty("/groupG/Zpsuppr/text", "");
		    			}
		    			
		    			var oSelPr = sap.ui.getCore().byId("selPr");
		    			if (oSelPr) {
		    				oSelPr.bindAggregation("items", {
		    			    	path: "/PrSet",
		    			    	events:{
                    				dataReceived: this.corpSec.eventHandler.onZpsupprDataReceived.bind(this)
                    			},
		    			        filters: [new Filter("Podp", FilterOperator.EQ, oBindingContext.getProperty("Id"))],
		    			        template: new ListItem({
		    			        	key: "{Id}",
		    			        	text: "{Txtlg}"
		    			        })
		    				});
		    			}
		    		}
		    	},
		    	
		    	onChangeZpsuppr: function(oEvent) {
		    		var oSelectedItem = oEvent.getParameter("selectedItem"),
						oBindingContext = oSelectedItem.getBindingContext();
		    		
		    		if (oBindingContext) {
		    			this.oEngineModel.setProperty("/groupG/Zpsuppr/text", oBindingContext.getProperty("Txtlg"));
		    		}
		    	},
		    	
		    	onZpsupprDataReceived: function(oEvent) {
		    		var oData = oEvent.getParameter("data");
		    		var bResult = false;
		    		
		    		if (oData && oData.results.length > 0) {
		    			bResult = true;
		    		}
		    		
	    			this.oEngineModel.setProperty("/groupG/Zpsuppr/visible", bResult);
	    			
	    			this.getView().byId('GroupG').bindProperty('validated', {
	                	parts: this.corpSec.stepValidation.inProcess.call(this),
	                	formatter: this.validators.simpleValidator
	                });
		    	}
			},
			
			stepValidation: {
	    		inProcess : function() {
	    			var aBinding = [ 
	    				"engine>/realizationForm/id",
	                    "engine>/riskDecr",
	                    "engine>/groupG/Zpsuppodp/id"
		    		];
	    			
	    			if (this.oEngineModel.getProperty("/groupG/Zpsuppr/visible")) {
	    				aBinding.push("engine>/groupG/Zpsuppr/id");
	    			}
	    			
	    			return aBinding;
	    		}
            }
		}
    };
    
}, true);