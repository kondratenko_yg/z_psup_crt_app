jQuery.sap.declare('initiatives.controllers.stepControllers.groupD.generalInfo');

jQuery.sap.require('sap.m.ColumnListItem');
jQuery.sap.require('sap.m.Select');
jQuery.sap.require('sap.m.Text');
jQuery.sap.require('sap.ui.core.ListItem');

initiatives.controllers.stepControllers.groupD.generalInfo = {

	onProjectCritChange: function (oEvent) {
		var oSelectedItem = oEvent.getParameter('selectedItem');
		if (oSelectedItem) {
			var oBindingContext = oSelectedItem.getBindingContext();
			
			this.oEngineModel.setProperty('/groupD/prjcr/text', oBindingContext.getProperty('Txtmd'));
			this.oEngineModel.setProperty('/groupD/prjcr/pts', oBindingContext.getProperty('ZpsPts'));
		}
	},
	
	onSubprogBindingChange: function () {
		if(this._bEdit) {
			var oSubprogSelect = sap.ui.getCore().byId('groupDSubprog');
			oSubprogSelect.fireChange({
				selectedItem: oSubprogSelect.getSelectedItem(),
				edit: true
			});
		}
	},
	
	onGroupDSubprogChange: function (oEvent) {
		var oSelectedItem = oEvent.getParameter('selectedItem');
		if (oSelectedItem) {
			var oBindingContext = oSelectedItem.getBindingContext();
			
			var groupId = oBindingContext.getProperty('ZpsGrp');
			var subprogId = oBindingContext.getProperty('Zsubprog');
			var oGroupDStep = this.getView().byId('GroupD');
			
			oGroupDStep.bindProperty('validated', {
				parts: this._oValidationBinding.GroupD.inProcess(subprogId),
				formatter: this.validators.simpleValidator
			});
			
			if (groupId != "0"){
				this.oEngineModel.setProperty('/groupD/groupId', groupId);
			}
			this.oEngineModel.setProperty('/groupD/subprogText', oBindingContext.getProperty('Txtlg'));
			
			var oTypeSelect = sap.ui.getCore().byId('groupDTypeSelect');
			oTypeSelect.getBinding('items').filter(
					new sap.ui.model.Filter({
						path: 'Zsubprog',
						operator: 'EQ',
						value1: subprogId
					})
			);
			
			var oTable = sap.ui.getCore().byId('ZPs_tpcrTable');
			oTable.getBinding('items').filter(new sap.ui.model.Filter('Zsubprog', 'EQ', ''));
			
			if(groupId == 0 || subprogId=='006' || subprogId=='007' || subprogId=='008') {
				this.oEngineModel.setProperty('/groupD/STPRate', 10);
				var oSTPRateText = sap.ui.getCore().byId('STPRateText');
				oSTPRateText.bindText({path:'engine>/groupD/STPRate'})
			}
			
			var bInitialEdit = oEvent.getParameter('edit');
			//если это не изначальное проставление данных для редактировании
			//очищаем селект типов и список критериев
			if(!bInitialEdit){
				oTypeSelect.setSelectedKey('');
				this.oEngineModel.setProperty('/groupD/STP', {});
				this.oEngineModel.setProperty('/groupD/STPVals', {});
			}
		}
	},

  /**
   * Обработчик смены типа проекта
   * @param oEvent
   */
  onProjectTypeChange: function (oEvent) {
	  var oSelItem = oEvent.getParameter('selectedItem');
	  if (oSelItem) {
		  var oContext = oSelItem.getBindingContext();
		  if (oContext) {
			  this.oEngineModel.setProperty('/groupD/prjty/text', oContext.getProperty('Txtmd'));
			  this.oEngineModel.setProperty('/groupD/prjty/descr', oContext.getProperty('Txtlg'));
		  }
	  }
  },

  onSubprogDataRecived: function (oEvent) {
	  if(this._bEdit){
		  var oTypesSelect = sap.ui.getCore().byId('groupDTypeSelect'),
		  	  oSelectedItem = oTypesSelect.getSelectedItem();
		  if (oSelectedItem) {
		      oTypesSelect.fireChange({
		    	  selectedItem: oSelectedItem,
		          edit: true
		      });
		  }
	  }
  },

  groupFormatter: function (groupId) {
    var sgroupText = '';
    switch (groupId){
      case '1':
        sgroupText = this.getText("GrA"); 
        var oEstimStep = this.oView.byId('EstimateProbability');
        this.oView.byId('GroupD').setNextStep(oEstimStep);
        break;
      case '2':
        sgroupText = this.getText("GrB"); 
        var oEstimStep = this.oView.byId('EstimateProbabilityB');
        this.oView.byId('GroupD').setNextStep(oEstimStep);
        break;
    }

    this.oEngineModel.setProperty('/groupD/groupText', sgroupText);
    this.oEngineModel.setProperty('/groupD/groupId', groupId);
    return sgroupText;
  },

  /**
   * Обработчик смены Тип оборудования / ПО
   * @param oEvent
   */
  onZpsEstypChange: function (oEvent) {
    var sZpsEstyp = oEvent.getParameter('selectedItem').getKey();
    var sZpsEstypText = oEvent.getParameter('selectedItem').getText();

    this.oEngineModel.setProperty('/groupD/poTypeText', sZpsEstypText);

    var oTable = sap.ui.getCore().byId('ZPs_tpcrTable');
    var sSubprogId = this.oEngineModel.getProperty('/groupD/Zsubprog');
    var oBinding = oTable.getBinding('items');
    oBinding.filter(
      [
        new sap.ui.model.Filter({
          path: 'ZpsEstyp',
          operator: 'EQ',
          value1: sZpsEstyp
        }),
        new sap.ui.model.Filter({
          path: 'Zsubprog',
          operator: 'EQ',
          value1: sSubprogId
        })
      ],
      true
    )
    var bInitialEdit = oEvent.getParameter('edit');
    //если это не изначальное проставление данных для редактировании
    //очищаем список критериев
    if(!bInitialEdit){
      this.oEngineModel.setProperty('/groupD/STP', {});
      this.oEngineModel.setProperty('/groupD/STPVals', {});
    }
  },

  /**
   * Фабрика строк таблицы "Соответствие Технической политике"
   * @param sId
   * @param oContext
   * @constructor
   */
  ZPs_tpcrTableFactroy: function (sId, oContext) {
    var bIsReview = /Review/.test(sId);
    var oObj = oContext.getObject();
    return new sap.m.ColumnListItem({
      cells:[
        new sap.m.Text({text: oObj.Txtmd, width: '100%'}),
        new sap.m.Text({text: oObj.Txtlg, width: '100%'}),
        new sap.m.HBox({
          items:[
            new sap.m.Select({
              enabled: !bIsReview,
              selectedKey: '{engine>/groupD/STP/' + oObj.ZpsTpcr + '}',
              items: Array.apply(0, Array(11)).map(function(item, index) {
                return new sap.ui.core.ListItem({key: index, text: index}).addCustomData(new sap.ui.core.CustomData({key:'points', value:index * oObj.ZpsWght}));
              }),
              change: function (oEvent) {
                this.oEngineModel.setProperty('/groupD/STPVals/' + oObj.ZpsTpcr, +oEvent.getParameter('selectedItem').getText());
                this.oEngineModel.setProperty('/groupD/STPPoints/' + oObj.ZpsTpcr, +oEvent.getParameter('selectedItem').data('points'));
                this._groupDReviewBinded = false;
                
              //Сравнение количества строк в таблице и количество заполненых строк.
                var oMas = this.oEngineModel.getProperty('/groupD/STPPoints');
                var nCountPoints = Object.keys(oMas).length;
                var nTabCount = sap.ui.getCore().byId('ZPs_tpcrTable').getItems().length;            	
            	if (nTabCount === nCountPoints){
            		this.oEngineModel.setProperty('/groupD/STPFullTab', '1');
            		}
            	else
            		{
            		this.oEngineModel.setProperty('/groupD/STPFullTab', '');
            		}
                
              }.bind(this)
            }).addStyleClass('sapUiSmallMarginEnd'),
            new sap.m.RatingIndicator({
              enabled: false,
              maxValue: 10,
              value: '{engine>/groupD/STPVals/' + oObj.ZpsTpcr + '}'
            })
          ]
        })
      ]
    })
  },

  /**
   * Обработчик изменения значений биндинга таблицы "Соответствие Технической политике"
   * @param oEvent
   */
  onZPs_tpcrTableBindingChange: function (oEvent) {
	  var oTable = sap.ui.getCore().byId('ZPs_tpcrTable'),
	  	  aItems = oTable.getItems(),
	  	  aBindingContexts = aItems.map(function (item) {
	  		  var oSelect = item.getCells()[2].getItems()[0];
	  		  if(this._bEdit){
	  			  oSelect.fireChange({
	  				  selectedItem: oSelect.getSelectedItem()
	  			  });
	  		  }
	  		  return {path: 'engine>' + oSelect.getBinding('selectedKey').getPath().replace('STP', 'STPPoints')};
	  	  }.bind(this)),
	  	  oSTPRateText = sap.ui.getCore().byId('STPRateText');
	  
	  if (aBindingContexts.length > 0) {
		  oSTPRateText.bindText({
			  parts: aBindingContexts,
			  formatter: function () {
				  var aValues = Array.prototype.slice.call(arguments);
				  var rateSum = aValues.reduce(function (sum, val) {
					  return sum += (+val || 0);
				  }, 0);
	        
				  if('{engine>/groupD/Zsubprog}' == '006' || '{engine>/groupD/Zsubprog}' == '007' || '{engine>/groupD/Zsubprog}' == '008'){
					  rateSum = 10; // если ФН 6,7,8 и таблица не отображается
				  }else{
					  rateSum = rateSum.toFixed(1); // когда таблица отображается
				  }
				  this.oEngineModel.setProperty('/groupD/STPRate', rateSum);
				  return rateSum
			  }.bind(this)
		  });
	  }
	  this._groupDReviewBinded = false;
  },


  /**
   * Функция для подсчета количества строк в таблице расчета соответствия Технической политике
   *
   */
 
  
  /**
   * форматтер таблицы итоговых рейтиногов по группе Д
   * @param estimate - вероятность отказа
   * @param risk - критичность для бизнеса
   * @param str - соответствие технической политике
   */
  dResultFormatter: function (estimate, risk, str, year) {
	  var fZpsPpjrt = ((risk * 0.5) + ((1+(+estimate/100)*9) * 0.3) + (str * 0.2)).toFixed(1),
	  		mYears = this.oEngineModel.getProperty('/years');
	  mYears.forEach(function(item, i, arr) {
		  if (item.year === year) {
			  this.oEngineModel.setProperty('/years/' + i + '/groupD/ZpsPpjrt', fZpsPpjrt);
		  }
	  }.bind(this));
	  return fZpsPpjrt;
  },

  onZPs_tpcrReviewTableBindingChange: function (oEvent) {
    if(!this._groupDReviewBinded){
      var sSubprogId = this.oEngineModel.getProperty('/groupD/Zsubprog');
      var sZpsEstyp = this.oEngineModel.getProperty('/groupD/ZpsEstyp');
      oEvent.getSource().filter(
        [
          new sap.ui.model.Filter({
            path: 'ZpsEstyp',
            operator: 'EQ',
            value1: sZpsEstyp
          }),
          new sap.ui.model.Filter({
            path: 'Zsubprog',
            operator: 'EQ',
            value1: sSubprogId
          })
        ],
        true
      )
    }
    this._groupDReviewBinded = true;
  }

};