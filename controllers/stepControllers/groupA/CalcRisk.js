sap.ui.define([
	"sap/ui/model/Filter",
	"sap/m/Label",
	"sap/m/Input",
	"sap/m/Text",
	"sap/ui/layout/form/FormElement"
], function(Filter, Label, Input, Text, FormElement) {
	"use strict";
	
	return {
		/**
		 * Фабрика по созданию инпутов для ручного ввода вероятности отказа
		 * @param sId
		 * @param oContext
		 */
		breakChanceInputFactory: function (sId, oContext) {
			return new FormElement({
				label: new Label({text: "{engine>year}"}),
				fields: [
					new Input({
						value: {
							path:'engine>breakChance',
							type:'sap.ui.model.type.Float',
							formatOptions:{
								maxIntegerDigits: 3,
								maxFractionDigits: 2
							}
						},
						change: function(oEvent){
							if (oEvent.mParameters.value>100) {
								this.setValue(100);
							}
						}
					}).addEventDelegate({
						onfocusin: function(oEvent) {
							if(+oEvent.target.value == 0) {
								oEvent.target.value = '';
							}
						}
					}),
					new Text({
						text:'%'
					})
				]
			})
		}
	};
});