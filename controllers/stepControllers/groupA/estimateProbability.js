jQuery.sap.declare('initiatives.controllers.stepControllers.groupA.estimateProbability');

initiatives.controllers.stepControllers.groupA.estimateProbability = {
  estimateProbability: {
    onInit: function () {
      this.estimateProbability.setNextStep.apply(this, arguments);
    },
    setNextStep: function (oEvent) {
      var oView = this.getView();
      var oSource = oEvent.getSource();
      var funcDir = this.oEngineModel.getProperty('/funcDir/id');
      switch (funcDir) { // Постоянное значение true вместо foo
        case "07":
            //TODO Если нужно
          break;
      }
    }
  },
  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "История использования оборудования"
   * @param oEvent
   */
  onHistoryChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('history');
    }
  },

  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "Техническое состояние"
   * @param oEvent
   */
  onTechChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('techSost');
    }
  },

  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "Статистика ТО и ППР"
   * @param oEvent
   */
  onToPPPRChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('toppr');
    }
  },

  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "Статистика простоев"
   * @param oEvent
   */
  onStatDelayChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('statDelay');
    }
  },

  /**
   * Пересчет таблицы из опросника вероятности отказа
   * @param sTableProperty
   * @private
   */
  _rebuildTableAnswers: function (sTableProperty) {
    var aYears = this.oEngineModel.getProperty('/years');

    var limit = -1; //ограничитель последующих строк
    var bRelocate = false; //необходимость сдвинуть выбор показателя следующих годов если предыдущий стал больше

    aYears.forEach(function (year, index) { //идём по ккаждой строке (году)
      if(year.groupA[sTableProperty]){
        year.groupA[sTableProperty].answers.forEach(function (answer) { //идём по каждому варианту ответа
          if (answer.value >= limit) { //если значение ячейки привышает или равно лимиту
            if (bRelocate) { //если необходимо сдвинуть ячейку
              answer.selected = true;
              bRelocate = false;
            }
            answer.editable = true;
          } else {
            answer.editable = false;
            if (answer.selected) {
              answer.selected = false;
              bRelocate = true;
            }
          }
          if (answer.selected) {
            limit = answer.value;
            year.groupA[sTableProperty].selectedValue = answer.value;
            year.groupA[sTableProperty].selected = true;
            this._recountProbability(index)
          }
        }.bind(this))
      }
    }.bind(this));
    this.oEngineModel.updateBindings();
  },

  /**
   * Вычисление процента вероятности отказа исходя из опросника по годам
   * @param index
   * @private
   */
  _recountProbability: function (index) {
    var oYear = this.oEngineModel.getProperty('/years/' + index);
    var nProbability =
      oYear.groupA.history.selectedValue +
      oYear.groupA.techSost.selectedValue +
      oYear.groupA.toppr.selectedValue +
      oYear.groupA.statDelay.selectedValue;

    var percentOverall = 0;
    switch (nProbability) {
      case 0:
        percentOverall = 0;
        break;
      case 1:
        percentOverall = 1.14957148955805;
        break;
      case 2:
        percentOverall = 1.81029398575989;
        break;
      case 3:
        percentOverall = 2.85077034759998;
        break;
      case 4:
        percentOverall = 4.48926618476499;
        break;
      case 5:
        percentOverall = 7.06949645896286;
        break;
      case 6:
        percentOverall = 11.1327281845963;
        break;
      case 7:
        percentOverall = 17.5313245507005;
        break;
      case 8:
        percentOverall = 27.6075491474992;
        break;
      case 9:
        percentOverall = 43.4751388993668;
        break;
      case 10:
        percentOverall = 68.4627125798472;
        break;
      case 11:
        percentOverall = 90.00;
        break;
      case 12:
        percentOverall = 99.00;
        break;
      default:
        percentOverall = -1;
    }

    this.oEngineModel.setProperty('/years/' + index + '/breakChance', percentOverall)
  }

};