sap.ui.define([
    'sap/m/Button',
    'sap/m/Text',
    'sap/m/StandardListItem',
    'sap/m/GroupHeaderListItem',
    'sap/ui/core/CustomData',
    'sap/m/Dialog',
    'z_psup_shared/FUNCTIONAL_AREAS'
], function(Button, Text, StandardListItem, GroupHeaderListItem, CustomData, Dialog, FUNCTIONAL_AREAS) {
    return {
        onPrtypeChange: function (oEvent) {
            if(!this._bGroupARebuildRequired){
                this._handlePrtypeChange(oEvent);
            } else{
                this._showGroupADiscardDialog(oEvent);
            }
        },
        
        _showGroupADiscardDialog: function (oEvent) {
            var oSelect = oEvent.getSource();
            var origEvent = jQuery.extend(true,{},oEvent);
            var dialog = new Dialog({
                title: this.getText('Attention'),
                type: 'Message',
                content: new Text({ text: this.getText('ChangeTypeQuiz') }),
                beginButton: new Button({
                    text: this.getText('YES'),
                    press: function () {
                        this.byId('appWizard').discardProgress(this.byId('GroupA'));
                        this._handlePrtypeChange(origEvent);
                        this._bGroupARebuildRequired = false;
                        dialog.close();
                    }.bind(this)
                }),
                endButton: new Button({
                    text: this.getText('NO'),
                    press: function () {
                        oSelect.setSelectedKey(this._sCashedZpsPrtyp);
                        dialog.close();
                    }.bind(this)
               }),
               afterClose: function() {
                   dialog.destroy();
               }
            });
            dialog.open();
        },
        
        _handlePrtypeChange: function (oEvent) {
            var oSelectedItem = oEvent.getParameter('selectedItem');
            var oContext = oSelectedItem.getBindingContext();

            this.oEngineModel.setProperty('/realizationForm/id', oContext.getProperty('ZimplFrm'));
            this.oEngineModel.setProperty('/realizationForm/name', oContext.getProperty('NavToImplForm/Txtmd'));
            this.oEngineModel.setProperty('/realizationForm/shortName', oContext.getProperty('NavToImplForm/Txtsh'));

            this._setNextStepA(true);
        },
              
        /**
         * Валидатор времени простоя введенног вручную.
         * не пустое и один знак после запятой.
         * @param oEvent
         * @returns {boolean}
         * @private
        */
        _validateDowntimePenalty: function (oEvent) {
            var oInput = oEvent.getSource();
            var value = oEvent.getParameter('value');
            
            if (!value) {
                oInput.setValueState('Error').setValueStateText(this.getText('RequiredField'));
                return;
            }
            
            var reDecimal = /^\d*(\.\d{1,2})?\d{0}$/;
            if (reDecimal.test(value)) {
                oInput.setValueState('None').setValueStateText(this.getText('GoodInpt'));
            } else {
                oInput.setValueState('Error').setValueStateText(this.getText('WrongInpt'));
            }
        },
        
        onTSOSelect: function (oEvent) {
            var bIsSelected = oEvent.getParameter('selected');
            
            if (bIsSelected) {
                this.byId('BudgetStep').setNextStep(null);
            } else {
                this.byId('BudgetStep').setNextStep('EstimateProbability');
            }
        },
        
        onEquipValueHelp: function () {
            if (!this._oEquipDialog) {
                this._oEquipDialog = sap.ui.xmlfragment('initiatives.views.fragments.EquipSelectDialog', this);
                this._oEquipDialog.setModel(this.getView().getModel());
                this._oEquipDialog.setModel(this.oEngineModel, 'engine');
                this.getView().addDependent(this._oEquipDialog);
            }
            
            //сборс строки поиска
            this.oEngineModel.setProperty('/groupA/equip/searchText', '');
            
            var model = this.getView().getModel();
            
            model.read("/ZEquipIdSet('" + this.oEngineModel.getProperty('/workshop/id') + "')", {
                success: function (oData) {
                    var oTable = sap.ui.getCore().byId('equipTable');
                    oTable.bindRows({
                        path: "/ZEquipSet('" + oData.Id + "')",
                        parameters: {
                            navigation: {
                                'ZEquipSet': 'ChildNodes'
                            }
                        }
                    });
                    
                    this._oEquipDialog.open();
                }.bind(this)
            });
        },
        
        onEquipElderyChange: function (oEvent) {
            var selectedIndex = oEvent.getParameter('selectedIndex');
            var oEquipInput = sap.ui.getCore().byId('equipInput');
            
            if (selectedIndex === 0) {
                oEquipInput.setValue('');
                this.oEngineModel.setProperty('/groupA/equip/manual', this.oEngineModel.getProperty('/manualWorkshop'));
            } else {
                this.oEngineModel.setProperty('/groupA/equip/manual', true);
            }
        },
        
        onAggregateElderyChange: function (oEvent) {
            var selectedIndex = oEvent.getParameter('selectedIndex');
            
            this.oEngineModel.setProperty('/groupA/aggregate/name', '');
            this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/units', 0);
            this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/thousands', 0);
            this.oEngineModel.setProperty('/groupA/aggregate/id', '');
            
            if (selectedIndex === 0) {
                this.oEngineModel.setProperty('/groupA/aggregate/manual', false);
                this.oEngineModel.setProperty('/groupA/aggregate/downtimeManual', false);
            } else {
                this.oEngineModel.setProperty('/groupA/aggregate/manual', true);
                this.oEngineModel.setProperty('/groupA/aggregate/downtimeManual', true);
            }
        },
        
        onAggregateValueHelp: function () {
            if (!this._oAggregateDialog) {
                this._oAggregateDialog = sap.ui.xmlfragment('initiatives.views.fragments.AggregateSelectDialog', this);
                this.getView().addDependent(this._oAggregateDialog);
            }
            
            this._oAggregateDialog.bindAggregation('items', {
                path: '/ZPs_aggrSet',
                template: new StandardListItem({
                    title: '{Txtlg}',
                    description: '{ZpsAggr}',
                    type: 'Active'
                }).addCustomData(new CustomData({
                    key: 'downtime',
                    value: '{ZdwntmPc}'
                }))
            });
            
            this._oAggregateDialog.getBinding('items').filter([this._getFiltersForAggregate()]);
            this._oAggregateDialog.open();
        },
        
        onAggregateEntityChange: function (oEvent) {
            var oSelectedItem = oEvent.getParameter('selectedItem');
            var sAggrName = oSelectedItem.getTitle();
            var sAggrId = oSelectedItem.getDescription();
            var nDowntimePenalty = oSelectedItem.data('downtime');
            
            this.oEngineModel.setProperty('/groupA/aggregate/name', sAggrName);
            this.oEngineModel.setProperty('/groupA/aggregate/id', sAggrId);
            this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/units', +nDowntimePenalty);
            this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/thousands', +(+nDowntimePenalty / 1000).toFixed(3));
            
            //если не указано время простоя агрегата, показываем поля для ввода вручную
            this.oEngineModel.setProperty('/groupA/aggregate/downtimeManual', +nDowntimePenalty === 0);
        },
        
        onEquipSelect: function (oEvent) {
            var sPath = oEvent.getParameter('rowContext').getPath();
            var oModel = this.getView().getModel();
            var oRowData = oModel.getProperty(sPath);
            
            this.oEngineModel.setProperty('/groupA/equip/id', oRowData.Name);
            this.oEngineModel.setProperty('/groupA/equip/name', oRowData.Text)
        },
        
        onEquipDialogClosePress: function (oEvent) {
            sap.ui.getCore().byId('equipTable').getBinding('rows').filter([]);
            this._oEquipDialog.close();
        },
        
        onCritChange: function (oEvent) {
            var oSelectedItem = oEvent.getParameter('selectedItem');
            if (oSelectedItem) {
                var oContext = oSelectedItem.getBindingContext();
                this.oEngineModel.setProperty('/groupA/crit/text', oContext.getProperty('Text'));
                this.oEngineModel.setProperty('/groupA/crit/coefficient', oContext.getProperty('Zkoef'));
            }
        },
        
        onFsTypeHelpDialog: function(oEvent) {
            if (!this._oFsTypeHelpDialog) {
                this._oFsTypeHelpDialog = sap.ui.xmlfragment('initiatives.views.fragments.FireSafetyTypeDialog', this);
                this.getView().addDependent(this._oFsTypeHelpDialog);
            }
            
            this._oFsTypeHelpDialog.open();
        },
        
        onFsTypeHelpDialogCancel: function(oEvent) {
            this.getDialogContentById(this._oFsTypeHelpDialog, 'fsTypesList').removeSelections();
            this._oFsTypeHelpDialog.close();
        },
        
        onFsHelpDialogConfirm: function(oEvent) {
            var oSelectedItem = this.getDialogContentById(this._oFsTypeHelpDialog, 'fsTypesList').getSelectedItem();
            
            if (oSelectedItem) {
                var oSelectedItemObject = oSelectedItem.getBindingContext().getObject();
                this.oEngineModel.setProperty('/groupA/FsType/id', oSelectedItemObject.Zpsupid);
                this.oEngineModel.setProperty('/groupA/FsType/text', oSelectedItemObject.Text);
            }
            
            this._oFsTypeHelpDialog.close();    
        },
        
        getGroupHeader: function(oGroup) {
            return new GroupHeaderListItem({
                title: oGroup.key,
                upperCase: true
            });
        },
        
        onFireSafetySelect: function(oEvent) {
            var bSelected = oEvent.getParameter('selected');
            var oGroupA = this.byId('GroupA');
            var oGroupAValidBindingInfo = oGroupA.getBindingInfo('validated');
            
            this.oEngineModel.setProperty('/groupA/targetSubprogram', bSelected ? this.getText('FireSafety') : '');
            
            var oValidateObject = {
                parts: oGroupAValidBindingInfo.parts.map(function(item) {
                    return {
                        path: item.model + '>' + item.path
                    }
                }),
                formatter: oGroupAValidBindingInfo.formatter
            };
            var aDelPaths = [];
            var bProduction = this.oEngineModel.getProperty('/funcDir/id') === FUNCTIONAL_AREAS.GROUPA.PRODUCTION;
            
            if (bSelected) {
                if (bProduction) {
                    aDelPaths.push('engine>/targetSubprog/id');
                }
                oValidateObject.parts.push({path: 'engine>/groupA/FsType/id'});
                oValidateObject.parts.push({path: 'engine>/priority/id'});
                
                this.oEngineModel.setProperty('/targetSubprog/id', '');
                this.oEngineModel.setProperty('/targetSubprog/name', '');
            } else {
                if (bProduction) {
                    aDelPaths.push('engine>/groupA/FsType/id');
                    aDelPaths.push('engine>/priority/id');
                }
                oValidateObject.parts.push({path: 'engine>/targetSubprog/id'});
                
                this.oEngineModel.setProperty('/groupA/FsType/id', '');
                this.oEngineModel.setProperty('/groupA/FsType/text', '');
                this.oEngineModel.setProperty('/priority/id', '');
                this.oEngineModel.setProperty('/priority/txtsh', '');
                this.oEngineModel.setProperty('/priority/txtlg', '');
            }
            oGroupA.bindProperty('validated', this._removeValidatePath(oValidateObject, aDelPaths));
        },
        
        _removeValidatePath: function(oValidateObject, aPaths) {
            oValidateObject.parts.forEach(function(item, index, arr) {
                if ( aPaths.indexOf(item.path) !== -1) {
                    arr.splice(index, 1);
                }
            });
            return oValidateObject;
        },
        
        onCostHourInactivityChange: function(oEvent) {
            var sValue = oEvent.getParameter('value').replace(' ', '').replace(',', '.');
            var nValue = +sValue;
            
            if(!isNaN(nValue)) {
                this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/thousands', nValue);
                this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/units', nValue * 1000);
            } else {
                this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/thousands', 0);
                this.oEngineModel.setProperty('/groupA/aggregate/downtimePenalty/units', 0);
            }
        }
    };
});
