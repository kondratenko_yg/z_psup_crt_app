sap.ui.define([], function () {

  var selfObj = {
    labControl: {
      onAfterFragentLoad: function () {


      },
      onInit: function () {
        var sProjectname = this.oEngineModel.getProperty('/projectNumber');

        if (!sProjectname) {
          var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
          var sCompanyId = this.oEngineModel.getProperty('/company/id').substr(2);
          var prefix = sCompanyId + '_' + sFuncDir;
          this.oView.getModel()
            .read("/ZProjprostfixSet('" + prefix + "')", {
              success: function (oData) {

                var sFullName = prefix + '_' + oData.Zpostfix;

                this.oEngineModel.setProperty('/projectNumber', sFullName);
              }.bind(this)
            })
        }
      },
      formatter: {},
      factory: {},
      eventHandler: {
        ZeqCat: function (oEvent) {
          
        	var oStep = this.getView().byId('GroupV');
        	var ZeqCatKey = oEvent.getParameter('selectedItem').getKey();
        	
        	//По категории 1 вероятность отказа всегда 100%
         	if (ZeqCatKey == '1') {
         		this.oEngineModel.setProperty('/groupV/ZfailPr', 100);
         	}
         	else {
         		this.oEngineModel.setProperty('/groupV/ZregDoc', "");
         	}
         	
         	oStep.bindProperty('validated', {
                parts: this._oValidationBinding.GroupV.inProcess(ZeqCatKey),
                formatter: this.validators.simpleValidator
            });

         	var MessageStrip = sap.ui.getCore().byId('labControl-ZeqCat-Txtlg');
         	!MessageStrip.getVisible() ? MessageStrip.setVisible(true) : '';
         	MessageStrip.setText(oEvent.getSource().getSelectedItem().getAdditionalText());
         	this.oEngineModel.setProperty('/groupV/ZeqCat/text', oEvent.getParameter('selectedItem').getText());
        }
        
      },
      stepValidation: function () {

      }
    }
  };

  return selfObj;
}, true);