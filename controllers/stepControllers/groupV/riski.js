sap.ui.define([], function () {

    var selfObj = {
        riski: {
            onAfterFragentLoad: function() {

            },
            onInit: function(){
                //HARDCODE
                var serviceModel =  this.getView().getModel();
                serviceModel.oEngineModel = this.oEngineModel;
            },
            formatter: {
                ZfailPr: function() {
                    var aPts = Array.prototype.slice.call(arguments, 0, arguments.length / 2);
                    var aMax = Array.prototype.slice.call(arguments, arguments.length / 2);
                    var sumMax = aMax.reduce(function (sum, val) {
                        return sum += ( +val == +val ) ? +val : 0;
                    }, 0);
                    var sumValues = aPts.reduce(function (sum, val) {
                        return sum += ( +val == +val ) ? +val : 0;
                    }, 0);
                    var retValue = (sumValues / sumMax) * 100;
                    //По категории 1 вероятность отказа всегда 100%
                    if(this.oEngineModel.getProperty('/groupV/ZeqCat/id') == '1') retValue = 100;
                    //Сохраняем значение в модель
                    this.oEngineModel.setProperty('/groupV/ZfailPr',retValue);
                    return this.riski.formatter.ZfailPrFormatter.apply(this, [retValue]);
                },
                
                ZfailPrFormatter: function(retValue) {
                	return this.formatter.floatType.formatValue(retValue ,"string");
                }
                
            },
            factory: {

            },
            eventHandler:{
                setMaxOfValues: function(oEvent){
                    var oSource = oEvent.getSource();
                    var aValues = oSource.getContexts().map(function(oContext){return oContext.getObject().ZpsPts});
                    var vMax = Math.max.apply( Math, aValues);
                    var vOrder = oSource.aApplicationFilters[0].oValue1;
                    var aPath = [
                        {path:'/groupV/ZpsExpctCalc'},
                        {path:'/groupV/Ztechcond'},
                        {path:'/groupV/ZpsStatIn'},
                        {path:'/groupV/ZpsResAv'},
                        {path:'/groupV/ZpsDetDContP'},
                        {path:'/groupV/ZpsReqEco'},
                        {path:'/groupV/ZpsReqDoc'}
                    ];
                    var sPath = aPath[+vOrder-1].path + "/max";
                    oSource.getModel().oEngineModel.setProperty(sPath, vMax);

                }.bind(this),
                selectChange: function(oEvent){
                    var oSelectedItem = oEvent.getParameter("selectedItem");
                    var sPath = oEvent.getSource().data("sEnginePath");
                    if (oSelectedItem) {
                    	var oCntx = oSelectedItem.getBindingContext();
                    	this.oEngineModel.setProperty(sPath + "/text", oCntx.getProperty("Text"));
                    	this.oEngineModel.setProperty(sPath + "/pts", oCntx.getProperty("ZpsPts"));
                    	this.oEngineModel.setProperty(sPath + "/additionText", oCntx.getProperty("Helper"));
                    }
                },
                onLabSelectionChange: function (oEvent) {
                	var oSelectedItem = oEvent.getParameter('selectedItem');
                	if (oSelectedItem) {
                        var sLabName = oEvent.getParameter('selectedItem').getText();
                        this.oEngineModel.setProperty('/groupV/lab/text', sLabName)
                	}
                },
                onLabChange: function (oEvent) {
                    var sLabName = oEvent.getParameter('value');
                    this.oEngineModel.setProperty('/groupV/lab/text', sLabName)
                },
                onLabManualChange: function (oEvent) {
                	var selectedIndex = oEvent.getParameter('selectedIndex');
                	
                	this.oEngineModel.setProperty('/groupV/ZpsLabI', '');
                	this.oEngineModel.setProperty('/groupV/lab/id', '');
                	this.oEngineModel.setProperty('/groupV/ZmnlFlg', selectedIndex === 0 ? false : true);
                	
                },
                onLabReceived: function (oEvent) {
                	var oData = oEvent.getParameter('data'),
                		bManual = false;
                	
                	if (oData) {
                		bManual = oData.results.length > 0 ? false : true;
                		this.oEngineModel.setProperty('/groupV/ZpsManualPower', bManual);
                    	if (bManual) {
                    		this.oEngineModel.setProperty('/groupV/ZmnlFlg', bManual);
                    		this.oEngineModel.setProperty('/groupV/lab/id', '');
                    	}
                	}	
                },
                onEquipTypeChange: function (oEvent) {
                	var oStep = this.getView().byId('GroupV_riski');
                    var sType = oEvent.getParameter('selectedItem').getText();
                	var ZpsEqtyKey = oEvent.getParameter('selectedItem').getKey();
                	
                 	oStep.bindProperty('validated', {
                        parts: this._oValidationBinding.GroupV_riski.inProcess(ZpsEqtyKey),
                        formatter: this.validators.simpleValidator
                    });
                 	
                 	//Чистим поля, которые скрыты от пользователя
                 	var aEmptyProperty =  this._oValidationBinding.GroupV_riski.getEmptyPropertyEngineByZpsEqty(ZpsEqtyKey);
                 	aEmptyProperty.forEach(function(item, i, arr) {
                 		this.oEngineModel.setProperty(item, '');
        			}.bind(this));
                	
                    this.oEngineModel.setProperty('/groupV/ZpsEqty/text', sType)
                }
            }
        }
    };

    return selfObj;
}, true);