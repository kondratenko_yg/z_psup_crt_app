jQuery.sap.declare('initiatives.controllers.stepControllers.estimateProbabilityB');

initiatives.controllers.stepControllers.estimateProbabilityB = {
  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "Обязательность использования"
   * @param oEvent
   */
  onObligationChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('obligation');
    }
  },

  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "Риски неприобритения"
   * @param oEvent
   */
  onRisksChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('risks');
    }
  },

  /**
   * Обработчик выбора радио кнопки в опроснике вероятности отказа "Вид приобретения"
   * @param oEvent
   */
  onTypesChecked: function (oEvent) {
    var bSelected = oEvent.getParameter('selected');
    if (bSelected) {
      this._rebuildTableAnswers('type');
    }
  },

  /**
   * Пересчет таблицы из опросника вероятности отказа
   * @param sTableProperty
   * @private
   */
  _rebuildTableAnswers: function (sTableProperty) {
    var aYears = this.oEngineModel.getProperty('/years');

    var limit = -1; //ограничитель последующих строк
    var bRelocate = false; //необходимость сдвинуть выбор показателя следующих годов если предыдущий стал больше

    aYears.forEach(function (year, index) { //идём по ккаждой строке (году)
      year.estimateB[sTableProperty].answers.forEach(function (answer) { //идём по каждому варианту ответа
        if (answer.value >= limit) { //если значение ячейки привышает или равно лимиту
          if (bRelocate) { //если необходимо сдвинуть ячейку
            answer.selected = true;
            bRelocate = false;
          }
          answer.editable = true;
        } else {
          answer.editable = false;
          if (answer.selected) {
            answer.selected = false;
            bRelocate = true;
          }
        }
        if (answer.selected) {
          limit = answer.value;
          year.estimateB[sTableProperty].selectedValue = answer.value*answer.weight;
          year.estimateB[sTableProperty].selected = true;
          year.estimateB[sTableProperty].selectedIndex = answer.index;
          this._recountProbability(index)
        }
      }.bind(this))
    }.bind(this));
    this.oEngineModel.updateBindings();
  },

  /**
   * Вычисление процента вероятности отказа исходя из опросника по годам
   * @param index
   * @private
   */
  _recountProbability: function (index) {
    var oYear = this.oEngineModel.getProperty('/years/' + index);
    var nProbability =
      oYear.estimateB.obligation.selectedValue +
      oYear.estimateB.risks.selectedValue +
      oYear.estimateB.type.selectedValue;

    var percentOverall = (nProbability-1)/9;
    
    this.oEngineModel.setProperty('/years/' + index + '/breakChance', percentOverall*100)
  }

};