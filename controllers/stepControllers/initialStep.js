sap.ui.define([
    'z_psup_shared/dialog/CompanySelect',
    'z_psup_shared/dialog/WorkshopSelect',
    'z_psup_shared/FUNCTIONAL_AREAS',
    'z_psup_shared/PROJECT_TYPES',
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator',
    'sap/m/StandardListItem',
    'z_psup_shared/CONSTANTS'
], function(CompanySelect, WorkshopSelect, FUNCTIONAL_AREAS, PROJECT_TYPES, Filter, FilterOperator, StandardListItem, CONSTANTS) {
    'use strict';
    return {
        onCompanyValueHelp: function () {
            var oModel = this.getView().getModel('engine');

            if (!this._oCompanySelect) {
                this._oCompanySelect = new CompanySelect(this.getView());
            }
            this._oCompanySelect.open(function(oCompany) {
                oModel.setProperty('/company', oCompany);
                this._companyChange();
            }.bind(this));
        },

        /**
         * Обработчик изменения выбранной компании
         */
        _companyChange: function () {
            var oModel = this.getView().getModel('engine');
            var oCompany = oModel.getProperty('/company');

            //Обнуляем выбранный цех
            var oWorkshopsCB = sap.ui.getCore().byId('workshopName');
                oWorkshopsCB.setPlaceholder('').setValue('');

            //Заносим в модель бюджет
            oModel.setProperty('/budget', this.createBudgetObject(oCompany.currency, this._nFirstYear, CONSTANTS.COUNT_YEAR));

            //здесь оно для того, чтобы далее корректно отображалась валюта компании.
            switch (oCompany.currency) {
                case "USD":
                case "INR":
                    oModel.setProperty('/showedCurrency', this.getText("KUSD"));
                    break;
                case "DKK":
                case "EUR":
                    oModel.setProperty('/showedCurrency', this.getText("KEUR"));
                    break;
                default:
                    oModel.setProperty('/showedCurrency', this.getText("KRUB"));
                    break;
            };

            //запрашиваем количество доступных цехов по площадке
            //если цехв нет то допускаем ручной ввод
            var oCompanyFilter = new Filter('Zentity', FilterOperator.EQ, oCompany.id);
            var oWorkshopName = sap.ui.getCore().byId('workshopName');

            oWorkshopName.setEnabled(false);
            this.oView.getModel().read('/ZWorkshopSet/$count',{
                filters:[oCompanyFilter],
                success: function (count) {
                    oModel.setProperty('/manualWorkshop', +count == 0);
                    if (+count == 0) {
                        oModel.setProperty('/groupA/equip/manual', true);
                    }
                    oWorkshopName.setEnabled(true);
                }.bind(this),
                error: function () {
                    sap.m.MessageToast.show(this.getText("ReqErr"))
                }
            });
            this._checkAggregateCount(true);
        },

        _checkAggregateCount: function (bNeedReset) {
            var oModel = this.getView().getModel('engine');
            this.oView.getModel().read('/ZPs_aggrSet/$count', {
                filters: [this._getFiltersForAggregate()],
                success: function (count) {
                    var bManual = (+count == 0);
                    oModel.setProperty('/groupA/aggrDicEmpty', bManual);
                    if (bNeedReset && bManual) {
                        oModel.setProperty('/groupA/aggregate/manual', true);
                        oModel.setProperty('/groupA/aggregate/downtimeManual', true);
                        oModel.setProperty('/groupA/aggregate/id', '');
                        oModel.setProperty('/groupA/aggregate/name', '');
                        oModel.setProperty('/groupA/aggregate/downtimePenalty/units', 0);
                        oModel.setProperty('/groupA/aggregate/downtimePenalty/thousands', 0);
                    }
                }.bind(this)
            });
        },

        onWorkshopValueHelp: function () {
            var oModel = this.getView().getModel('engine');
            if (!this._oWorkshopSelect) {
                this._oWorkshopSelect = new WorkshopSelect(this.getView());
            }
            this._oWorkshopSelect.open(oModel.getProperty('/company/id'), function(oWorkshop) {
                if (oModel.getProperty('/workshop/id') !== oWorkshop.id) {
                    this._resetEquip();
                }
                oModel.setProperty('/workshop', oWorkshop);
            }.bind(this));
        },

        onWorkshopNameChange: function(oEvent) {
            this._resetEquip();
        },

        _resetEquip: function() {
            var oModel = this.getView().getModel('engine');
            var sFuncDir = oModel.getProperty("/funcDir/id");
            var bEdit = oModel.getProperty("/bEdit");
            var iZpsPrtyp = parseInt(oModel.getProperty('/groupA/ZpsPrtyp')); 

            if (bEdit === true && (FUNCTIONAL_AREAS.getGroupById(sFuncDir) === 'GROUPA')) {
                if (iZpsPrtyp !== 3 && iZpsPrtyp !== 4) {
                    oModel.setProperty('/groupA/equip/id', "");
                    oModel.setProperty('/groupA/equip/name', "");
                }
            }
        },
        
        onManualWorkshopSelect: function (oEvent) {
            var oModel = this.getView().getModel('engine');
            var bSelected = oEvent.getParameter('selected');
            
            oModel.setProperty('/workshop/name', '');
            oModel.setProperty('/workshop/id', '');
            oModel.setProperty('/workshop/shortName', '');
            
            oModel.setProperty('/groupA/equip/manual', bSelected);
        },

        /**
         * Обработчик изменения функционального направления
         * @param oEvent
         */
        onFuncDirChange: function (oEvent) {
            var sFuncDirKey = typeof oEvent === 'string' ? oEvent : oEvent.getSource().getSelectedKey();
            if(!this._bStepsBuilded){
                this._setSteps(sFuncDirKey);
            }else{
                this._showDiscardMessageBox(oEvent, function() {
                    this.byId('appWizard').discardProgress(this.byId('InitialParams'));
                    this.byId('BudgetStep').setNextStep(this.byId('uploadStep'));
                    this._setSteps(sFuncDirKey);
                    this._bStepsBuilded = false;
                }.bind(this), function() {
                    oEvent.getSource().setSelectedKey(this._sCashedFunDirId);
                }.bind(this));
            }
        },
        
        _showDiscardMessageBox: function(oEvent, fnConfirm, fnCancel) {
            var sSourceId = oEvent.getSource().getId();
            switch (sSourceId) {
                case 'funcDirection':
                    var sTitle = this.getText("ChngFuncAreaMsg");
                    break;
                case 'largeProject':
                    var sTitle = this.getText("ChngLargeProjectMsg");
                    break;
            }
            sap.m.MessageBox.warning(sTitle, {
                actions: [this.getText('YES'), this.getText('NO')],
                onClose: function(sAction) {
                    switch (sAction) {
                        case this.getText('YES'):
                            fnConfirm();
                            break;
                        case this.getText('NO'):
                            fnCancel();
                            break;
                    }
                }.bind(this)
            });
        },

        /**
         *
         * @param sFuncDirKey
         * @private
         */
        _setSteps: function (sFuncDirKey) {
            var oInitialStep = this.byId('InitialParams');
            
            var oModel = this.getView().getModel();
            
            if (!sFuncDirKey) {
                sFuncDirKey = this.getView().getModel('engine').getProperty('/funcDir/id');
            }

            this.oEngineModel.setProperty('/funcDir/name', oModel.getProperty("/ZFunc_dirSet('" + sFuncDirKey + "')/Zfunc_dirTxt"));
            var oBudgetStep = this.byId('BudgetStep');
            var sNextStepId = '';

            if(this._bEdit && this.oEngineModel.checkIsLaboratory()){
                sFuncDirKey = '07';
            }

            switch (sFuncDirKey) {
                case '01':
                case '03':
                case '04':
                case '05':
                    //группа А
                    if (sFuncDirKey == '04' && !this._bEdit) {
                        this._handleEnergeticsFNSelect();
                    }

                    sNextStepId = 'GroupA';
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupAReview';
                    this.byId('EstimateProbability').setNextStep(oBudgetStep);
                    oBudgetStep.setNextStep(this.byId('CalcRisk'));
                    break;
                case '02':
                    //группа Б ОТПБ
                    sNextStepId = 'GroupB';
                    var oOTPBStep = this.byId('OTPBDetect');
                    this.byId('GroupB').setNextStep(oOTPBStep);
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupB1Review';
                    oBudgetStep.setNextStep(this.byId('uploadStep'));
                    break;
                case '10':
                    //группа Б Экология
                    sNextStepId = 'GroupB';
            
                    var oEcoStep = this.byId('EcoDetect');
                    this.byId('GroupB').setNextStep(oEcoStep);
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupB1Review';
                    oBudgetStep.setNextStep(this.byId('uploadStep'));
                    break;
                case '07':
                    //группа В
                    sNextStepId = 'GroupV';
                    this.byId('EstimateProbability').setNextStep(oBudgetStep);
                    oBudgetStep.setNextStep(this.byId('CalcRiskV'));
            
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupVReview';
                    break;
                case '06':
                    //группа Г
                    sNextStepId = 'GroupG';
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupGReview';
                    oBudgetStep.setNextStep(this.byId('uploadStep'));
                    break;
                case '08':  // ИТ
                case '11':  // ИТ - Связь
                    //группа Д
                    sNextStepId = 'GroupD';
                    oBudgetStep.setNextStep(this.byId('groupDResult'));
                    this.byId('EstimateProbability').setNextStep(oBudgetStep);
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupDReview';
                    break;
                case '09':
                    //группа Е
                    sNextStepId = 'GroupE';
                    this._reviewPageFragment = 'initiatives.views.reviewFragments.groupEReview';
                    oBudgetStep.setNextStep(this.byId('uploadStep'));
                    break;
            }
            oInitialStep.setNextStep(this.byId(sNextStepId));
        },

        /**
         * Обработчик выбранного ФН Энергетика
         * @private
         */
        _handleEnergeticsFNSelect: function () {
            var dialog = new sap.m.Dialog({
                title: this.getText("FunAreaEnergy"),
                type: 'Message',
                content: new sap.m.Text({ text: this.getText("LabDiagUnit") }),
                beginButton: new sap.m.Button({
                    text: this.getText("YES"),
                    press: function () {
                        this.onFuncDirChange('07');
                        this.oEngineModel.setProperty('/Zpslabflg', true);
                        dialog.close();
                    }.bind(this)
                }),
                endButton: new sap.m.Button({
                    text: this.getText("NO"),
                    press: function () {
                        this.oEngineModel.setProperty('/Zpslabflg', false);
                        dialog.close();
                    }.bind(this)
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });
            dialog.open();
        },
        
        fetchProjectType: function(sProjectTypeId, fnSuccess) {
            var oModel = this.getView().getModel();
            oModel.read('/ZPs_prtypSet(\'' + sProjectTypeId + '\')', {
                success: function(oData, response) {
                    fnSuccess(oData);
                },
                urlParameters: {
                    $expand: 'NavToImplForm'
                }
            });
        },
        
        largeProjectVisibleFormatter: function(bEdit, bAdmin, bSupervisor, bCurrentApprover, sFuncDir, sProjType) {
            if (bEdit) {
                if(FUNCTIONAL_AREAS.getGroupById(sFuncDir) === 'GROUPA' && (sProjType === PROJECT_TYPES.AUTO_REPLACEMENT || sProjType === PROJECT_TYPES.TRANSPORT_REPLACEMENT)) {
                    return false;
                } else {
                    return bAdmin || bSupervisor || bCurrentApprover;
                }
            } else {
                return bAdmin || bSupervisor;
            }
        },
        
        largeProjectSelectedChanged: function(oEvent) {
            var oSource = oEvent.getSource();
            var bSelected = oEvent.getParameter('selected');
            var oEngineModel = this.getView().getModel('engine');
            var sGroup = FUNCTIONAL_AREAS.getGroupById(oEngineModel.getProperty('/funcDir/id'));
            
            if (this._bEdit) {
                if (sGroup === 'GROUPA') {
                    this.fetchProjectType(bSelected ? PROJECT_TYPES.LARGE_PROJECT : PROJECT_TYPES.OTHER_PROJECTS, function(oData) {
                        oEngineModel.setProperty('/groupA/ZpsPrtyp', oData.ZpsPrtyp);
                        oEngineModel.setProperty('/ZpsPrtypTxt', oData.Txtlg);
                        oEngineModel.setProperty('/realizationForm/id', bSelected ? oData.NavToImplForm.ZimplFrm : '');
                        oEngineModel.setProperty('/realizationForm/name', bSelected ? oData.NavToImplForm.Txtmd : '');
                        oEngineModel.setProperty('/realizationForm/shortName', bSelected ? oData.NavToImplForm.Txtsh : '');
                    }.bind(this));
                } else {
                    if (bSelected) {
                        this.fetchProjectType(PROJECT_TYPES.LARGE_PROJECT, function(oData) {
                            oEngineModel.setProperty('/groupA/ZpsPrtyp', oData.ZpsPrtyp);
                            oEngineModel.setProperty('/ZpsPrtypTxt', oData.Txtlg);
                        }.bind(this));
                    } else {
                        oEngineModel.setProperty('/groupA/ZpsPrtyp', '');
                        oEngineModel.setProperty('/ZpsPrtypTxt', '');
                    }
                }
            } else {
                if (this._bStepsBuilded) {
                    this._showDiscardMessageBox(oEvent, function() {
                        this.byId('appWizard').discardProgress(this.byId('InitialParams'));
                        this.byId('BudgetStep').setNextStep(this.byId('uploadStep'));
                        this._setSteps();
                        this._bStepsBuilded = false;
                    }.bind(this), function() {
                        oSource.setSelected(this._sCashedLargeProj);
                    }.bind(this));
                }
                if (bSelected) {
                    this.fetchProjectType(PROJECT_TYPES.LARGE_PROJECT, function(oData) {
                        var sFuncDir = oEngineModel.getProperty('/funcDir/id');
                        oEngineModel.setProperty('/groupA/ZpsPrtyp', oData.ZpsPrtyp);
                        oEngineModel.setProperty('/ZpsPrtypTxt', oData.Txtlg);
                        if (FUNCTIONAL_AREAS.getGroupById(sFuncDir) === 'GROUPA') {
                            oEngineModel.setProperty('/realizationForm/id', oData.NavToImplForm.ZimplFrm);
                            oEngineModel.setProperty('/realizationForm/name', oData.NavToImplForm.Txtmd);
                            oEngineModel.setProperty('/realizationForm/shortName', oData.NavToImplForm.Txtsh);
                        }
                    }.bind(this));
                } else {
                    oEngineModel.setProperty('/groupA/ZpsPrtyp', '');
                    oEngineModel.setProperty('/ZpsPrtypTxt', '');
                    oEngineModel.setProperty('/realizationForm/id', '');
                    oEngineModel.setProperty('/realizationForm/name', '');
                    oEngineModel.setProperty('/realizationForm/shortName', '');
                }
            }
        }
    }
});
