sap.ui.define([
    'sap/m/ColumnListItem',
    'sap/m/Input',
    'sap/m/Text'
], function(ColumnListItem, Input, Text) {
    'use strict';
    
    return {
        budgetStep: {
            setNextStep: function (oSource) {
                var oView = this.getView(),
                    funcDir = this.oEngineModel.getProperty('/funcDir/id'),
                    ProjectKey = +this.oEngineModel.getProperty('/groupA/ZpsPrtyp');
                
                switch (funcDir) {
                    case '05':
                        if(ProjectKey == 3){ //Если тип проекта: Замена легкового а/м
                            oSource.setNextStep(this.byId('uploadStep'));
                        } else if(ProjectKey == 4){ //Если тип проекта: Замена автотранспорта (в соответствии с расчётом ТСО)
                            oSource.setNextStep(this.byId('uploadStep'));
                        }
                        break;
                }
            }
        },
        
        _rebindBudget: function (bNds) {
            var sNds = bNds ? 'nds' : 'noNds',
                nUsdRate = this.oEngineModel.getProperty('/budget/rates/usd'),
                nEurRate = this.oEngineModel.getProperty('/budget/rates/eur'),
                nWearCount = this.oEngineModel.getProperty('/budget/waersCount'),
                nInvCount = this.oEngineModel.getProperty('/budget/invCount');
            
            this.oEngineModel.setProperty('/iNdsSelected', bNds ? 1 : 0);
                
            var oTable = sap.ui.getCore().byId('iBudgetTable'),
                nRowIndex = 0;
            
            oTable.bindAggregation('items', {
                path: 'engine>/budget/rows/' + sNds,
                factory: function (sId, oContext) {
                    var sPath = oContext.getPath();
                    var oRowData = oContext.getObject();

                    var aCells = [];

                    var clause = new Text({
                      text: oRowData.clause || ''
                    });

                    var waers = new Text({
                      text: oRowData.waers || ''
                    });
                    
                    var getAmountTextParts = function(sBudgetPart){
                        var mParts = [];
                        for(var i = 0; i < nInvCount; i++){
                            mParts.push({path: 'engine>/budget/rows/' + sNds + '/'+ (nWearCount+1)*i + '/' + sBudgetPart});
                        }
                        return mParts;
                    }
                    
                    var getHeaderTextParts = function(sBudgetPart){
                        var mParts = [];
                        if(sBudgetPart!='financeLeft'){
                            for(var i = 1; i <= nWearCount; i++){
                                mParts.push({path: 'engine>/budget/rows/' + sNds + '/' + (nRowIndex + i)+'/' + sBudgetPart});
                            }
                        }
                        else{
                            mParts.push({path: 'engine>' + sPath + '/allBudget'});
                            mParts.push({path: 'engine>' + sPath + '/financed'});
                        }
                        return mParts;
                    }

                    var allBudget;
                    if (oRowData.isAmmount) {
                      allBudget = new Text({
                        text: {
                          parts: getAmountTextParts('allBudget'),
                          formatter: function (val1, val2, val3, val4) {
                            var allBudgetAmout = +val1 + +val2 + +val3 + +val4;
                            if (!bNds) {
                              this.oEngineModel.setProperty('/allBudget', allBudgetAmout);
                            }
                            this.oEngineModel.setProperty(sPath + '/allBudget', allBudgetAmout);
                            return this.formatter.numberWithSpaces(+allBudgetAmout.toFixed(this._COUNT_BUDGET_DIGITS));
                          }.bind(this)
                        }
                      })
                    } else if (oRowData.isHeader) {
                        allBudget = new Text({
                            text: {
                                parts: getHeaderTextParts('allBudget'),
                                formatter: function (nRub, nUsd, nEur) {
                                    var thisYear = new Date().getFullYear();
                                    var nUsdYear = this.oRatesModel.getProperty('/' + thisYear + '/USD');
                                    var nEurYear = this.oRatesModel.getProperty('/' + thisYear + '/EUR');
                                    var nAllBudgetAmount = 0;
                                    if(nWearCount==1){
                                        nAllBudgetAmount = +nRub;
                                    }
                                    else{
                                        nAllBudgetAmount = +nRub+ (+nUsd * nUsdYear) + (+nEur * nEurYear);
                                    }
                                    this.oEngineModel.setProperty(sPath + '/allBudget', nAllBudgetAmount);
                                    
                                    return this.formatter.numberWithSpaces(+nAllBudgetAmount.toFixed(this._COUNT_BUDGET_DIGITS));
                                }.bind(this)
                            }
                        });
                    } else {
                        allBudget = new Input({
                            value: {
                                path: 'engine>' + sPath + '/allBudget',
                                type: 'initiatives.models.type.cFloat',
                                formatOptions: {
                                    groupingEnabled: true,
                                    groupingSeparator: ' ',
                                    maxFractionDigits: this._COUNT_BUDGET_DIGITS
                                }
                            },
                            change: this.formatter.onVATLiveChange.bind(this)
                        }).addEventDelegate({
                            onfocusin: function(oEvent){
                                if(+oEvent.target.value == 0) {
                                    oEvent.target.value = '';
                                }
                            }
                        });
                    }

                    var financed;
                    if (oRowData.isAmmount) {
                      financed = new Text({
                        text: {
                          parts: getAmountTextParts('financed'),
                          formatter: function (val1, val2, val3, val4) {
                            var allFinancedAmmount = +val1 + +val2 + +val3 + +val4;
                            this.oEngineModel.setProperty(sPath + '/financed', allFinancedAmmount);
                            return this.formatter.numberWithSpaces(+allFinancedAmmount.toFixed(this._COUNT_BUDGET_DIGITS));
                          }.bind(this)
                        }
                      })
                    } else if (oRowData.isHeader) {
                      financed = new Text({
                        text: {
                          parts: getHeaderTextParts('financed'),
                          formatter: function (nRub, nUsd, nEur) {
                            var nCurYear = new Date().getFullYear();
                            var nCurYearUsd = this.oRatesModel.getProperty('/'+ nCurYear + '/USD');
                            var nCurYearEur = this.oRatesModel.getProperty('/'+ nCurYear + '/EUR');
                            var nFinancedAmount = 0;
                            if(nWearCount==1){
                                nFinancedAmount = +nRub ;
                            }
                            else{
                                nFinancedAmount = +nRub + (+nUsd * nCurYearUsd) + (+nEur * nCurYearEur);
                            }
                            this.oEngineModel.setProperty(sPath + '/financed', nFinancedAmount);
                            return this.formatter.numberWithSpaces(+nFinancedAmount.toFixed(this._COUNT_BUDGET_DIGITS));
                          }.bind(this)
                        }
                      });
                    } else {
                      financed = new Input({
                        value: {
                          path: 'engine>' + sPath + '/financed',
                          type: 'initiatives.models.type.cFloat',
                          formatOptions: {
                            groupingEnabled: true,
                            groupingSeparator: ' ',
                            maxFractionDigits: this._COUNT_BUDGET_DIGITS
                          }
                        },
                        enabled: this.budgetCellEnabledFormatter(sPath + '/financed', false),
                        change: this.formatter.onVATLiveChange.bind(this)
                      }).addEventDelegate({
                        onfocusin: function(oEvent){
                          if(+oEvent.target.value == 0) {
                            oEvent.target.value = '';
                          }
                        }
                      });
                    }

                    var financeLeft;
                    if (oRowData.isAmmount) {
                      financeLeft = new Text({
                        text: {
                          parts: getAmountTextParts('financeLeft'),
                          formatter: function (val1, val2, val3, val4) {
                            var allFinanceLeftAmount = +val1 + +val2 + +val3 + +val4;
                            this.oEngineModel.setProperty(sPath + '/financeLeft', allFinanceLeftAmount);
                            return this.formatter.numberWithSpaces(+allFinanceLeftAmount.toFixed(this._COUNT_BUDGET_DIGITS));
                          }.bind(this)
                        }
                      })
                    } else if (oRowData.isHeader) {
                      financeLeft = new Text({
                        text: {
                          parts: getHeaderTextParts('financeLeft'),
                          formatter: function (allBudget, financed) {
                            var nFinanceLeft = allBudget - financed;
                            this.oEngineModel.setProperty(sPath + '/financeLeft', nFinanceLeft);
                            return this.formatter.numberWithSpaces(+nFinanceLeft.toFixed(this._COUNT_BUDGET_DIGITS));
                          }.bind(this)
                        }
                      });
                    } else {
                      financeLeft = new Text({
                        text: {
                          parts: getHeaderTextParts('financeLeft'),
                          formatter: function (allBudget, financed) {
                            var nFinanceLeft = allBudget - financed;
                            this.oEngineModel.setProperty(sPath + '/financeLeft', nFinanceLeft);
                            return this.formatter.numberWithSpaces(+nFinanceLeft.toFixed(this._COUNT_BUDGET_DIGITS));
                          }.bind(this)
                        }
                      });
                    }

                    aCells.push(clause, waers, allBudget, financed, financeLeft);

                    oRowData.years.forEach(function (year, index) {
                      if (oRowData.isAmmount) {
                        aCells.push(new Text({
                          text: {
                            parts: getAmountTextParts('years/' + index + '/value'),
                            formatter: function (val1, val2, val3, val4) {
                              var allByYearAmount = +val1 + +val2 + +val3 + +val4;
                              this.oEngineModel.setProperty(sPath + '/years/' + index + '/value', allByYearAmount);
                              if (index === 0) {
                                  this.checkFirstYearBudget(allByYearAmount);
                              }
                              return this.formatter.numberWithSpaces(+allByYearAmount.toFixed(this._COUNT_BUDGET_DIGITS));
                            }.bind(this)
                          }
                        }))
                      } else if (oRowData.isHeader) {
                        aCells.push(new Text({
                          text: {
                            parts: getHeaderTextParts('years/' + index + '/value'),
                            formatter: function (nRub, nUsd, nEur) {
                              var thisYear = new Date().getFullYear();
                              var nUsdYear = this.oRatesModel.getProperty('/' + thisYear + '/USD');
                              var nEurYear = this.oRatesModel.getProperty('/' + thisYear + '/EUR');
                              var nFinancebyYearAmount = 0;
                              if(nWearCount == 1){
                                  nFinancebyYearAmount = +nRub;
                              }
                              else{
                                  nFinancebyYearAmount = +nRub + (+nUsd * nUsdYear) + (+nEur * nEurYear);
                              }
                              this.oEngineModel.setProperty(sPath + '/years/' + index + '/value', nFinancebyYearAmount);
                              return this.formatter.numberWithSpaces(+nFinancebyYearAmount.toFixed(this._COUNT_BUDGET_DIGITS));

                            }.bind(this)
                          }
                        }));
                      } else {
                        aCells.push(new Input({
                          value: {
                            path: 'engine>' + sPath + '/years/' + index + '/value',
                            type: 'initiatives.models.type.cFloat',
                            formatOptions: {
                              groupingEnabled: true,
                              groupingSeparator: ' ',
                              maxFractionDigits: this._COUNT_BUDGET_DIGITS
                            }
                          },
                          enabled: this.budgetCellEnabledFormatter(sPath + '/years/' + index + '/value', year.isLastYear),
                          change: this.formatter.onVATLiveChange.bind(this)
                        }).addEventDelegate({
                          onfocusin: function(oEvent){
                            if(+oEvent.target.value == 0) {
                              oEvent.target.value = '';
                            }
                          }
                        }))
                      }
                    }.bind(this));

                    var oRow = new ColumnListItem({
                      cells: aCells
                    });
                    
                    //Если это строка заголовка или Итого, то делаем её фон другим
                    if (oRowData.isHeader || oRowData.isAmmount) {
                        oRow.addStyleClass('myBudgetTableRow');
                        oRow.addStyleClass('myBudgetTableRowSpan');
                    }
                    
                    nRowIndex++;
                    return oRow;
                }.bind(this)
            })
        },

        /**
         * Обработчик переключения НДС в таблице бюджета
         * @param oEvent
         */
        onNdsSelect: function (oEvent) {
            var selectedIndex = oEvent.getParameter('selectedIndex');
            
            if (selectedIndex === 0) {
                this.bNds = false;
                this._rebindBudget(false);
            } else {
                this.bNds = true;
                this._rebindBudget(true);
            }
        },
        
        _checkAccess: function() {
            var oSettingsModel = this.getView().getModel('settings');
            var bAdmin = oSettingsModel.getProperty('/roles/isAdmin');
            var bEarlyImplementation = this.oEngineModel.getProperty('/earlyImplementation/ZpsImpl');
            
            var oOpenSystemDate = oSettingsModel.getProperty('/sysStartDate');
            var oCreationDate = this.oEngineModel.getProperty('/creationDate');
            
            if (oCreationDate >= oOpenSystemDate) {
                if (!bEarlyImplementation && !bAdmin) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        },
        
        budgetCellEnabledFormatter: function(sCellPath, bLastYear) {
            var bAccess = this._checkAccess();
            if (!bAccess) {
                if (bLastYear) {
                    return true;
                } else {
                    this.oEngineModel.setProperty(sCellPath, 0);
                    return false;
                }
            } else {
                return true;
            }
        },
        
        projectPhaseEnabledFormatter: function(bLastYear) {
            var bAccess = this._checkAccess();
            if (!bAccess) {
                return bLastYear? true : false;
            } else {
                return true;
            }
        },
        
        earlyImplementationSelectionChange: function(oEvent) {
            var bIsSelected = oEvent.getParameter("selected");
            var aProjPhasesSelects = sap.ui.getCore().byId('projectPhaseTable').getItems()[0].getCells();
            
            aProjPhasesSelects.forEach(function(oSelect, index) {
                if(index !== aProjPhasesSelects.length - 1) {
                    if (bIsSelected) {
                        oSelect.setEnabled(true);
                    } else {
                        if (!this._checkAccess()) {
                            oSelect.setEnabled(false);
                            oSelect.setSelectedItem(null);
                            this.oEngineModel.setProperty('/years/' + index + '/projectPhase/id', '');
                            this.oEngineModel.setProperty('/years/' + index + '/projectPhase/text', '');
                        }
                    }
                }
            }, this);
            
            this._rebindBudget(this.bNds);
            sap.ui.getCore().byId("earlyImplementationReasonContainer").setVisible(bIsSelected);
        },
        
        earlyImplementationReasonSelectionChanged: function(oEvent) {
            var oSelectedButtonContext = oEvent.getSource().getSelectedButton().getBindingContext(),
                sReasonId = this.getModel().getProperty(oSelectedButtonContext.getPath() + "/ZpsReasn");
            this.oEngineModel.setProperty("/earlyImplementation/reason", sReasonId);
        },
        
        /**
         * Обработчик смены фазы проекта. Записывает название фазы в модель
         * @param oEvent
         */
        onPhaseChange: function (oEvent) {
            var oSelectedItem = oEvent.getParameter('selectedItem');
            if (oSelectedItem) {
                var sPath = oEvent.getSource().getBindingContext('engine');
                var sPhaseText = oSelectedItem.getBindingContext().getProperty('Txtlg');
                this.oEngineModel.setProperty(sPath + '/projectPhase/text', sPhaseText);
            }
        }
    };
});