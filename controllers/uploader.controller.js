sap.ui.define([
  'initiatives/controllers/BaseController',
  'sap/m/UploadCollection',
  'sap/m/UploadCollectionItem',
  'sap/m/UploadCollectionParameter',
  'sap/ui/core/CustomData',
  'sap/m/ListMode',
  'sap/m/MessageBox'
], function (BaseController, UploadCollection, UploadCollectionItem, UploadCollectionParameter, CustomData, ListMode, MessageBox) {
  return BaseController.extend('initiatives.controllers.uploader', {
	  onInit: function() {
	  },
	  onExit: function() {
	  },
	  onAfterRendering: function() {
	  },
	  
	  onBeforeUploadStarts: function(oControlEvent) {
		  var fAddParameter = oControlEvent.getParameter('addHeaderParameter'),
		  		oUploader = oControlEvent.getSource(),
		  		oEngineModel = this.getView().getModel('engine');
		  
		  fAddParameter(new UploadCollectionParameter({
			  name: "x-csrf-token",
			  value: oEngineModel.getProperty('/token')
		  }));
	    	
		  fAddParameter(new UploadCollectionParameter({
			  name: "FileName",
			  value: encodeURIComponent(oControlEvent.getParameter('fileName'))
		  }));
	    	
		  fAddParameter(new UploadCollectionParameter({
			  name: "ProjName",
			  value: oEngineModel.getProperty('/projectNumber')
		  }));
		  
		  fAddParameter(new UploadCollectionParameter({
			  name: "ObjType",
			  value: oUploader.data('objectType')
		  }));
	  },
	  
	  onUploadComplete: function(oControlEvent) {
		  var oFiles = oControlEvent.getParameter('files'),
		  	  oBundle = this.getView().getModel('i18n').getResourceBundle(),
		  	  sError = oBundle.getText("ErrorSendFile"),
		  	  mErrorFiles = [];
		  
		  oFiles.forEach(function(oFile) {
			  if (oFile.status != 201) {
				  sError += (mErrorFiles.length > 0 ? ', ' : ' ') + oFile.fileName;
				  mErrorFiles.push(oFile);
			  }
		  });
		  
		  if (mErrorFiles.length != 0)
			  MessageBox.error(sError)
	  },
	  
	  uploadCollectionFactory: function (sUploaderId, oContext) {
		  var oData = oContext.getObject(),
		  	sPath = oContext.getPath(),
		  	oBundle = this.getView().getModel('i18n').getResourceBundle(),
		  	oEngineModel = this.getView().getModel('engine');
		  	sServicePrefix = this._fixGetWindowsLocationOrigin(), 
		  	sProjectNumber = oEngineModel.getProperty('/projectNumber');
		  	
		  	var oUploadCollection = new UploadCollection(sUploaderId, {
		  		mode: ListMode.None,
		  		selectionChange: function (oEvent) {
		  			if (oEvent.getParameter('selected') === true) {
		  				var oSelectedItem = oEvent.getParameter('selectedItem'),
		  				sUrl = oSelectedItem.getUrl();
		  				if(sUrl){
		  					window.open(oSelectedItem.getUrl(), '_blank');
		  				}
		  				oSelectedItem.setSelected(false);
		  			}
		  		},
		  		instantUpload: false,
		  		numberOfAttachmentsText: oBundle.getText(oData.numberOfAttachmentsText),
		  		uploadUrl: sServicePrefix + '/sap/opu/odata/sap/Z_PSUP_SERVICES_SRV/FileSet',
		  		multiple: true,
		  		showSeparators: 'All',
		  		beforeUploadStarts: this.onBeforeUploadStarts.bind(this),
		  		uploadComplete: this.onUploadComplete.bind(this)
	    		}).addCustomData(new CustomData({
	    			key: 'objectType',
	    			value: oData.objectType
	    		}));

		  	if(this.oView.getModel('engine').getProperty('/bEdit')){
		  		var sPath = oContext.getPath();

		  		oUploadCollection.bindAggregation('items', {
		  			path: 'engine>' + sPath + '/items',
		  			factory: function (sItemId, oInnerContext) {
		  				var oData = oInnerContext.getObject();
		  				var sFileName = oData.name;
		  				var sHref = '';
		  				if(oEngineModel.getProperty('/bEdit') || oEngineModel.getProperty('/bView')){
		  					sHref = sServicePrefix + '/sap/opu/odata/sap/Z_PSUP_SERVICES_SRV/FileSet(FileName=\''+ encodeURIComponent(sFileName) +'\',ProjName=\''+ encodeURIComponent(sProjectNumber) +'\',ObjType=\''+oContext.getObject().objectType+'\')/$value'
		  				}
		  				var oCollectionItem = new UploadCollectionItem(sItemId, {
		  					fileName: sFileName,
		  					url: sHref
		  				});

		  				return oCollectionItem
		  			}.bind(this)
		  		})
		  	}
		  	return oUploadCollection
	  },
  })
});