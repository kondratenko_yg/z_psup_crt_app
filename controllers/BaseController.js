sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/ui/core/routing/History"
], function (Controller, History) {
  return Controller.extend("initiatives.controllers.BaseController", {

    onInit: function () {
      this.oEventBus = sap.ui.getCore().getEventBus();
      this.sServicePrefix = this._fixGetWindowsLocationOrigin();
    },
    
    _fixGetWindowsLocationOrigin: function() {
    	var sLocationOrigin = "";
    	if (!window.location.origin) {
    		sLocationOrigin = window.location.protocol + "//" 
    			+ window.location.hostname 
    			+ (window.location.port ? ':' + window.location.port : '');
    	}
    	else
    		sLocationOrigin = window.location.origin;
    	return sLocationOrigin;
    },
    getRouter: function () {
      return sap.ui.core.UIComponent.getRouterFor(this);
    },
    
    getModel: function(sName) {
		return this.getView().getModel(sName);
	},
	
	/*
	Функция для получения текста из модели i18n
	*/
	getText: function(sName, aArgs) {
        var oResourceBundle = this.getModel('i18n').getResourceBundle();

        if (aArgs) {
            return oResourceBundle.getText(sName, aArgs);
        }
        return oResourceBundle.getText(sName);
    },
	
	onReplaceInternText: function(oModel) {
		//var o=1;
		// Ходим по JSON модели и заменяем где текст на текст из интернацианализации	
  	  for(var key in oModel) {
		  if (oModel.hasOwnProperty(key)) {
			  var oValue = oModel[key];
			  switch (typeof oValue) {
			  	case "string":
			  		if (oValue.indexOf("i18n>") == 0) {
			  			oModel[key] = this.getText(oValue.slice(5));
			  		}
			  		break;
			  	case "object":
			  		this.onReplaceInternText(oModel[key]);
			  		break;
			  	default:
			  		break;
			  	
			  }
		  }
	  }
    },
    
    
    
    onNavBack: function (oEvent) {
      var oHistory, sPreviousHash;
      oHistory = History.getInstance();
      sPreviousHash = oHistory.getPreviousHash();
      if (sPreviousHash !== undefined) {
        window.history.go(-1);
      } else {
        this.getRouter().navTo("main", {}, true /*no history*/);
      }
    }
  })
});
