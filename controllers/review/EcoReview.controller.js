sap.ui.define([
  'initiatives/controllers/BaseController',
  'sap/m/UploadCollection',
  'sap/m/VBox',
  'sap/m/Input',
  'sap/m/CheckBox',
  'sap/ui/layout/form/FormElement'
], function (BaseController, UploadCollection, VBox, Input, CheckBox, FormElement) {
  return BaseController.extend('initiatives.controllers.review.EcoReview', {

    groupB2ReviewChBoxFactory: function (sId, oContext) {
      var oData = oContext.getObject();
      var oCheckbox = {};
      if(oData.ZmnlFlg){
        if(+oData.ZpsChbg === 4) {
          oCheckbox = new VBox({
            items: [
              new CheckBox({
                text: oData.Txtlg,
                selected: '{engine>/groupB/eco/otherMode}',
                editable: false
              }),
              new Input({
                width: '50%',
                value: '{engine>/groupB/eco/ZpsWrkmi}',
                editable: false,
              })
            ]
          })
        }else if(+oData.ZpsChbg === 5) {
          oCheckbox = new VBox({
            items: [
              new CheckBox({
                text: oData.Txtlg,
                selected: '{engine>/groupB/eco/otherType}',
                editable: false
              }),
              new Input({
                width: '50%',
                value: '{engine>/groupB/eco/ZpsIncti}',
                editable: false,
              })
            ]
          })
        }
      }else{
        oCheckbox = new CheckBox({
          text: oData.Txtlg,
          selected: '{engine>/groupB/checkBoxes/' + oData.ZpsChbx + '}',
          editable: false
        });
      }

      return new FormElement({
        fields: [oCheckbox] })
    }

  })

});