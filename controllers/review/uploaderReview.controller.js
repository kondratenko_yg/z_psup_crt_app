sap.ui.define([
  'initiatives/controllers/BaseController',
  'sap/m/UploadCollection',
  'sap/m/HBox',
  'sap/m/VBox',
  'sap/m/Link',
  'sap/ui/core/Icon',
  'sap/m/Panel',
  'sap/ui/layout/Grid',
  'sap/m/Toolbar',
  'sap/m/Title'
], function (BaseController, UploadCollection, HBox, VBox, Link, Icon, Panel, Grid, Toolbar, Title) {
  return BaseController.extend('initiatives.controllers.review.uploaderReview', {

    /**
     * Фабрика для загруженных файлов для ревью
     * @param sId
     * @param oContext
     */
  })

});