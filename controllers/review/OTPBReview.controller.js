sap.ui.define([
  'initiatives/controllers/BaseController',
  'sap/m/UploadCollection',
  'sap/m/VBox',
  'sap/m/Input',
  'sap/m/CheckBox',
  'sap/ui/layout/form/FormElement'
], function (BaseController, UploadCollection, VBox, Input, CheckBox, FormElement) {
  return BaseController.extend('initiatives.controllers.review.OTPBReview', {

	  groupBReviewChBoxFactory: function (sId, oContext) {
	      var oData = oContext.getObject();
	      var oCheckbox = {};
	      if(oData.ZmnlFlg){
	        oCheckbox = new VBox({
	          items:[
	            new CheckBox({
	              text: oData.Txtlg,
	              selected: '{engine>/groupB/OTPB/otherType}',
	              editable: false
	            }),
	            new Input({
	              width: '50%',
	              value: '{engine>/groupB/OTPB/ZpsDntpi}',
	              editable: false,
	            })
	          ]
	        })
	      }else{
	        oCheckbox = new CheckBox({
	          text: oData.Txtlg,
	          selected: '{engine>/groupB/checkBoxes/' + oData.ZpsChbx + '}',
	          editable: false
	        });
	      }

	      return new FormElement({
	        fields: [oCheckbox] })
	  },
	  
	  groupBReviewRadioBtnFactory: function(sId, oContext) {
		  var oData = oContext.getObject();
		    var oRadioButton = {};
		    if (oData.ZmnlFlg) {
		        oRadioButton = new sap.m.VBox({
		            items:[
		            	new sap.m.RadioButton({
		            		text: oData.Txtlg,
		            		groupName: "TypeDamage",
		            		selected: "{= ${engine>/groupB/OTPB/TypeDamage/id} === '" + oData.ZpsChbx + "' }",
		            		editable: false
		            	}),
		            	new sap.m.Input({
		            		width: '50%',
		            		editable: false,
		            		value: '{engine>/groupB/OTPB/TypeDamage/ZpsDntpi}'
		            	})
		            ]
		        });
			} else {
			    oRadioButton = new sap.m.RadioButton({
			        text: oData.Txtlg,
			        groupName: "TypeDamage",
			        selected: "{= ${engine>/groupB/OTPB/TypeDamage/id} === '" + oData.ZpsChbx + "' }",
			        editable: false
			    });
			}
		    return new sap.ui.layout.form.FormElement({
		        fields: [oRadioButton]
		    });
	  }
  })

});