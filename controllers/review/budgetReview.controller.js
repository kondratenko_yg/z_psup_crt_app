sap.ui.define([
  'initiatives/controllers/BaseController'
], function (BaseController) {
  return BaseController.extend('initiatives.controllers.review.budgetReview', {
	_COUNT_BUDGET_DIGITS: 2,
	
    onReviewNdsSelect: function (oEvent) {
      var oEngineModel = this.oView.getModel('engine');
      var oReviewBudget = this.oView.byId('reviewBudgetTable');

      var selectedIndex = oEvent.getParameter('selectedIndex');

      var oBinding = oReviewBudget.getBinding('items');

      if (selectedIndex === 0) {
        oBinding.sPath = '/budget/rows/noNds'
      } else {
        oBinding.sPath = '/budget/rows/nds'
      }

      oEngineModel.updateBindings();
    },

    fieldsRounder: function (value) {
    	var formatters = initiatives.util.formatters;
    	return formatters.numberWithSpaces(+(+value).toFixed(this._COUNT_BUDGET_DIGITS));
    },
    
    getEarlyReasonText: function(sId) {
    	var sPath = "/Zps_ReasnSet('" + sId + "')/Txtlg"
    	return this.oView.getModel().getProperty(sPath);
	}
  })

});