sap.ui.define([
  'initiatives/controllers/BaseController',
  'sap/ui/model/json/JSONModel',
  'sap/ui/table/Column',
  'sap/m/Label',
  'sap/m/Input',
  'sap/m/Text',
  'sap/m/HBox',
  'sap/m/VBox',
  'z_psup_shared/model/EngineModel',
  'initiatives/models/VizModel',
  'sap/ui/core/ListItem',
  'sap/m/Select',
  'sap/ui/model/type/Integer',
  'sap/ui/core/CustomData',
  'sap/m/ColumnListItem',
  'z_psup_shared/model/oDataValidateData',
  'sap/ui/model/odata/v2/ODataModel',
  'sap/ui/layout/form/FormElement',
  "sap/ui/core/Popup",
  "sap/ui/model/resource/ResourceModel",
  'initiatives/util/formatters',
  "sap/ui/model/Filter",
  "sap/ui/model/FilterOperator",
  'sap/m/UploadCollection',
  'sap/m/UploadCollectionItem',
  'sap/m/ListMode',
  'sap/ui/core/Icon',
  'sap/m/Panel',
  'sap/ui/layout/Grid',
  'sap/m/Toolbar',
  'sap/m/Title',
  'sap/m/Link',
  'sap/m/MessageToast',
  'z_psup_shared/dialog/NewsPopover',
  'z_psup_shared/dialog/OzmSelect',
  'z_psup_shared/dialog/PriorityDialog',
  'z_psup_shared/NEWS_TYPE',
  'z_psup_shared/FUNCTIONAL_AREAS',
  'z_psup_shared/PROJECT_TYPES',
  'z_psup_shared/REALIZATION_FORM',
  'z_psup_shared/model/formatter',
  'z_psup_shared/model/ODataEngineModel',
  'z_psup_shared/CONSTANTS'
], function (BaseController,
		JSONModel,
		Column,
		Label,
		Input,
		Text,
		HBox,
		VBox,
		EngineModel,
		VizModel,
		ListItem,
		Select,
		Integer,
		CustomData,
		ColumnListItem,
		oDataValidateData,
		ODataModel,
		FormElement,
		Popup,
		ResourceModel,
		formatters,
		Filter,
		FilterOperator,
		UploadCollection,
		UploadCollectionItem,
		ListMode,
		Icon,
		Panel,
		Grid,
		Toolbar,
		Title,
		Link,
		MessageToast,
		NewsPopover,
		OzmSelect,
		PriorityDialog,
		NEWS_TYPE,
		FUNCTIONAL_AREAS,
		PROJECT_TYPES,
		REALIZATION_FORM,
		SharedFormatters,
		ODataEngineModel,
		CONSTANTS) {

  return BaseController.extend('initiatives.controllers.Wizard', {
	  formatter: formatters,
	  sharedFormatter: SharedFormatters,
	// Текущий год или первый год из заполненной программы
	_nFirstYear: 0,
	
	//Количество символов в рассчёте в бюджете
	_COUNT_BUDGET_DIGITS: 2,
	
	//Функциональные направления
	_FN_SAFETY: '02',
	_FN_ENERGETICS: '04',
	_FN_LOGISTICS: '05',
	_FN_CORPORATE_SECURITY: '06',
	_FN_LIKIO: '07',
	
	//Значение по умолчанию для Характеристики проекта
	_PROJECT_CHARACTERISTIC_DEFAULT: "02",
	
    onInit: function () {
      initiatives.controllers.BaseController.prototype.onInit.apply(this, arguments);
      if(!this.loadingDialog){
        this.loadingDialog = new sap.m.BusyDialog();
        this.loadingDialog.open();
      }

      this.oWizard = this.byId('appWizard');

      this.oEngineModel = new EngineModel();
      this.getView().setModel(this.oEngineModel, 'engine');
      
      this.getRouter().attachRoutePatternMatched(this._onRoutePatternMatchedHandler, this);

      this.oRatesModel = new JSONModel();
      this.oView.setModel(this.oRatesModel, 'rates');

      this.oVizModel = new VizModel().initialize();
      this.getView().setModel(this.oVizModel, 'viz');
      
      // Проставляем первый год в переменную
      this._nFirstYear = new Date().getFullYear() + 1;
      
      this.oNewsPopover = new NewsPopover(this.getView());
    },

    _onRoutePatternMatchedHandler: function (oEvent) {
      var sRouteName = oEvent.getParameter('name');
      this.oEngineModel.setData({});
      
      if(!this.editLoaded) {
        this.editLoaded = true
      }else{
        location.reload()
      }
      
      var oInitParam = {};

      if(sRouteName === 'edit' || sRouteName === 'view') {
          //Устанавливаем флаг редактирования
          this._bEdit = true;
          
          var oZSupoDfd = jQuery.Deferred(); //Deffered объект для набора ZSupoSet
          var oEstimateDfd = jQuery.Deferred(); //Deffered объект для набора ZSupoByYearSet
          var oBudgetDfd = jQuery.Deferred(); //Deffered объект для набора ZSupoBudgetByYearSet
          var oGroupBDfd = jQuery.Deferred(); // Deffered основные данные по группе Б
          var oItDfd = jQuery.Deferred(); //Deffered по IT
          var oLaboratoryDfd = jQuery.Deferred(); //Deffered по Лаборатории
          var oFileSetDfd = jQuery.Deferred(); //Deffered по загруженным файлам
          
          var sZspId = oEvent.getParameter('arguments')['ZspId'];


        //Здесь небольшой workaround, т.к. в ланчпаде закрываеются все busy диалоги
        //поэтому приходится пересоздавать
        this.loadingDialog.close();
        this.loadingDialog = new sap.m.BusyDialog();
        this.loadingDialog.open();

        this.loadingDialog.setTitle(this.getText("LoadInit") + " " + sZspId); 
        jQuery('#sap-ui-blocklayer-popup').css({'opacity': 1, 'backgroundColor': '#FFFFFF'})
        //Запрашиваем основную информацию, далее резолвим результат в набор "по годам"
        this.oView.getModel().read('/ZSupoSet(\'' + sZspId + '\')', {
          success: function (oData) {
        	  oZSupoDfd.resolve(oData);
          }.bind(this),
          urlParameters:{
            $expand:'ToZImplForm,ToZeqCrit,ToZPs_aggr,ToZEntity,ToZWorkshop,ToLongText,ToOTPBSubprog,ToProjDes,ToPr,ToSubprog,ToOzm,ToPriority,ToPriority/ToPr,ToPrtyp,ToZDwntmFc,ToZReserv'
          }
        });

        //Обработчик подхватывает завершение загрузки основной инфы и запрашивает данные по годам Zsubprog
        oZSupoDfd.done(function (oZSupo) {
        	this.oView.getModel().read('/ZSupoByYearSet', {
        		success: function (byYearData) {
        			oZSupo.yearData  = byYearData.results;
        			if(oZSupo.ZpsPrtyp === '1') {
        				this.oView.byId('GroupA').setNextStep(this.byId('BudgetStep'));
        			}
        			if(oZSupo.ZfuncDir === '02' || oZSupo.ZfuncDir === '10'){
        				oGroupBDfd.resolve(oZSupo);
        			} else if (this.sharedFormatter.checkIsIt(oZSupo.ZfuncDir)) {
        				//если оборудование по группе ПО
        				if(oZSupo.ToIT.ZpsGrp == '2'){
        					var oEstimStep = this.oView.byId('EstimateProbabilityB');
        					this.oView.byId('GroupD').setNextStep(oEstimStep);
        				}
        				oItDfd.resolve(oZSupo);
        			} else if (this.sharedFormatter.checkIsLaboratory(oZSupo.ZfuncDir, oZSupo.Zpslabflg === 'X')) {
        				oLaboratoryDfd.resolve(oZSupo);
        			} else {
        				oEstimateDfd.resolve(oZSupo);
        			}
        		}.bind(this),
        		filters:[ new Filter('ZspPId', FilterOperator.EQ, sZspId) ],
        		sorters:[ new sap.ui.model.Sorter('Calyear') ],
        		urlParameters:{
        			$expand:'ToMduse,ToAcqty,ToNacqr,ToProjph'
        		}
        	});
        }.bind(this));

        oItDfd.done(function (oZsupo) {
        	this.oView.getModel().read("/ITSet('" + sZspId + "')", {
        		success: function (oItData) {
        			oZsupo.ToIT = oItData;
        			oEstimateDfd.resolve(oZsupo);
        		}.bind(this),
                urlParameters:{
                	$expand:'ToZPsPrjty,ToITCriteria,ToZprjcrIt'
                }
        	});
        }.bind(this));
        
        oLaboratoryDfd.done(function (oZsupo) {
        	this.oView.getModel().read("/LaboratorySet('" + sZspId + "')", {
        		success: function (oLaboratoryData) {
        			oZsupo.ToLaboratory = oLaboratoryData;
        			oEstimateDfd.resolve(oZsupo);
        		}.bind(this),
                urlParameters:{
                	$expand:"ToZpsLab,ToZpsExpctCalc,ToZtechcond,ToZpsStatIn,ToZpsResAv,ToZpsDetDContP,ToZpsReqEco,ToZpsReqDoc"
                }
        	});
        }.bind(this));

        oGroupBDfd.done(function (oZSupo) {
        	this.oView.getModel().read("/OTPBandEcologySet('" + sZspId + "')", {
        		success: function (groupBdata) {
        			oZSupo.groupB = groupBdata;
        			var oValuationStep = this.oView.byId('OTPBValuation');
        			//Если есть остаточный риск, добавлем соотвтествующий шаг
        			if(oZSupo.groupB.ZpsRrsk){
        				oZSupo.groupB.ZpsRrsk = true;
        				oValuationStep.setNextStep(this.oView.byId('OTPBLeftoverRisk'));
        			}
        			oEstimateDfd.resolve(oZSupo);
        		}.bind(this),
        		urlParameters:{
        			$expand:'ToOTPBSubprog,ToRskrt,ToDnwty,ToRrskrt,ToCorrectiveAction,ToChbxSave,ToChbxSave/ToDesc'
        		}
        	})
        }.bind(this));

        oEstimateDfd.done(function (oZSupo) {
          this.oView.getModel().read('/ZSupoBudgetByYearSet', {
            success: function (budgetData) {
              oZSupo.budgetData = budgetData.results;
              
        	  // Проставляем первый год из сервиса
              oZSupo.budgetData.forEach(function(element, index, array) {
            	  var iYear = parseInt(element.Calyear); 
            	  if (iYear != 0 && iYear < this._nFirstYear)
            		  this._nFirstYear = iYear;
              }.bind(this));
              
              oFileSetDfd.resolve(oZSupo);
            }.bind(this),
            filters: [
              new sap.ui.model.Filter('ZspPId', 'EQ', sZspId)
            ],
            sorters:[
              new sap.ui.model.Sorter('Calyear')
            ]
          })
        }.bind(this));

        oFileSetDfd.done(function (oZSupo) {
          new ODataModel(this.sServicePrefix + '/sap/opu/odata/sap/Z_PSUP_SERVICES_SRV/',{useBatch:false})
            .read('/FileSet', {
              success: function (response) {
                oZSupo.files = response.results;
                oBudgetDfd.resolve(oZSupo);
              },
              filters: [
                new Filter('ProjName', FilterOperator.EQ, sZspId),
                new Filter('Deleted', FilterOperator.NE, 'X')
              ]
            })
        }.bind(this));

        //Финальный обработчик данных для изменений
        oBudgetDfd.done(function (oZSupo) {
        	this.oEngineModel.setData({});
        	
        	oInitParam.oBudget = this.createBudgetObject(oZSupo.ToZEntity.Currency, this._nFirstYear, CONSTANTS.COUNT_YEAR);
        	this.oEngineModel.initialize(oInitParam, oZSupo);
        	//Translate
        	var OEM = this.oEngineModel.getData();
        	this.onReplaceInternText(OEM);
        	this.oEngineModel.setData(OEM);
        	this.onFuncDirChange(oZSupo.ZfuncDir);

        	this.oView.getModel().callFunction('/CheckIsCurrentApprover', {
        		method: 'POST',
        		urlParameters: {
        			IV_P_ID: this.oEngineModel.getProperty('/projectNumber')
				},
				success: function(response) {
				    this.getView().getModel('settings').setProperty('/roles/isCurrentApprover', response.Code === 'X');
				}.bind(this)
			});
        	
        	var bFirstOpenShbl = this.oEngineModel.checkIsFirstOpenShbl();
        	
			if(FUNCTIONAL_AREAS.getGroupById(oZSupo.ZfuncDir) === 'GROUPA') {
				this._checkAggregateCount(bFirstOpenShbl);
			}
        	
        	if (bFirstOpenShbl) {
        		this._closeLoading();
        	} else {
            	//раскрываем все шаги
            	this.oWizard.attachStepActivate(function openAllSteps(oEvent) {
            		setTimeout(function () {
            			var sStepName = this.oWizard.getProgressStep().getTitle();
            			this.loadingDialog.setText(sStepName);
            			if(this.oWizard.getProgressStep().getNextStep()) {
            				this.oWizard.nextStep();
            			} else {
            				this.oWizard.setBusy(false);
            				
            				if(oZSupo.ZfuncDir === '07') {
            					this._fireSelectChange({
            						sId: 'zeqCatCb'
            					});
            					
            					this._fireSelectChange({
            						sId: 'labEquipType'
            					});
            				}
            				
            				if(this.sharedFormatter.checkIsIt(oZSupo.ZfuncDir)) {
            					//вручную пересобираем любую таблицу по ПО, чтобы пересчитались проценты
            					this._rebuildTableAnswers('obligation')
            				}
            				
            				if(oZSupo.ZfuncDir === this._FN_CORPORATE_SECURITY) {
            					this._fireSelectChange({
            						sId: "selPodp",
            						oParameters: {
            							isLoadedProject: true
            						}
            					});
            				}
            				
            				if(sRouteName === 'view') {
            					this.oEngineModel.setProperty('/bView', true);
            					this.navToReview();
            					
            					setTimeout(function () {
            						this._closeLoading();
            					}.bind(this), 100)
            				} else {
            					this._closeLoading();
            				}
            				//Отвязываем функцию от события чтобы при изменении шаги автоматически не раскрывались
            				this.oWizard.detachStepActivate(openAllSteps, this);
            			}
            		}.bind(this));
            	}, this);
            	
            	if(this.oWizard.getProgressStep().getNextStep()){
            		this.oWizard.nextStep();
            	}
        	}
        }.bind(this))
      } else{
        this.oEngineModel.initialize(oInitParam);
        this._closeLoading();
      }
    },

    /**
     * Корректное закрытие первоначальной загрузки с восстановлением стилей
     * @private
     */
    _closeLoading: function () {
      if(this.loadingDialog){
        this.loadingDialog.close();
        jQuery('#sap-ui-blocklayer-popup').css({'opacity': 0.6, 'backgroundColor': '#000000'})
      }
    },
    
    /**
     * функция запускающая Select change после прогрузки
     */
    _fireSelectChange: function (oInfo) {
    	var oSelect = sap.ui.getCore().byId(oInfo.sId);
    	if (oSelect) {
    		var oItem = oSelect.getSelectedItem();
    		if (!oItem) { //Если пока состояние не прогрузилось
        		var oBinding = oSelect.getBinding("items");
        		var fnChange = function() {
        			var mParameters = {
        				selectedItem: oItem
        	        };
        			if (oInfo.oParameters) {
        				mParameters.oParameters = oInfo.oParameters;
        			}
    				oSelect.fireChange(mParameters);
    				oSelect.detachDataReceived(fnChange);
        		};
        		
        		if (oBinding) {
        			oBinding.attachDataReceived(fnChange) ;
        		}
    		} else { //если элемент прогрузился
    			this._fireChangeBatch([oInfo]);
    		}
    	}
    },

    /**
     * функция запускающая события change группы селектов
     * @private
     */
    _fireChangeBatch: function (aIds) {
      aIds.forEach(function (oId) {
        var oSelect = sap.ui.getCore().byId(oId.sId);
        if (oSelect) {
          var mParameters = {
        	selectedItem: oSelect.getSelectedItem()
          };
          if(oId.oParameters) {
        	  mParameters.oParameters = oId.oParameters;
          }
          oSelect.fireChange(mParameters);
        }
      })
    },

    _initUset: function(){
      var oServiceModel = this.getOwnerComponent().getModel();
      var oEngineModel = this.oEngineModel;
      if(!sap.ushell || !oServiceModel){
        responsible = {
          Login: ' '
      };
      oEngineModel.setProperty('/responsible', responsible);
        return;
      }
      var oUserInfo = sap.ushell.Container.getUser();
      var sLogin = oUserInfo.getId();
      oServiceModel.read('/UserSet',{
        filters: [
          new sap.ui.model.Filter({
            path: "Login",
            operator: "EQ",
            value1: sLogin
          })
        ],
        success: function(){
          var oUserData = arguments[0].results.pop();
          var  responsible = {
              Login: sLogin,
              firstName: oUserData.Firstname,
              secondName: oUserData.Middlename,
              familyName: oUserData.Lastname,
              fio: [oUserData.Lastname,oUserData.Firstname,oUserData.Middlename].join(' '), //Протокол тестирования ПП1 №2
              position: oUserData.Function, //Протокол тестирования ПП1 №2
              contact:'', //Протокол тестирования ПП1 №2
              telephone: oUserData.Telephone,
              email: oUserData.Email
          };
          oEngineModel.setProperty('/responsible', responsible);
        }
      });
    },
    onAfterRendering: function () {
      if(!this._bEdit){
        this._initUset();
      }
      this._getRates();
    },

    _getStepController: function (sControllerName) {
      jQuery.sap.require(sControllerName);
      jQuery.extend(this, jQuery.sap.getObject(sControllerName));
    },

    /**
     * Функция наполняющая engine модель курсами валют по годам
     * @private
     */
    _getRates: function () {
      var oModel = this.oView.getModel();
      oModel.read('/ChangeRatesSet',{
        success: function (oData) {
          var aRates = oData.results;
          var oRatesData = {};

          aRates.forEach(function (rate) {
            if(!oRatesData[rate.Calyear]) oRatesData[rate.Calyear] = {year: rate.Calyear};
            oRatesData[rate.Calyear][rate.Currency] = rate.Zexrate;
          });

          this.oRatesModel.setData(oRatesData);
        }.bind(this)
      })
    },
    /**
     * Функция получающая табличные данные по оценке вероятности отказа
     * @private
     */
    _getTableEstimProbB: function () {
        var oModel = this.oView.getModel();
        var aYears = this.oEngineModel.getProperty('/years');
        var oEstimProbParts = [
        		{
        			name: 'obligation',
        			path: '/MduseSet',
        			crit: 'ZpsMduse'
        		},
        		{
        			name: 'risks',
        			path: '/NacqrSet',
        			crit: 'ZpsNacqr'
        		},
        		{
        			name: 'type',
        			path: '/AcqtySet',
        			crit: 'ZpsAcqty'
        		}	
        ];
        oEstimProbParts.forEach(function (part) {
        oModel.read(part.path, {
          success: function (oData) {
            var aPart = oData.results;
            aYears.forEach(function (year) { 
            	aPart.forEach(function (itemPart) {
                	var answer = {};
                	answer.value = itemPart.ZpsPts;
                	answer.weight = itemPart.ZpsWght;
                	answer.index = itemPart[part.crit];
                	answer.selected = year.estimateB[part.name].selectedIndex==answer.index ? true : false;
                	answer.editable = true;
                	year.estimateB[part.name].answers.push(answer);
                }.bind(this))  
              }.bind(this))
          }.bind(this)
        })
       }.bind(this))
      },

    onNewsView: function(oEvent) {
        this.oNewsPopover.open(oEvent.getSource(), this._bEdit ? NEWS_TYPE.CHANGE : NEWS_TYPE.INPUT);
    },
    
    onInitalStepActivate: function (oEvent) {
        var oStep = oEvent.getSource();
        
        // интернационализация EngineModel
        var oEMD = this.oEngineModel.getData();
        this.onReplaceInternText(oEMD);
        this.oEngineModel.setData(oEMD);
        
        sap.ui.require(['initiatives/controllers/stepControllers/initialStep'], function(initialStep) {
            jQuery.extend(this, initialStep);
            
            var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.GeneralInfo", this);
            oStep.insertContent(oStepContent, 0);

            //обнуляем инпут, чтобы не было пустых скобочек
            sap.ui.getCore().byId('workshopName').setValue('');

            this.oTransitoryProjectCB = sap.ui.getCore().byId('transitoryProject');
            this.oProjectCharacteristics = sap.ui.getCore().byId('projectCharacteristic');
        }.bind(this));
    },
    
	unrealizedProjectSelectedChanged: function(oEvent) {
		var bIsSelected = oEvent.getParameter("selected");
		if (bIsSelected) {
			this.oTransitoryProjectCB.setSelected(true);
			this.oTransitoryProjectCB.fireSelect({selected: true});
		}
	},
	
	transitoryProjectSelectedChanged: function(oEvent) {
		var bIsSelected = oEvent.getParameter("selected");
		if (bIsSelected) {
			var oRadioBtnGrp = this.oProjectCharacteristics,
				oButtons = oRadioBtnGrp.getButtons();
			oButtons.forEach(function(oButton, index) {
				var oButtonContext = oButton.getBindingContext(),
					iCharacteristicId = oButtonContext.getModel().getProperty(oButtonContext.getPath() + "/Id")
				if (iCharacteristicId === this._PROJECT_CHARACTERISTIC_DEFAULT) {
					oRadioBtnGrp.setSelectedIndex(index);
					oRadioBtnGrp.fireSelect({selectedIndex: index});
				}
			}.bind(this));
		}
		else {
			this.oProjectCharacteristics.setSelectedIndex(-1);
			this.oProjectCharacteristics.fireSelect({selectedIndex: -1});
		}
	},
	
	projectCharacteristicSelectionChange: function(oEvent) {
		if (oEvent.getParameter("selectedIndex") !== -1) {
			var oSelectedButtonContext = oEvent.getSource().getSelectedButton().getBindingContext(),
			sReasonId = this.getModel().getProperty(oSelectedButtonContext.getPath() + "/Id");
			this.oEngineModel.setProperty("/projectCharacteristic/id", sReasonId);
			this.oEngineModel.setProperty("/projectCharacteristic/text", oSelectedButtonContext.getProperty("Txtlg"));
		}
		else {
			this.oEngineModel.setProperty("/projectCharacteristic/id", "00");
			this.oEngineModel.setProperty("/projectCharacteristic/text", "");
		}
	},
	
    /**
     * Обработчик завершения начального шага
     * @param oEvent
     */
    onInitialStepComplete: function (oEvent) {
      this._sCashedFunDirId = this.oEngineModel.getProperty('/funcDir/id');
      this._sCashedLargeProj = this.oEngineModel.getProperty('/largeProject');
      this._bStepsBuilded = true;
    },

    _destroyStepContent: function (oEvent) {
      oEvent.getSource().destroyContent()
    },

    onGroupAStepActivate: function (oEvent) {
    	this._destroyStepContent(oEvent);
        var oStep = oEvent.getSource();
    	this.oGroupAStep = this.byId('GroupA');
    	
    	
    	sap.ui.require(["initiatives/controllers/stepControllers/groupA/mainInfo"], function(mainInfo) {
    		jQuery.extend(this, mainInfo);
    		
        	var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.GroupA", this);
        	oStep.insertContent(oStepContent, 0);
        	
        	var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
        	if(this._bEdit){
        		this._setNextStepA(this.oEngineModel.checkIsFirstOpenShbl());
        	}
        	
        	if (sFuncDir === FUNCTIONAL_AREAS.GROUPA.ENERGETICS || sFuncDir === FUNCTIONAL_AREAS.GROUPA.LOGISTICS || sFuncDir === FUNCTIONAL_AREAS.GROUPA.PRODUCTION) {
        		var oFuncDirSelect = sap.ui.getCore().byId('idFuncDirA');
        		oFuncDirSelect.getBinding("items").filter([new Filter("ZfuncDir", FilterOperator.EQ, sFuncDir)]);
        	}
    	}.bind(this));
    },
    
    _setNextStepA: function (bChange) {
		var funcDirId = parseInt(this.oEngineModel.getProperty('/funcDir/id'), 10);
        var ProjectKey = parseInt(this.oEngineModel.getProperty('/groupA/ZpsPrtyp'), 10);
		
    	if (funcDirId === 5) {
            if (ProjectKey === 3 || ProjectKey === 4) {
            	this.byId('GroupA').setNextStep(this.byId('BudgetStep'));
            	if (bChange) {
            		this._bindGroupAValidation({
            			parts: [
            				{path: 'engine>/groupA/ZpsPrtyp'},
            				{path: 'engine>/groupA/ZpsMlage'},
            				{path: 'engine>/groupA/ZpsCmdt'},
            				{path: 'engine>/riskDecr'},
            				{path: 'engine>/targetSubprog/id'}
            			],
            			formatter: this.validators.simpleValidator
                    });
            	}
            } else if (ProjectKey === 5) { //если прочие проекты
            	this.byId('GroupA').setNextStep(this.byId('EstimateProbability'));
            	
            	if (bChange) {
            		this._bindGroupAValidation({
                        parts: [
                          {path: 'engine>/groupA/ZpsPrtyp'},
                          {path: 'engine>/groupA/equip/name'},
                          {path: 'engine>/groupA/aggregate/downtimePenalty/units'},
                          {path: 'engine>/groupA/crit/id'},
                          {path: 'engine>/riskDecr'},
                          {path: 'engine>/targetSubprog/id'}
                        ],
                        formatter: this.validators.simpleValidator
            		});
            	}

            } else {
              this.byId('GroupA').setNextStep(this.byId('EstimateProbability'));
              if (bChange) {
            	  this._bindGroupAValidation({
                      parts: [
                        {path: 'engine>/groupA/ZpsPrtyp'},
                        {path: 'engine>/groupA/equip/name'},
                        {path: 'engine>/groupA/ZpsMlage'},
                        {path: 'engine>/groupA/ZpsCmdt'},
                        {path: 'engine>/groupA/aggregate/downtimePenalty/units'},
                        {path: 'engine>/groupA/crit/id'},
                        {path: 'engine>/riskDecr'},
                        {path: 'engine>/targetSubprog/id'}
                      ],
                      formatter: this.validators.simpleValidator
                  });
              }
            }
          } else {
              var oGroupAValidation = {
                  parts: [],
                  formatter: this.validators.simpleValidator
              };

              if (ProjectKey === 1 || ProjectKey === 5) {
            	  this.byId('GroupA').setNextStep(this.byId('EstimateProbability'));
            	  this.byId('BudgetStep').setNextStep(this.byId('CalcRisk'));
            	  if (bChange) {
            		  oGroupAValidation.parts = [
            			  {path: 'engine>/groupA/equip/name'},
            			  {path: 'engine>/groupA/aggregate/downtimePenalty/units'},
            			  {path: 'engine>/groupA/crit/id'},
            			  {path: 'engine>/riskDecr'}
            		  ];
            	  }
              } else if (ProjectKey === 2) {
                this.byId('GroupA').setNextStep(this.byId('EstimateProbability'));
                this.byId('BudgetStep').setNextStep(this.byId('CalcRisk'));
                if (bChange) {
                    oGroupAValidation.parts = [
                        {path: 'engine>/realizationForm/id'},
                        {path: 'engine>/groupA/equip/name'},
                        {path: 'engine>/groupA/aggregate/downtimePenalty/units'},
                        {path: 'engine>/groupA/crit/id'},
                        {path: 'engine>/riskDecr'}
                    ];
                }
              } else if (ProjectKey === 3 || ProjectKey === 4) {
                this.byId('GroupA').setNextStep(this.byId('BudgetStep'));
                this.byId('BudgetStep').setNextStep(this.byId('uploadStep'));
                if (bChange) {
                    oGroupAValidation.parts = [
                        {path: 'engine>/realizationForm/id'},
                        {path: 'engine>/riskDecr'}
                    ];
                }
              }
              
              if (funcDirId !== 3) {
                  if (bChange) {
                      oGroupAValidation.parts.push({path: 'engine>/targetSubprog/id'});
                  }
              }
              if (bChange) {
            	  this._bindGroupAValidation(oGroupAValidation);
              }
          }
    },
    
    _bindGroupAValidation: function(oValidObj) {
		var bFs = this.oEngineModel.getProperty('/groupA/isFireGuard'),
			sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
		if (bFs) {
			oValidObj.parts.push({path: 'engine>/groupA/FsType/id'});
		}
		this.byId('GroupA').bindProperty("validated", oValidObj);
	},
    
    onGroupAStepComplete: function (oEvent) {
      this._sCashedZpsPrtyp = this.oEngineModel.getProperty('/groupA/ZpsPrtyp');
      this._bGroupARebuildRequired = true;
    },

    /**
     * Обработчик перехода на шаг бюджета
     */
    onBudgetStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      var oStep = oEvent.getSource();
      
      sap.ui.require(["initiatives/controllers/stepControllers/budgetStep"], function(budgetStep) {
    	  jQuery.extend(this, budgetStep);
          var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.Budget", this);
          
          //Если проект текущего года, то не отображаем CheckBox и Vbox
          var creationDate = this.oEngineModel.getProperty('/creationDate');
          if (creationDate.getFullYear() !== new Date().getFullYear()) {
        	sap.ui.getCore().byId('earlyImplementation').setVisible(false);
        	sap.ui.getCore().byId('earlyImplementationReasonContainer').setVisible(false);
          }
          else {
        	  if (this.oEngineModel.getProperty('/earlyImplementation/ZpsImpl') === true) {
        		  sap.ui.getCore().byId('earlyImplementationReasonContainer').setVisible(true);
        	  }
          }
          
          oStep.insertContent(oStepContent, 0);

          //Собираем пути по фазам проекта для биндинга валидации
          var aYears = this.oEngineModel.getProperty('/years');
          var aBudgetValidatePaths = aYears.map(function (yearData, index) {
            return 'engine>/years/' + index + '/projectPhase/id'
          });
          
          aBudgetValidatePaths.push('engine>/earlyImplementation/ZpsImpl', 'engine>/earlyImplementation/reason', 'engine>/earlyImplementation/description');
          
          aBudgetValidatePaths = aBudgetValidatePaths.concat(this.oEngineModel.builderBudgetValidators());
          
          oStep.bindProperty('validated', {
            parts: aBudgetValidatePaths,
            formatter: this.validators.budgetValidator
          });

          //Ребиндим бюджет без НДС - первым всегда показывается бюджет без НДС
          this._rebindBudget(false);
          
          this.budgetStep.setNextStep.call(this, oStep);
      }.bind(this));
    },
    
    /**
     * Обработчик перехода на шаг вероятности отказа
     * @param oEvent
     */
    onEstimateProbabilityStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupA.estimateProbability');
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.EstimateProbability", this);

      var oEstimateStep = oEvent.getSource();

      //Собираем пути биндингов строк таблиц для проверки на заполненость
      var aYears = this.oEngineModel.getProperty('/years');
      var aEstimateProbabilityBinding = aYears.reduce(function (prev, cur, index) {
        prev.push('engine>/years/' + index + '/groupA/history/selected');
        prev.push('engine>/years/' + index + '/groupA/techSost/selected');
        prev.push('engine>/years/' + index + '/groupA/toppr/selected');
        prev.push('engine>/years/' + index + '/groupA/statDelay/selected');
        return prev
      }, []);

      oEstimateStep.bindProperty('validated', {
        parts: aEstimateProbabilityBinding,
        formatter: this.validators.simpleValidator
      });


      oEstimateStep.addContent(oStepContent);

      //workaround если мы в режиме редактирования принудительно пересчитываем
      //проценты вероятности отказа, т.к. из сервиса приходят обрезные
      if(this._bEdit){
        aYears.forEach(function (e, index) {
          this._recountProbability(index);
        }.bind(this))
      }
    },

    onEstimateProbabilityBStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      this._getTableEstimProbB();
      this._getStepController('initiatives.controllers.stepControllers.estimateProbabilityB');
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.EstimateProbabilityB", this);

      var oEstimateStep = oEvent.getSource();

      //Собираем пути биндингов строк таблиц для проверки на заполненость
      var aYears = this.oEngineModel.getProperty('/years');
      var aEstimateProbabilityBinding = aYears.reduce(function (prev, cur, index) {
        prev.push('engine>/years/' + index + '/estimateB/obligation/selected');
        prev.push('engine>/years/' + index + '/estimateB/risks/selected');
        prev.push('engine>/years/' + index + '/estimateB/type/selected');
        return prev
      }, []);

      oEstimateStep.bindProperty('validated', {
        parts: aEstimateProbabilityBinding,
        formatter: this.validators.simpleValidator
      });

      oEvent.getSource().addContent(oStepContent);
    },


    onCalcRiskStepActivate: function (oEvent) {
    	this._destroyStepContent(oEvent);
    	var oStep = oEvent.getSource();
    	
    	sap.ui.require(["initiatives/controllers/stepControllers/groupA/CalcRisk"], function(CalcRisk) {
    		jQuery.extend(this, CalcRisk);
    		
    		var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.CalcRisk", this);
    		oStep.insertContent(oStepContent, 0);
    		
    		if (this._bEdit) {
    			var oDfs = sap.ui.getCore().byId("downtimeFactorsSelect");
    			oDfs.getBinding("items").filter(new Filter("Zreserve", FilterOperator.EQ, this.oEngineModel.getProperty('/reserve/id')));
    		}
    		
    		oStep.bindProperty('validated', {
    			parts: this._oValidationBinding.CalcRisk.inProcess(this.oEngineModel.getProperty('/groupA/ZpsPrtyp')), //.init()
    			formatter: this.validators.simpleValidator
    		});
    	}.bind(this));
    },

    onGroupBActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupB.mainInfo');
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.GroupB", this);
      oEvent.getSource().addContent(oStepContent);


      var oSelect = sap.ui.getCore().byId('groupBTargetSubprogSelect');
      var oContext = oSelect.getBinding('items');
      var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
      oContext.filter([new sap.ui.model.Filter({path:'ZfuncDir', operator:'EQ', value1: sFuncDir})])
      
      oEvent.getSource().bindProperty('validated', {
          parts: this._oValidationBinding.GroupB.SourceDataVal.inProcess(sFuncDir),
          formatter: this.validators.simpleValidator
      });
    },

    onOTPBDetectStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupB.riskDetection');
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.OTPBDetect", this);
      oEvent.getSource().addContent(oStepContent);
    },

    onOTPBValuationStepActivate: function (oEvent) {
    	this._destroyStepContent(oEvent);
    	var oStep = oEvent.getSource();
    	var sConfigPath = jQuery.sap.getModulePath('initiatives.controllers.model');
    	var sConfFile = this.oEngineModel.getProperty('/funcDir/id') == FUNCTIONAL_AREAS.GROUPB.SAFETY ? '/OTPBriskCalcTableConf.json' : '/EcoRiskCalcTableConf.json';
    	var oRiskTableConfig = jQuery.sap.syncGetJSON(sConfigPath + sConfFile).data;
    	
        /*для моделей 'OTPBriskCalcTableConf.json' и 'EcoRiskCalcTableConf.json'
         * подменяем все надписи на значения из модели i18n */
    	this.onReplaceInternText(oRiskTableConfig);
    	
        //создаём путь для таблицы остаточного риска
        this.oEngineModel.setProperty('/groupB/riskConfig', oRiskTableConfig);
        var test = jQuery.extend(true, {}, oRiskTableConfig.possibility);
        this.oEngineModel.setProperty('/groupB/riskConfig/leftoverPossibility', test);
    	
    	sap.ui.require(['initiatives/controllers/stepControllers/groupB/riskValuation'], function(riskValuation) {
    		jQuery.extend(this, riskValuation);
    		var oStepContent = sap.ui.xmlfragment('initiatives.views.stepFragments.OTPBValuation', this);
    		oStep.insertContent(oStepContent, 0);
    		
    		oStep.bindProperty('validated', {
    			parts: this._oValidationBinding.OTPBVal.inProcess(this.oEngineModel.getProperty('/funcDir/id')),
    			formatter: this.validators.simpleValidator
    		});
    	}.bind(this));
    },
    
    onOTPBValuationStepComplete: function() {
        var bResidualRisk = this.oEngineModel.getProperty("/groupB/bLeftoverRisk");
        var oValuationStep = this.oView.byId('OTPBValuation');
        if (bResidualRisk) {
            oValuationStep.setNextStep(this.oView.byId('OTPBLeftoverRisk'));
        } else {
            oValuationStep.setNextStep(this.oView.byId('BudgetStep'));
        }
    },

    onOTPBLeftoverRiskActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      var oStep = oEvent.getSource();
      var sConfigPath = jQuery.sap.getModulePath('initiatives.controllers.model'),
      	  sConfFile = this.oEngineModel.getProperty('/funcDir/id') == '02' ? '/OTPBriskCalcTableConf.json' : '/EcoRiskCalcTableConf.json',
      	  oRiskTableConfig = jQuery.sap.syncGetJSON(sConfigPath + sConfFile).data;
      
      sap.ui.require(["initiatives/controllers/stepControllers/groupB/leftoverRisk"], function(leftoverRisk) {
          jQuery.extend(this, leftoverRisk);
          
          var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupB.OTPBLeftoverRisk", this);
          oStep.insertContent(oStepContent, 0);

          this.onReplaceInternText(oRiskTableConfig);
          
          var oRiskModel = new JSONModel(oRiskTableConfig);
          var oTable = sap.ui.getCore().byId('LeftoverRiskTable');
          oTable.setModel(oRiskModel, "risk");
          
          oStep.bindProperty('validated', {
              parts: this._oValidationBinding.GroupB.OTPBLeftoverRisk,
              formatter: this.validators.simpleValidator
          });
      }.bind(this));
    },

    onLabControlStepActivate: function(oEvent){
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupV.labControl');
      this.labControl.onAfterFragentLoad.apply(this, arguments);
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupV.labControl", this);
      var oStepLab = oEvent.getSource();
      oStepLab.addContent(oStepContent);
      
      oStepLab.bindProperty('validated', {
    	  parts: this._oValidationBinding.GroupV.init(),
    	  formatter: this.validators.simpleValidator
      });
      
      this.labControl.onInit.apply(this, arguments);
    },

    onLabControlRiskiStepActivate: function(oEvent){
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupV.riski');
      this.riski.onAfterFragentLoad.apply(this, arguments);
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupV.riski", this);
      var oLabControlRiski = oEvent.getSource();
      oLabControlRiski.addContent(oStepContent);
      
      sap.ui.getCore().byId('labCb').getBinding("items").filter([
    	  new Filter("Zentity", FilterOperator.EQ, this.oEngineModel.getProperty('/company/id'))
      ]);
      
      oLabControlRiski.bindProperty('validated', {
    	  parts: this._oValidationBinding.GroupV_riski.init(),
    	  formatter: this.validators.simpleValidator
      });
      
      this.riski.onInit.apply(this, arguments);
    },
    onLabControlRiskiCalcStepActivate: function(oEvent){
      this._destroyStepContent(oEvent);
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupV.fragments.ZpsExpctCalc", this);
      oEvent.getSource().addContent(oStepContent);
    },
    onCorpSecStepActivate:function(oEvent){
    	this._destroyStepContent(oEvent);
        var oStep = oEvent.getSource();
        
        sap.ui.require(["initiatives/controllers/stepControllers/groupG/CorpSecurity"], function(CorpSecurity) {
        	jQuery.extend(this, CorpSecurity);
            
        	var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupG.CorpSecurity", this);
            oStep.insertContent(oStepContent, 0);
            
            oStep.bindProperty('validated', {
            	parts: this.corpSec.stepValidation.inProcess.call(this),
            	formatter: this.validators.simpleValidator
          });
        }.bind(this));
    },
    onSocialSphStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupE.socialSph", this);
      oEvent.getSource().addContent(oStepContent);
    },
    
    onEcoDetectStepActivation: function (oEvent) {
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupB.ecoRiskDetect');

      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.EcoDetect", this);
      oEvent.getSource().addContent(oStepContent);
    },

    onGroupDStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      this._getStepController('initiatives.controllers.stepControllers.groupD.generalInfo');

      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupD.groupD", this);
      var oGroupDStep = oEvent.getSource();
      oGroupDStep.addContent(oStepContent);
      
      sap.ui.getCore().byId('groupDSubprog').getBinding("items").filter([
    	  new Filter("ZfuncDir", FilterOperator.EQ, this.oEngineModel.getProperty('/funcDir/id'))
      ]);
      
      oGroupDStep.bindProperty('validated', {
    	  parts: this._oValidationBinding.GroupD.init(),
    	  formatter: this.validators.simpleValidator
      });
    },

    onGroupDResultStepActivate: function (oEvent) {
      this._destroyStepContent(oEvent);
      var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupD.result", this);
      oEvent.getSource().addContent(oStepContent);
    },

    onCalcRiskVStepActivate: function (oEvent) {
    	this._destroyStepContent(oEvent);
    	var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.groupV.CalcRiskV", this);
    	var oCalcRiskVStep = oEvent.getSource();
    	oCalcRiskVStep.addContent(oStepContent);
    	
    	var oFormDownTime = sap.ui.getCore().byId('CalcRiskVDownTime');
    	if (oFormDownTime && oFormDownTime.getVisible()) {
    		
    		if (this._bEdit) {
    			var oDfs = sap.ui.getCore().byId("downtimeFactorsSelect");
    			oDfs.getBinding("items").filter(new Filter("Zreserve", FilterOperator.EQ, this.oEngineModel.getProperty('/reserve/id')));
    		}
    		
    		oCalcRiskVStep.bindProperty('validated', {
    			parts: this._oValidationBinding.CalcRiskV.init(),
    			formatter: this.validators.simpleValidator
    		});
    	}
    },

    onUploadStepActivate: function (oEvent) {          
        //кешируем аплоадеры
    	this._aFileUploaders = oEvent.getSource().getContent()[0].byId('uploader').getItems();
    },
    
    onCommentStepActivate: function (oEvent) {
    	this._destroyStepContent(oEvent);
    	var oStepContent = sap.ui.xmlfragment("initiatives.views.stepFragments.Comment", this);
    	oEvent.getSource().addContent(oStepContent);
    },


    /**
     * Обновляем номер проекта через сервис
     * @private
     */
    _refreshProjectNumber: function () {
      var sProjectname = this.oEngineModel.getProperty('/projectNumber');

      if(!this._bEdit){
        var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
        var sCompanyId = this.oEngineModel.getProperty('/company/id').substr(2);
        var prefix = sCompanyId + '_' + sFuncDir;
        this.oView.getModel()
          .read("/ZProjprostfixSet('" + prefix + "')", {
            success: function (oData) {

              var sFullName = prefix + '_' + oData.Zpostfix;

              this.oEngineModel.setProperty('/projectNumber', sFullName);
            }.bind(this)
          })
      }
    },

    /**
     * Корректное обновление модели с загружаемыми файлами
     * @private
     */
    _reviewUploads: function () {
      this._aFileUploaders.forEach(function (oUploader, index) {
        var aUploaderItems = oUploader.getItems();

        if(aUploaderItems.length > 0){
          var aItemsToStore = [];
          aUploaderItems.forEach(function (item) {
            aItemsToStore.push({
              name: item.getFileName(),
              url: item.getUrl()
            });
          });
          this.oEngineModel.setProperty('/upload/' + index + '/review', aItemsToStore);
        }else{
          this.oEngineModel.setProperty('/upload/' + index + '/review', []);
        }
      }.bind(this))
    },

    /**
     * Обработчик завершения визарда
     */
    navToReview: function () {
        var bView = this.oEngineModel.getProperty('/bView');
        var fnToReview = function() {
            //Запросим номер проекта
            this._refreshProjectNumber();
            this._reviewUploads();

            //ребиндим бюджет с НДС
            this._rebindBudget(true);

            this._fetchImplementationFormShortName();
              
            var oNavContainer = this.byId('appNavContainer');
            var oReviewPage = this.byId('reviewPage');
            oReviewPage.destroyContent();
            var oReviewContent = sap.ui.xmlfragment(this._reviewPageFragment, this);
            oReviewPage.addContent(oReviewContent);
            oNavContainer.to(oReviewPage);
        }.bind(this);
        
        if (!bView) {
            if (this.checkBeforeNavToReviewEdit()) {
                this.checkProjImplPeriod().then(this.checkBudget.bind(this)).then(function() {
                    fnToReview();
                }, function(sId) {
                    sap.ui.getCore().byId(sId).focus();
                    return;
                });
            } else {
                return;
            }
        } else {
            fnToReview();
        }
    },

    /**
     * Функция получающая короткое название формы реализации
     * @private
     */
    _fetchImplementationFormShortName: function () {
      var sImplFormId = this.oEngineModel.getProperty('/realizationForm/id');
      if(sImplFormId){
        this.oView.getModel().read('/ZImpl_frmSet', {
          async: false,
          filters:[
            new sap.ui.model.Filter('ZimplFrm', 'EQ', sImplFormId)
          ],
          success: function (response) {
            var sShortName = response.results[0].Txtsh;
            this.oEngineModel.setProperty('/realizationForm/shortName', sShortName)
          }.bind(this)
        })
      }
    },

    onReviewNavBack: function () {
      var oNavContainer = this.byId('appNavContainer');
      oNavContainer.to(this.byId('wizardPage'));
      this.oEngineModel.setProperty('/saveEnabled', true);
      this._rebindBudget(false); // При возврате возвращаем бюджет без НДС
    },

    createRequest: function () {
      if (!this._oRequestStatusDialog) {
        this._oRequestStatusDialog = sap.ui.xmlfragment("initiatives.views.fragments.RequestStatusDialog", this);
        this._oRequestStatusDialog.getBeginButton().attachPress(jQuery.proxy(this._oRequestStatusDialog.close, this._oRequestStatusDialog));
        this._oRequestStatusDialog.setModel(new JSONModel());
        this._oRequestStatusDialog.show = function(oStatus){
          // debugger;
          this.getModel().setProperty("/",oStatus);
          this.open();
        };
        this._oRequestStatusDialog.createPromise = function(){
          var lastIndex, that = this;
          that.aDeferredWait = [];
          return {
            addDeferred: function(){
                that.aDeferredWait.push(jQuery.Deferred());
            },
            /**
             *
             * @returns {T} последний добавленный Deferred
               */
            last: function(){
                return that.aDeferredWait.slice(-1).pop();
            },
            /**
             *
             * @returns {Promise.all}
             * Пример использование .then(function() {
                console.log("ALL PROMISES RESOLVED");
              });
               */
            getPromise: function(){
              return Promise.all(that.aDeferredWait);
            }
          }
        }
      }
      var bError = false;
      var oView = this.getView();
      var oFPromise = this._oRequestStatusDialog.createPromise();
      var oMainModel = this.oView.getModel();
      var oData = this.oEngineModel.getData();
      var aBatchChanges = [];
      var oDataValidate = new oDataValidateData(oMainModel);
      var oODataEngineModel = new ODataEngineModel(this.oEngineModel);
      function errHandler(oError) {
        oFPromise.last().reject
        if(!bError){
          oView.setBusy(false);
          var sMessage = this.getText("LoadErr");
          if (oError.statusCode === "412") {
        	  sMessage = this.getText("UpdateErr412");
          }
          sap.m.MessageToast.show(sMessage, {
        	  at: Popup.Dock.CenterCenter,
        	  animationDuration: 10000
          })
          bError = true;
        }
        this._refreshProjectNumber();
      }
      //Статус загрузки
      oView.setBusy(true);
      
      var oGeneralData = oODataEngineModel.getGeneralData(this._bEdit);
      if(!oDataValidate.validateByEntitySetName("ZSupoSet", oGeneralData.toServiceData)){
        //ERROR
        return ;
      }
      jQuery.extend(oGeneralData.toServiceData, oDataValidate.getFixedData());
      // debugger;
      oFPromise.addDeferred();

      oMainModel[this._bEdit ? 'update' : 'create'](oGeneralData.path, oGeneralData.toServiceData, {
        groupId: 'createPsup',
        success: oFPromise.last().resolve,
        error: errHandler.bind(this),
        merge:false
      });
      
      /* Записываем длинные тексты */
      // debugger
      var aLongTextData = oODataEngineModel.getLongTexts(this._bEdit)
      aLongTextData.forEach(function (oLongTextData) {
        oFPromise.addDeferred();
        oMainModel[this._bEdit ? 'update' : 'create'](oLongTextData.path, oLongTextData.toServiceData, {
          groupId: 'createPsup',
          success: oFPromise.last().resolve,
          error: errHandler.bind(this),
          merge:false
        });
      }.bind(this));
      
      //Поля по группе Б
      if(oData.funcDir.id === '02' || oData.funcDir.id === '10'){
        var oGroupBData = oODataEngineModel.getGroupBData(this._bEdit);
        oDataValidate.clearFixedData();
        if(!oDataValidate.validateByEntitySetName("OTPBandEcologySet", oGroupBData.toServiceData)){
          //ERROR
          return ;
        }
        jQuery.extend(oGroupBData.toServiceData, oDataValidate.getFixedData());
        // debugger;
        oFPromise.addDeferred();
        oMainModel[this._bEdit ? 'update' : 'create'](oGroupBData.path, oGroupBData.toServiceData, {
          groupId: 'createPsup',
          success: oFPromise.last().resolve,
          error: errHandler.bind(this),
          merge:false
        });

        var oCorrectionEvents = oODataEngineModel.getCorrectionEvents(this._bEdit);
        oDataValidate.clearFixedData();
        if(!oDataValidate.validateByEntitySetName("CorrectiveActionSet", oCorrectionEvents.toServiceData)){
          //ERROR
          return ;
        }
        jQuery.extend(oCorrectionEvents.toServiceData, oDataValidate.getFixedData());

        // debugger
        oFPromise.addDeferred();
        oMainModel[this._bEdit ? 'update' : 'create'](oCorrectionEvents.path, oCorrectionEvents.toServiceData, {
          groupId: 'createPsup',
          success: oFPromise.last().resolve,
          error: errHandler.bind(this),
          merge:false
        });
        
        var aCheckboxes = [];
        var aEmptyCheckboxes = [];
        jQuery.each(oData.groupB.checkBoxes, function(key, value){
          if(value === true){
            aCheckboxes.push(key);
          }else{
            aEmptyCheckboxes.push(key);
          }
        });
        //save type damage
        aCheckboxes.push(this.oEngineModel.getProperty("/groupB/OTPB/TypeDamage/id")); 
        
        aCheckboxes.forEach(function (chBoxId) {
          var chBData = {
            ZspPId: oData.projectNumber.toString(),
            ZpsChbx: chBoxId
          };
          oFPromise.addDeferred();
          sPath = '/ChbxSaveSet'+ (this._bEdit ? '(ZspPId=\''+ oData.projectNumber.toString() +'\',ZpsChbx=\''+chBoxId+'\')': '');
          oMainModel[this._bEdit ? 'update' : 'create'](sPath, chBData, {
            groupId: 'createPsup',
            success: oFPromise.last().resolve,
            error: errHandler.bind(this),
            merge:false
          });

        }.bind(this));

        if(this._bEdit){
          aEmptyCheckboxes.forEach(function (chBoxId) {
            var chBData = {
              ZspPId: oData.projectNumber.toString(),
              ZpsChbx: chBoxId
            };
            // oFPromise.addDeferred();
            sPath = '/ChbxSaveSet'+ (this._bEdit ? '(ZspPId=\''+ oData.projectNumber.toString() +'\',ZpsChbx=\''+chBoxId+'\')': '');
            oMainModel.remove(sPath, chBData, {
              groupId: 'createPsup',
              // success: oFPromise.last().resolve,
              error: errHandler.bind(this),
              merge:false
            });

          }.bind(this));
        }
      }
      //Поля по группе В
      if(this.oEngineModel.checkIsLaboratory()) {
          var oLabData = oODataEngineModel.getLabData(this._bEdit);
          oDataValidate.clearFixedData();
          if(!oDataValidate.validateByEntitySetName("LaboratorySet", oLabData.toServiceData)){
              //ERROR
              return ;
          }
          jQuery.extend(oLabData.toServiceData, oDataValidate.getFixedData());

          oFPromise.addDeferred();
          oMainModel[this._bEdit ? 'update' : 'create'](oLabData.path, oLabData.toServiceData, {
              groupId: 'createPsup',
              success: oFPromise.last().resolve,
              error: errHandler.bind(this),
              merge:false
          });
      }
      //Поля по группе Д
      if(this.oEngineModel.checkIsIt()) {
          var oITData = oODataEngineModel.getITData(this._bEdit);
          oDataValidate.clearFixedData();
          if(!oDataValidate.validateByEntitySetName("ITSet", oITData.toServiceData)){
              //ERROR
              return ;
          }
          jQuery.extend(oITData.toServiceData, oDataValidate.getFixedData());
          // debugger
          oFPromise.addDeferred();
          oMainModel[this._bEdit ? 'update' : 'create'](oITData.path, oITData.toServiceData, {
              groupId: 'createPsup',
              success: oFPromise.last().resolve,
              error: errHandler.bind(this),
              merge: false
          });

          var aCriteriaData = oODataEngineModel.getCriteriaData(this._bEdit);
          aCriteriaData.forEach(function (oCriteriaData) {
              oDataValidate.clearFixedData();
              if(!oDataValidate.validateByEntitySetName("ITCriteriaSet", oCriteriaData.toServiceData)){
                  //ERROR
                  return;
              }
              jQuery.extend(oCriteriaData.toServiceData, oDataValidate.getFixedData());

              oFPromise.addDeferred();
              oMainModel[this._bEdit ? 'update' : 'create'](oCriteriaData.path, oCriteriaData.toServiceData, {
                  groupId: 'createPsup',
                  success: oFPromise.last().resolve,
                  error: errHandler.bind(this),
                  merge: false
              });
        }.bind(this))

      }

      var aYearsData = oODataEngineModel.getYearsData(this._bEdit);
      aYearsData.forEach(function (oYearData) {
        oFPromise.addDeferred();
        oMainModel[this._bEdit ? 'update' : 'create'](oYearData.path, oYearData.toServiceData, {
          groupId: 'createPsup',
          success: oFPromise.last().resolve,
          error: errHandler.bind(this),
          merge:false
        });
      }.bind(this));

      var aBudgetData = oODataEngineModel.getBudgetData(this._bEdit);
      aBudgetData.forEach(function(oBudgetData) {
          if (oBudgetData.toServiceData.Zinvcost) {
              oFPromise.addDeferred();
            
              oMainModel[this._bEdit ? 'update' : 'create'](oBudgetData.path, oBudgetData.toServiceData, {
                  groupId: 'createPsup',
                  success: oFPromise.last().resolve,
                  error: errHandler.bind(this),
                  merge:false
                  });
          }
      }, this);

      oFPromise.getPromise().then(function(){
    	  
    	  //Сохраняем все файлы из аплоадеров
          //Получаем x-csrf токен
          this.oView.getModel().refreshSecurityToken();
          var sUrl2 = this.sServicePrefix + "/sap/opu/odata/sap/Z_PSUP_SERVICES_SRV/FileSet(FileName='*',ProjName='*',ObjType='*')/$value";
          var token;
          jQuery.ajax({
            url : sUrl2,
            type : "GET",
            async: false,
            beforeSend : function(xhr) {
              xhr.setRequestHeader("X-CSRF-Token", "Fetch");
            },

            success : function(data, textStatus, XMLHttpRequest) {
              this.token = XMLHttpRequest.getResponseHeader('X-CSRF-Token');
              this.oEngineModel.setProperty('/token', this.token);
            }.bind(this)
          });

          this._aFileUploaders.forEach(function (oUploader) {
        	  oUploader.upload();
        	  
        	  //Если мы в режиме редактирования удалили ранее загруженный файл,
        	  //удаляем его через запрос.
        	  if(this._bEdit && oUploader._aDeletedItemForPendingUpload.length > 0){
        		  //т.к. записи в приватном массиве дублируются, будем сохранять id отправленных загрузчиков
        		  var aStored = [];
        		  oUploader._aDeletedItemForPendingUpload.forEach(function (item) {
        			  //если мы еще не сохранили и элемент не содержит ссылки на свой аплоадер
        			  // (был удален до того как был загружен)
        			  if(aStored.indexOf(item.sId) < 0 && !item.getFileUploader()){
        				  var sUrl = item.getUrl();
        				  
        				  if (sUrl) {
            				  jQuery.ajax({
            					  url : sUrl,
            					  type : "DELETE",
            					  beforeSend : function(xhr) {
            						  xhr.setRequestHeader("X-CSRF-Token", this.token);
            					  }.bind(this)
            				  });
            				  aStored.push(item.sId);
        				  }
        				  
        			  }
        		  }.bind(this))
        	  }
          }.bind(this))
          
          // debugger;
          oView.setBusy(false);
          var oStatus = {
        		  success:{
        			  title: this.getText("Successfully"),
        			  type: "Message",
        			  state: "Success",
        			  text:  this.getText("ReqSuccs")
        		  },
        		  fail: {
        			  title: this.getText("Failed"),
        			  type: "Message",
        			  state: "Error",
        			  text: this.getText("ReqErr")
        		  }
          };
          this._oRequestStatusDialog.show.apply(this._oRequestStatusDialog, [oStatus.success]);
          // Проставляем флаг возможности отправить на согласование
          this.getView().getModel().read("/SettingsSet('ZPSUP_00.00.00_ZPS_STAT')", {
        	  success: function(oData, response) {
        		  var bEnabled = true;
        		  if (oData.Value === 'X') bEnabled = false;
        		  this.oEngineModel.setProperty('/approveEnabled', bEnabled);
        	  }.bind(this),
        	  error: function(oError) {
        		  this.oEngineModel.setProperty('/approveEnabled', false);
        	  }.bind(this)
          });
          this.oEngineModel.setProperty('/saveEnabled', false);
          this.oEngineModel.setProperty('/printEnabled', true);
          //Переходим в режим редактирования для последующих изменений
          this._bEdit = true;
          this.oEngineModel.setProperty('/bEdit', true);
      	}.bind(this));
    },

    /***********Группа Б*******************/
    /**
     *
     * @param oEvent
       */
    onEquipSearch: function(oEvent){
      var oTable = sap.ui.getCore().byId("equipTable").setBusy(true);
      var oBindParameters = {
        parameters: {
          navigation: {
            'ZEquipSet': 'ChildNodes'
          }
        }
      };
      var searchText = this.oEngineModel.getProperty('/groupA/equip/searchText');
      var searchKey = this.oEngineModel.getProperty('/groupA/equip/searchKey');
      var oFilter = new sap.ui.model.Filter(searchKey, sap.ui.model.FilterOperator.Contains, searchText);
      if(!oEvent.getParameter('clearButtonPressed')){
        oBindParameters.filters = [oFilter];
      }
      var model = this.getView().getModel();
      model.read("/ZEquipIdSet('" + this.oEngineModel.getProperty("/workshop/id") + "')",
          {
            success: function (oData) {
              oBindParameters.path = "/ZEquipSet('" + oData.Id + "')";
              oTable.bindRows(oBindParameters);
              oTable.setBusy(false);
            }.bind(this),
            error: function () {
              oTable.setBusy(false);
            }
          }
      );
    },
    /**
     *
     * @param oEvent
       */
    handleAggregateSearch: function(oEvent){
      var sValue = oEvent.getParameter("value");
      var aFilters = [this._getFiltersForAggregate()];
      
      if (sValue !== "") {
    	  aFilters.push(new Filter({
    		  filters: [
    			  new Filter("Txtlg", FilterOperator.Contains, sValue),
    			  new Filter("ZpsAggr", FilterOperator.Contains, sValue)
    		  ],
    		  and: false
    	  }));
      }
      oEvent.getSource().getBinding("items").filter(new Filter({
    	  filters: aFilters, 
    	  and: true
      }));
    },

    onApprovePress : function() {
      var sZspPId = this.oEngineModel.getProperty('/projectNumber');
      var oModel = this.getView().getModel();
      oModel.create("/ApprovingSet", {
          ZspPId : sZspPId
        }, {
          success : function(oData, response) {
            this.oEngineModel.setProperty('/approveEnabled', false);
            var msg = this.getText("Sent");
            sap.m.MessageBox.show( msg , {
              icon: sap.m.MessageBox.Icon.INFORMATION,
              title: this.getText("Approval"),
              actions: [sap.m.MessageBox.Action.OK],
              onClose: function(oAction) { / * do something * / }
            });
          }.bind(this),
          error: function(oError) {
        	  sap.m.MessageToast.show("Ошибка: " + JSON.parse(oError.responseText).error.message.value)
          }
        }
      );
    },

    revieGroupBValuationTableFactory: function (sId, oContext) {
      var oData = oContext.getObject();
      if(oData.text){
        return new sap.m.Text({
          text: '{engine>text}'
          //level: 'H3'
        })
      }else{
        //флаг, обазначающий что имеется значение (при редактировании)
        var bToSelect = oData.value == this.oEngineModel.getProperty('/groupB/riskValue');
        var sFunDirId = this.oEngineModel.getProperty('/funcDir/id');

        var oCell = new sap.m.HBox({
          justifyContent: 'Center',
          alignItems: 'Center',
          height: '100%',
          items: new sap.ui.core.Icon({
            src: "sap-icon://accept",
            visible: '{= !!${engine>selected}}'
          })
        }).addCustomData(
          new sap.ui.core.CustomData({
            key: 'dangerState',
            value: '{engine>state}',
            writeToDom: true
          })
        ).addCustomData(
          new sap.ui.core.CustomData({
            key: 'selected',
            value: '{= typeof ${engine>selected} === "undefined" ? "" : ${engine>selected}.toString()}',
            writeToDom: true
          })
        )
      }
      return oCell
    },
    
    onReserveChange: function(oEvent) {
    	var oSelectedItem = oEvent.getParameter("selectedItem");
    	
    	if (oSelectedItem) {
    		var oCntx = oSelectedItem.getBindingContext(),
    			sSelectedKey = oSelectedItem.getKey(),
    			oDfs = sap.ui.getCore().byId("downtimeFactorsSelect");
    		
    		if (oCntx) {
    			this.oEngineModel.setProperty("/reserve/text", oCntx.getProperty("ZreserveTxt"));
    		}
    		
            //Сбрасываем селект резервного оборудования
            if(sSelectedKey !== "3"){
            	this.oEngineModel.setProperty("/downtimeFactor/id", "");
            	this.oEngineModel.setProperty("/downtimeFactor/text", "");
            	this.oEngineModel.setProperty("/emergencyBuyTime", 0);
            }
            
            oDfs.getBinding("items").filter(new Filter("Zreserve", FilterOperator.EQ, sSelectedKey));
    	}
    },
    
    validators:{
      initParamsValidator: function() {
          var aFields = Array.prototype.slice.apply(arguments),
          		 sSpp = aFields.pop(),
          		 iProjChar = aFields.pop();
    	  var bMainField = this.validators.simpleValidator.apply(this, aFields);
    	  
    	  // Если пользователь начал вводить СПП, то необходимо, чтобы он ввёл,
    	  // его корректно, т.е. маски XX-XXXX или XX-XXXX-XX
    	  var bSpp = true;
    	  var oRegExp = new RegExp('[а-яА-Яa-zA-Z0-9]{2}-[а-яА-Яa-zA-Z0-9]{4}(-__|-[а-яА-Яa-zA-Z0-9]{2})');
    	  if (!(sSpp === '' || oRegExp.test(sSpp))) {
    		  bSpp = false;
    	  }
    	  
    	  var bProjChar = true;
    	  if (iProjChar === "00") {
    		  bProjChar = false;
    	  }
    	  return bMainField && bSpp && bProjChar;
      },
      
      simpleValidator: function () {
        //для легкости дебага параметер снимающий валидацию
        if(jQuery.sap.getUriParameters().get("no-validate")) return true;
        var aFields = Array.prototype.slice.apply(arguments);
        return aFields.every(function (val) {
          return !!val
        })
      },
      
      budgetValidator: function (phase1, phase2, phase3, phase4, phase5, bEarlyImpl, bEarlyImplReason, bEarlyImplDesc, nAllBudget, ndsAllBudget, sOzmId) {
        var aPhase = [phase1, phase2, phase3, phase4, phase5];
        //TODO сделать валидацию фаз в зависимости от выбранных сроков ZsubpInp
        //Если не передан с НДС - не проверяем
        var bBudget = (ndsAllBudget === undefined) ? (nAllBudget > 0) : (nAllBudget > 0 && ndsAllBudget > 0);
        var bEarlyImplementation = bEarlyImpl ? (bEarlyImplReason !== '') && (bEarlyImplDesc !== '') : true;
        
        var bIsSap = this.getModel('engine').getProperty('/company/isSap');
        var sRealizationFormId = this.getModel('engine').getProperty('/realizationForm/id');
        var bOzm = true;
        if (sOzmId !== undefined && bIsSap && sRealizationFormId === '2') {
            bOzm = !!sOzmId;
        }
        return bBudget && bEarlyImplementation && bOzm && aPhase.some(function(phase){ return phase !== ""})
      }
    },
    
    _aLabStepValidationBinding : ['engine>/realizationForm/id', 'engine>/groupV/ZeqCat/id'],
    
    _oValidationBinding : {
    	GroupD : {
    		_aMain :	[
    		     'engine>/realizationForm/id',
    		     'engine>/groupD/prjcr/id',
    		     'engine>/groupD/Zsubprog',
    		     'engine>/groupD/ZpsEstyp',
    		     'engine>/groupD/prjty/id',
    		     'engine>/riskDecr',
    		     'engine>/groupD/STPFullTab'
    		],
    		
     		init : function() {
    			return this._aMain.slice();
    		},
    		
    		inProcess : function(ZsubprogKey) {
    			var aBinding =  this.init();
    			
    			if (ZsubprogKey === '009' || ZsubprogKey === '008' || ZsubprogKey === '007' || ZsubprogKey === '006') {
    				var index = aBinding.indexOf('engine>/groupD/ZpsEstyp');
    				
    				if (index > -1) {
    					aBinding.splice(index, 1);
    				}
    				var index2 = aBinding.indexOf('engine>/groupD/STPFullTab');
    				if (index2 > -1) {
    					aBinding.splice(index2, 1);
    				}
    			}    				
    			return aBinding;
    		}
    	},
    	GroupV : {
    		_aMain :	[
    		    'engine>/realizationForm/id',
    		    'engine>/groupV/ZeqCat/id',
    		    'engine>/targetSubprog/id'
    		],
    		
    		_aZeqCatOne : [
    		    'engine>/groupV/ZregDoc'
    		],
    		
    		init : function() {
    			return this._aMain.slice();
    		},
    		
    		inProcess : function(ZeqCatKey) {
    			var aBinding =  this.init();
    			
    			if (ZeqCatKey == '1') {
    				aBinding = aBinding.concat(this._aZeqCatOne);
    			}
    			
    			return aBinding;
    		}
    	},
    	GroupV_riski : {
    		_aMain : [
    		    'engine>/groupV/ZpsEqty/id',
    		    'engine>/riskDecr'
    		],
    		
    		_ZpsEqty : [
    		           [
    		            'engine>/groupV/ZpurchPr',
    		            'engine>/groupV/ZdwntmPc'
    		           ],
    		           [
    		            'engine>/groupV/ZncPr',
    		            'engine>/groupV/ZdwntmLm'
    		           ],
    		           [
    		            'engine>/groupV/ZdwntmPc'
    		           ]
    		],
    		
    		init : function() {
    			return this._aMain.slice();
    		},
    		
    		inProcess : function(ZpsEqtyKey) {
    			var aBinding =  this.init();
    			
    			switch(ZpsEqtyKey) {
    				case '1':
    					aBinding = aBinding.concat(this._ZpsEqty[0]);
    					break;
    				case '2':
    					aBinding = aBinding.concat(this._ZpsEqty[1]);
    					break;
    				case '3':
    					aBinding = aBinding.concat(this._ZpsEqty[2]);
    					break;
    				default:
    					break;
    			}
    			
    			return aBinding;
    		},
    		
    		getEmptyPropertyEngineByZpsEqty : function(ZpsEqtyKey) {
    			var aResult = [];
    			if (ZpsEqtyKey) {
    				var iKey = parseInt(ZpsEqtyKey) - 1;
        			this._ZpsEqty.forEach(function(item, i, arr) {
        				if (i != iKey) {
        					aResult = aResult.concat(this._ZpsEqty[i]);
        				}
        			}.bind(this));
        			
        			//Удаляем элементы которые повторяются для различных условий
        			this._ZpsEqty[iKey].forEach(function(item, i, arr) {
        				var index = aResult.indexOf(item);
        				while (index !== -1) {
        					aResult.splice(index, 1);
        					index = aResult.indexOf(item);
        				}
        			});
        				
        			aResult.forEach(function(item, i, arr) {
        				arr[i] = item.replace('engine>', '');
        			});		
    			}
    			return aResult;
    		}
    	},
    	
    	CalcRiskV : {
    		_aMain : [ 
    			'engine>/downtimeTotal' 
    		],
    		
    		init : function() {
    			return this._aMain.slice();
    		}
    	},
    	GroupB: {
    	    OTPBLeftoverRisk: [
    	        "engine>/groupB/leftoverRiskFrequency/id",
                "engine>/groupB/leftoverRiskValue"
    	    ],
    		SourceDataVal: {
    			_aMain: [
        			'engine>/projectName',
                    'engine>/targetSubprog/id',
        			'engine>/realizationForm/id'
        		],
        		
        		_aAdditionalFields: [
        			'engine>/groupB/projectType/id'
        		],
        		init: function() {
    				return this._aMain.slice();
    			},
    			
    			inProcess: function(sFuncDir) {
    				var aBinding =  this.init();
    				if (sFuncDir === FUNCTIONAL_AREAS.GROUPB.SAFETY) {
    					aBinding = aBinding.concat(this._aAdditionalFields);
    				}
    				return aBinding;
    			}
    		}
    	},
    	OTPBVal : {
    		_aMain : [ 
    			'engine>/groupB/ZpsRskdt',
                'engine>/groupB/valuer/fio',
                'engine>/groupB/riskFrequency/id',
                'engine>/groupB/riskValue'
            ],
            
            _aELReisk : [ 
                'engine>/groupB/ZpsExploss',
                'engine>/groupB/ZriskK' 
            ],
            
            _aPriority: [
                'engine>/priority/id'
            ],
    		
    		init : function() {
    			return this._aMain.slice();
    		},
    		
    		inProcess : function(FA) {
    			var aBinding =  this.init();
    			
    			if (FA == '10') {
    				aBinding = aBinding.concat(this._aELReisk);
    			} else {
    			    aBinding = aBinding.concat(this._aPriority);
    			}
    			
    			return aBinding;
    		}
    	},
    	
    	CalcRisk : {
    		_aMain : [ 
    			'engine>/downtimeTotal',
    		],
    		
    		_aRealizeForm : [
    			'engine>/realizationForm/id'
    		],
    		
    		init : function() {
    			return this._aMain.slice();
    		},
    		
    		inProcess : function(ZpsPrtyp) {
    			var aBinding =  this.init();
    			
    			if (ZpsPrtyp === '5') {
    				aBinding = aBinding.concat(this._aRealizeForm);
    			}
    			
    			return aBinding;
    		}
    	}
    },
    
    
    createBudgetObject: function(sCurrency, nFirstYear, nCountYear) {
    	var budget = this.oEngineModel.generateBudget(sCurrency, nFirstYear, nCountYear);
    	this.onReplaceInternText(budget)
    	return budget;
    },
    
    _getFiltersForAggregate: function() {
        var sCompanyId = this.oEngineModel.getProperty('/company/id');
        var sWorkshopId = this.oEngineModel.getProperty('/workshop/id');

        var oCompanyFilter = new Filter({
        	path: 'Zentity',
        	operator: FilterOperator.EQ,
        	value1: sCompanyId
        });
        //Убрали фильт по цеху, теперь агрегаты по компании фильтруются
        //var oWorkshopFilter = new Filter('ZspWshp', FilterOperator.EQ, sWorkshopId);
        
        return new Filter({
        	filters: [oCompanyFilter], //если с цехом [oCompanyFilter, oWorkshopFilter]
        	and: true
        });
    },

    reviewFilesFactory: function (sId, oContext) {
    	var oData = oContext.getObject();
        var oBundle = this.oView.getModel('i18n').getResourceBundle();
        var files = oData.review.map(function (filename) {
        	var sHref = '';
        	var oEngineModel = this.oView.getModel('engine');
        	if(oEngineModel.getProperty('/bEdit') || oEngineModel.getProperty('/bView')){
        		var sProjectName = oEngineModel.getProperty('/projectNumber');
        		sHref = this.sServicePrefix + '/sap/opu/odata/sap/Z_PSUP_SERVICES_SRV/FileSet(FileName=\''+filename.name+'\',ProjName=\''+sProjectName +'\',ObjType=\''+oData.objectType+'\')/$value';
        	}
        	var oItem = filename.url ? new Link({
				text: filename.name,
				target: '_blank',
				href: sHref
			}).addStyleClass('sapUiSmallMarginBegin'):
				new Text({
					text: filename.name
			});
        	return new HBox({
        		items: [
        			new Icon({
        				src: UploadCollection.prototype._getIconFromFilename(filename.name),
        				size: '3rem'
        			}),
        			new VBox({
        				items: oItem,
        				justifyContent: 'Center'
        			})
        		]
        	})
        }.bind(this));

        return new Panel({
          expandable: true,
          expanded: false,
          visible: files.length > 0,
          headerToolbar: new Toolbar({
            content: new Title({
              text: oBundle.getText(oData.numberOfAttachmentsText) + ' (' + files.length + ')'
            })
          }),
          content: new Grid({
            content: files
          })
        });
    },
    
    checkBeforeNavToReviewEdit: function() {
        var sFuncDirId = this.oEngineModel.getProperty('/funcDir/id');
        var sRealizationFormId = this.oEngineModel.getProperty('/realizationForm/id');
        var sZpsPrtyp = this.oEngineModel.getProperty('/groupA/ZpsPrtyp');
        var oEquip = this.oEngineModel.getProperty('/groupA/equip');
        var iProjectCharacteristic = this.oEngineModel.getProperty('/projectCharacteristic/id');
        var oEarlyImplementation = this.oEngineModel.getProperty('/earlyImplementation');
        var sTargetSubprog = this.oEngineModel.getProperty('/targetSubprog/id');
        var sOTPBTypeDamage = this.oEngineModel.getProperty('/groupB/OTPB/TypeDamage/id');
        var sOTPBProjectType = this.oEngineModel.getProperty('/groupB/projectType/id');
        var bGroupAFs = this.oEngineModel.getProperty('/groupA/isFireGuard');
        var sGroupAFsType = this.oEngineModel.getProperty('/groupA/FsType/id');
        var bManualWorkshop = this.oEngineModel.getProperty('/manualWorkshop');
        var sWorkshopName = this.oEngineModel.getProperty('/workshop/name');
        var bIsSap = this.oEngineModel.getProperty('/company/isSap');
        var bIsCustomOzm = this.oEngineModel.getProperty('/ozm/isCustom');
        var iWearCount = this.oEngineModel.getProperty('/budget/waersCount');
        var iInvCount = this.oEngineModel.getProperty('/budget/invCount');
        var iFirstYearTotalBudget = this.oEngineModel.getProperty('/budget/rows/noNds/' + (iWearCount + 1) * iInvCount + '/years/0/value');
        var sPriority = this.oEngineModel.getProperty('/priority/id');
        var sGroup = FUNCTIONAL_AREAS.getGroupById(sFuncDirId);

        //Проверяем оборудование
        if (this.checkIsNeedEquip(sFuncDirId) && this.formatter.visibilityEquipment(sRealizationFormId, sZpsPrtyp)) {
            if ((oEquip.manual && oEquip.name === '') || (!oEquip.manual && oEquip.id === '')) {
                this.showErrorMessageBox(this.getText('EquipEditError'));
                return false;
            }
        }
        
        if (['GROUPA', 'GROUPG', 'GROUPE'].indexOf(sGroup) !== -1) {
            if (sRealizationFormId === '' || sRealizationFormId === '0') {
                this.showErrorMessageBox(this.getText('ErrorFormImplementation'));
                return false;
            }
        }

        //Проверка что проставлена характеристика проекта
        if (iProjectCharacteristic === '00') {
            this.showErrorMessageBox(this.getText('ErrorProjectCharacteristicText'), function() {
                sap.ui.getCore().byId('projectCharacteristic').focus();
            });
            return false;
        }

        //Проверяем что заполнен цех при ручном вводе
        if (bManualWorkshop && sWorkshopName === '') {
            this.showErrorMessageBox(this.getText('ErrorWorkshopText'), function() {
                sap.ui.getCore().byId('workshopName').focus();
            });
            return false;
        }

        //Проверяем что заполнены причина и ее описание для ранней реализации
        if (oEarlyImplementation.ZpsImpl) {
            if (oEarlyImplementation.reason === '' || oEarlyImplementation.description === '') {
                this.showErrorMessageBox(this.getText('ErrorEarlyImplementationText'), function() {
                    sap.ui.getCore().byId('earlyImplementation').focus();
                });
                return false;
            }
        }

        //Проверяем что заполнена целевая подпрограмма
        if ((sFuncDirId === this._FN_LIKIO || sFuncDirId === this._FN_ENERGETICS || sFuncDirId === this._FN_LOGISTICS) && sTargetSubprog === '000') {
            this.showErrorMessageBox(this.getText('ErrorTargetSubprogText'), function() {
                if (sFuncDirId === '07') {
                    sap.ui.getCore().byId('targetSubprogCb').focus();
                } else {
                    if (sFuncDirId === '04' || sFuncDirId === '05') {
                        sap.ui.getCore().byId('idFuncDirA').focus();
                    }
                }
            });
            return false;
        }

        if (sFuncDirId === this._FN_SAFETY) {
            //Проверяем что заполнен тип опасности в ОТПБ
            if (sOTPBTypeDamage === '') {
                this.showErrorMessageBox(this.getText('ErrorTypeDamageText'));
                return false;
            }
            //Проверяем что заполнен тип проекта в ОТПБ
            if (sOTPBProjectType === '') {
                this.showErrorMessageBox(this.getText('ErrorProjectTypeText'));
                return false;
            }
            if (sPriority === '') {
                this.showErrorMessageBox(this.getText('ErrorPriorityText'));
                return false;
            }
        }

        if (bGroupAFs) {
            //Проверяем что заполнен тип ПБ если установлен флаг Пожарная безопасность
            if (sGroupAFsType === '') {
                this.showErrorMessageBox(this.getText('ErrorFsTypeText'));
                return false;
            }
            if (sPriority === '') {
                this.showErrorMessageBox(this.getText('ErrorPriorityText'));
                return false;
            }
        }
        
        if (bIsSap && !bIsCustomOzm && sRealizationFormId === REALIZATION_FORM.REP_INST_EQUIPMENT && iFirstYearTotalBudget > 0) {
            var sOzm = this.oEngineModel.getProperty('/ozm/data/id');
            if (sOzm === '') {
                this.showErrorMessageBox(this.getText('ErrorOzm'), function() {
                    sap.ui.getCore().byId('iBudgetTable').focus();
                }.bind(this));
                return false;
            }
        }

        return true;
    },
    
    showErrorMessageBox: function(sErrorText, fnClose) {
        sap.m.MessageBox.show(sErrorText, {
            icon: sap.m.MessageBox.Icon.ERROR,
            title: this.getText('ErrorTitle'),
            onClose: fnClose ? fnClose : null
        });
    },
    
    showWarningDialog: function(sTitle, sFocusId, oPromise) {
        sap.m.MessageBox.show(sTitle, {
            icon: sap.m.MessageBox.Icon.WARNING,
            title: this.getText('CheckBudgetWarningTitle'),
            actions: [this.getText('CheckBudgetWarningToEditButton'), this.getText('CheckBudgetWarningIgnoreButton')],
            onClose: function(sAction) {
                switch (sAction) {
                    case this.getText('CheckBudgetWarningToEditButton'):
                        oPromise.reject(sFocusId);
                        break;
                    case this.getText('CheckBudgetWarningIgnoreButton'):
                        oPromise.resolve();
                        break;
                }
            }.bind(this)
        });
    },
    
    checkBudget: function() {
        var oPromise = new jQuery.Deferred();
        var iBudgetDifference = this.getBudgetDifference();
        var aTextArgs = [iBudgetDifference > 0 ? this.getText('CheckBudgetWarningGreater') : this.getText('CheckBudgetWarningLess')];
        aTextArgs.push(Math.abs(iBudgetDifference).toString());

        if (iBudgetDifference !== 0) {
            var sTitle = this.getText('CheckBudgetWarningMessage', aTextArgs);
            this.showWarningDialog(sTitle, 'iBudgetTable', oPromise);
        } else {
            oPromise.resolve();
        }

        return oPromise.promise();
    },
    
    checkProjImplPeriod: function() {
        var oPromise = new jQuery.Deferred();
        var bValid = this.isProjImplPeriodValid();

        if (!bValid) {
            var sTitle = this.getText('CheckProjImplPeriodWarningMessage');
            this.showWarningDialog(sTitle, 'dateLimit', oPromise);
        } else {
            oPromise.resolve();
        }

        return oPromise.promise();
    },
    
    getBudgetDifference: function() {
        var iWearCount = this.oEngineModel.getProperty('/budget/waersCount');
        var iInvCount = this.oEngineModel.getProperty('/budget/invCount');
        var oTotalBudget = this.oEngineModel.getProperty('/budget/rows/noNds/' + (iWearCount + 1) * iInvCount);
        var iSumYearsBudget = oTotalBudget.years.reduce(function(iSum, oYearBudget) {
            return iSum + oYearBudget.value;
        }, 0);
        var iDifference = oTotalBudget.allBudget - (oTotalBudget.financed + iSumYearsBudget);
        
        return +iDifference.toFixed(2);
    },
    
    isProjImplPeriodValid: function() {
        var iProjImplPeriodYear = this.oEngineModel.getProperty('/dateLimit').getFullYear();
        var iWearCount = this.oEngineModel.getProperty('/budget/waersCount');
        var iInvCount = this.oEngineModel.getProperty('/budget/invCount');
        var oTotalBudget = this.oEngineModel.getProperty('/budget/rows/noNds/' + (iWearCount + 1) * iInvCount);
        
        var iLastNotEmplyYear = oTotalBudget.years.reduce(function(year, oYearBudget) {
            return oYearBudget.value > 0 ? oYearBudget.year : year;
        }, null);
        
        return iProjImplPeriodYear === iLastNotEmplyYear;
    },
    
    checkIsNeedEquip: function(sFuncDirKey) {
    	return FUNCTIONAL_AREAS.getGroupById(sFuncDirKey) === 'GROUPA' && !this.oEngineModel.checkIsLaboratory();
    },
    
    onPrintPassport: function(oEvent) {
    	var sZspPId = this.oEngineModel.getProperty('/projectNumber'),
    		sUrl = this._fixGetWindowsLocationOrigin() + 
    				"/sap/opu/odata/sap/Z_PSUP_SERVICES_SRV/PrintFormSet('" + sZspPId + "')/$value";
    	window.open(sUrl); 
    },
    
    onSendMessage: function(oEvent) {
    	var sProjectId = this.oEngineModel.getProperty('/projectNumber');
    	var sProjectName = this.oEngineModel.getProperty('/fullProjectName');
        var oI18n = this.getView().getModel('i18n').getResourceBundle();
        
        sap.m.URLHelper.triggerEmail(
    		'',
    		oI18n.getText('SendMessageSubject', [sProjectId]),
    		sProjectName + '\r\n'
    	);
    },
    
    onImplFormChange: function (oEvent) {
    	var oSelectedItem = oEvent.getParameter('selectedItem');
    	
    	if (oSelectedItem) {
            var oBindingContext = oSelectedItem.getBindingContext();
            
            this.oEngineModel.setProperty('/realizationForm/name', oBindingContext.getProperty("Txtmd"));
            this.oEngineModel.setProperty('/realizationForm/shortName', oBindingContext.getProperty("Txtsh"));
            
           var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
           
           if (FUNCTIONAL_AREAS.getGroupById(sFuncDir) === 'GROUPA') {
               var sProjectType = this.oEngineModel.getProperty('/groupA/ZpsPrtyp');
               var sImpForm = oSelectedItem.getKey();
               if (sProjectType === PROJECT_TYPES.OTHER_PROJECTS && sImpForm === REALIZATION_FORM.REP_INST_EQUIPMENT) {
                   var iWearCount = this.oEngineModel.getProperty('/budget/waersCount');
                   var iInvCount = this.oEngineModel.getProperty('/budget/invCount');
                   this.checkFirstYearBudget(this.oEngineModel.getProperty('/budget/rows/noNds/' + (iWearCount + 1) * iInvCount + '/years/0/value'));
               }
           }
        }
    },
    
    checkFirstYearBudget: function(iFirstYearTotalBudget) {
        var bIsSap = this.oEngineModel.getProperty('/company/isSap');
        var sRealizationFormId = this.oEngineModel.getProperty('/realizationForm/id');
        var bCustomOzm = this.oEngineModel.getProperty('/ozm/isCustom');
        var bNeedShowOzm = (bIsSap && sRealizationFormId === REALIZATION_FORM.REP_INST_EQUIPMENT);
        var oStep = this.byId(this.getStepForOzmValidation());
        
        if (bNeedShowOzm) {
            if (!bCustomOzm) {
                if (iFirstYearTotalBudget > 0) {
                    this.setStepValidationPath('add', oStep, 'engine>/ozm/data/id');
                } else {
                    this.setStepValidationPath('remove', oStep, 'engine>/ozm/data/id');
                }
            } else {
                this.setStepValidationPath('remove', oStep, 'engine>/ozm/data/id');
            }
        } else {
            this.oEngineModel.setProperty('/ozm/data', {
                id: '',
                name: '',
                planedDelivery: '',
                date: null,
                price: 0,
                currency: ''
            });
        }
    },
    
    getStepForOzmValidation: function() {
        var sFuncDir = this.oEngineModel.getProperty('/funcDir/id');
        var sProjectType = this.oEngineModel.getProperty('/groupA/ZpsPrtyp');
        
        switch (FUNCTIONAL_AREAS.getGroupById(sFuncDir)) {
            case 'GROUPB':
            case 'GROUPV':
            case 'GROUPG':
            case 'GROUPE':
                return 'BudgetStep';
                break;
            case 'GROUPA':
                if (sProjectType === PROJECT_TYPES.AUTO_REPLACEMENT || sProjectType === PROJECT_TYPES.TRANSPORT_REPLACEMENT || sProjectType === "0") {
                    return 'BudgetStep';
                }
                if (sProjectType === PROJECT_TYPES.OTHER_PROJECTS) {
                    return 'CalcRisk';
                }
                break;
            default:
                return '';
                break;
        }
    },
    
    setStepValidationPath: function(sOperation, oStep, sPath) {
        var oStepBindingInfo = oStep.getBindingInfo('validated');
        if (oStepBindingInfo) {
            var oValidateObject = {
                    parts: oStepBindingInfo.parts.map(function(item) {
                        return item.model + '>' + item.path
                    }),
                    formatter: oStepBindingInfo.formatter
                };
                
                switch (sOperation) {
                    case 'add':
                        if (oValidateObject.parts.indexOf(sPath) === -1) {
                            oValidateObject.parts.push(sPath);
                        }
                        break;
                    case 'remove':
                        var iDelIndex = -1;
                        oValidateObject.parts.forEach(function(item, index) {
                            switch (typeof(item)) {
                                case "string":
                                    if (item === sPath) {
                                        iDelIndex = index;
                                    }
                                    break;
                                case "object":
                                    if (item.path === sPath) {
                                        iDelIndex = index;
                                    }
                                    break
                            }
                        });
                        if (iDelIndex !== -1) {
                            oValidateObject.parts.splice(iDelIndex, 1);
                        }
                        break;
                }
                
                oStep.bindProperty('validated', oValidateObject);
        }
    },
    
    onTargetSubprogChange: function(oEvent) {
        var oSelectedItem = oEvent.getParameter('selectedItem');
    
        if (oSelectedItem) {
            this.oEngineModel.setProperty('/targetSubprog/name', oSelectedItem.getBindingContext().getProperty('Txtlg'));
        }
    },
    
    onOzmValueHelp: function(oEvent) {
        var oModel = this.getView().getModel('engine');

        if (!this._oOzmSelect) {
            this._oOzmSelect = new OzmSelect(this.getView());
        }
        this._oOzmSelect.open(oModel.getProperty('/company/id'), function(oOzm) {
            oModel.setProperty('/ozm/data', oOzm);
        }.bind(this));
    },
    
    OnResetOzm: function(oEvent) {
        this.oEngineModel.setProperty('/ozm/data', {
            id: '',
            name: '',
            shortName: '',
            planedDelivery: '',
            date: null,
            price: 0,
            currency: ''
        });
    },
    
    getDialogContentById: function(oDialog, sId) {
        var aContent = oDialog.getContent(),
            oList;
        
        aContent.forEach(function(oItem) {
            if (oItem.getId() === sId) {
                oList = oItem;
            }
        });
        
        return oList;
    },
    
    onPriorityHelpDialog: function(oEvent) {
        var oModel = this.getView().getModel('engine');
        var sFuncDir = oModel.getProperty('/funcDir/id');
        var sTargetSubprog = oModel.getProperty('/targetSubprog/id');

        if (!this._oPriorityDialog) {
            this._oPriorityDialog = new PriorityDialog(this.getView());
        }
        this._oPriorityDialog.open(sFuncDir, sTargetSubprog, function(oPriority) {
            oModel.setProperty('/priority/txtsh', oPriority.txtsh);
            oModel.setProperty('/priority/txtlg', oPriority.txtlg);
            oModel.setProperty('/priority/id', oPriority.id);
        }.bind(this));
    },
    
    onCustomOzmSelectedChanged: function(oEvent) {
        var bSelected = oEvent.getParameter('selected');
        var iWearCount = this.oEngineModel.getProperty('/budget/waersCount');
        var iInvCount = this.oEngineModel.getProperty('/budget/invCount');
        
        if (bSelected) {
            this.oEngineModel.setProperty('/ozm/data', {
                id: '',
                name: '',
                shortName: '',
                planedDelivery: '',
                date: null,
                price: 0,
                currency: ''
            });
        }
        
        this.checkFirstYearBudget(this.oEngineModel.getProperty('/budget/rows/noNds/' + (iWearCount + 1) * iInvCount + '/years/0/value'));
    }
  });
});