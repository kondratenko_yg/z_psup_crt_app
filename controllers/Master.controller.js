sap.ui.define(['initiatives/controllers/BaseController'], function (BaseController) {
  return BaseController.extend('initiatives.controllers.Master', {
    onNewRequestPress: function () {
      this.getRouter().navTo('new');
    }
  });
});
