sap.ui.define([
  'sap/ui/core/UIComponent',
  'sap/ui/core/AppCacheBuster',
  'sap/ui/model/json/JSONModel',
  'sap/ui/model/resource/ResourceModel'
], function (UIComponent, AppCacheBuster, JSONModel, ResourceModel) {
    'use strict';
    
    return UIComponent.extend('initiatives.Component', {
        metadata: {
            manifest: 'json'
        },
        init: function () {
            AppCacheBuster.register('/sap/bc/ui5_ui5/sap/z_psup_shared');
            sap.ui.getCore().loadLibrary('z_psup_shared', '/sap/bc/ui5_ui5/sap/z_psup_shared/webapp');
            
            // call the init function of the parent
            UIComponent.prototype.init.apply(this, arguments);

            jQuery.sap.includeStyleSheet(jQuery.sap.getModulePath('initiatives.styles.style','.css'));
            
            jQuery.sap.require('initiatives.util.formatters');
            jQuery.sap.require('initiatives.util.polyfills');
            jQuery.sap.require('initiatives.models.type.cInteger');
            jQuery.sap.require('initiatives.models.type.cFloat');
            jQuery.sap.require('initiatives.models.type.cDayToHourFloat');
            
            //Прогружаем модель настроек
            var oSettingsJSON = new JSONModel();
            var sPath = jQuery.sap.getModulePath('initiatives.JSON.Settings') + '.json';
            oSettingsJSON.loadData(sPath, '', false);
            this.setModel(oSettingsJSON, 'settings');
            
            var oSettingsModel = this.getModel('settings');
            oSettingsModel.setProperty('/CONST/FA', z_psup_shared.FUNCTIONAL_AREAS);
            oSettingsModel.setProperty('/CONST/RF', z_psup_shared.REALIZATION_FORM);
            oSettingsModel.setProperty('/CONST/GROUPB', z_psup_shared.GROUPB);
            this.getModel().read('/SettingsSet', {
                success: function (oData, response) {
                    oData.results.forEach(function (oSetting) {
                        var bInd = (oSetting.Value === 'X');
                        switch (oSetting.Key) {
                            case 'IS_ADMIN':
                                oSettingsModel.setProperty('/roles/isAdmin', bInd);
                                break;
                            case 'IS_SUPERVISOR':
                                oSettingsModel.setProperty('/roles/isSupervisor', bInd);
                                break;
                            case 'IS_ADVUSER':
                                oSettingsModel.setProperty('/roles/isAdvUser', bInd);
                                break;
                            case 'IS_USER':
                                oSettingsModel.setProperty('/roles/isUser', bInd);
                                break;
                            case 'ZPSUP_00.00.00_ZPS_START_SYS':
                                var sDate = oSetting.Value;
                                var oDate = new Date(+sDate.substr(0,4), +sDate.substr(4,2) - 1, +sDate.substr(6,2));
                                oSettingsModel.setProperty('/sysStartDate', oDate);
                                break;
                        }
                    }.bind(this));
                }.bind(this)
            });
            
            this.getRouter().initialize();
        }
    });
});